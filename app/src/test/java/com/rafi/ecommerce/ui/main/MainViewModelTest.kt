package com.rafi.ecommerce.ui.main

import androidx.lifecycle.SavedStateHandle
import com.nhaarman.mockitokotlin2.whenever
import com.rafi.core.data.model.SessionModel
import com.rafi.core.data.model.dummyPreferences
import com.rafi.core.data.repository.NotificationRepository
import com.rafi.core.data.repository.ProductRepository
import com.rafi.core.data.repository.UserRepository
import com.rafi.core.data.source.local.room.entity.dummyCartList
import com.rafi.core.data.source.local.room.entity.dummyNotification
import com.rafi.core.data.source.local.room.entity.dummyWishList
import com.rafi.ecommerce.util.TestDispatcherRule
import junit.framework.TestCase.assertEquals
import junit.framework.TestCase.assertFalse
import junit.framework.TestCase.assertTrue
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.flow.first
import kotlinx.coroutines.flow.flowOf
import kotlinx.coroutines.test.runTest
import org.junit.Before
import org.junit.Rule
import org.junit.Test
import org.junit.runner.RunWith
import org.junit.runners.JUnit4
import org.mockito.Mock
import org.mockito.Mockito.mock
import org.mockito.kotlin.verify

@RunWith(JUnit4::class)
class MainViewModelTest {

    @OptIn(ExperimentalCoroutinesApi::class)
    @get:Rule
    val testDispatcher = TestDispatcherRule()

    @Mock
    private var userRepository: UserRepository = mock()
    private var productRepository: ProductRepository = mock()
    private var notificationRepository: NotificationRepository = mock()
    private lateinit var mainViewModel: MainViewModel
    private var savedStateHandle: SavedStateHandle =
        SavedStateHandle(mapOf(MainFragment.MOVE_TRANSACTION_BUNDLE_KEY to false))

    @Before
    fun setUp() {
        mainViewModel = MainViewModel(
            userRepository,
            productRepository,
            notificationRepository,
            savedStateHandle
        )
    }

    @Test
    fun `test productCartSize`() = runTest {
        // Given
        whenever(productRepository.getProductCartSize())
            .thenReturn(flowOf(dummyCartList.size))

        // When
        val expectedResult = mainViewModel.productCartSize().first()

        // Then
        verify(productRepository).getProductCartSize()

        assertEquals(5, expectedResult)
    }

    @Test
    fun `test wishListProductSize`() = runTest {
        // Given
        whenever(productRepository.getWishListProductsSize())
            .thenReturn(flowOf(dummyWishList.size))

        // When
        val expectedResult = mainViewModel.wishListProductSize().first()

        // Then
        verify(productRepository).getWishListProductsSize()

        assertEquals(5, expectedResult)
    }

    @Test
    fun `test notificationSize`() = runTest {
        // Given
        whenever(notificationRepository.getNotificationDataSize())
            .thenReturn(flowOf(dummyNotification.size))

        // When
        val expectedResult = mainViewModel.notificationSize().first()

        // Then
        verify(notificationRepository).getNotificationDataSize()

        assertEquals(6, expectedResult)
    }

    @Test
    fun `test getSession`() = runTest {
        // Given
        whenever(userRepository.getSession())
            .thenReturn(flowOf(dummyPreferences))

        // When
        val expectedResult = mainViewModel.user

        // Then
        verify(userRepository).getSession()
        assertEquals(dummyPreferences, expectedResult)
    }

    @Test
    fun `test checkUserData`() = runTest {
        // Given
        whenever(userRepository.getSession())
            .thenReturn(flowOf(dummyPreferences))

        // When
        val expectedResult = mainViewModel.checkUserData()

        // Then
        verify(userRepository).getSession()
        assertTrue(expectedResult)
    }

    @Test
    fun `test checkLogin`() = runTest {
        // Given
        whenever(userRepository.getSession())
            .thenReturn(flowOf(SessionModel()))

        // When
        val expectedResult = mainViewModel.checkLogin()

        // Then
        verify(userRepository).getSession()
        assertFalse(expectedResult)
    }
}
