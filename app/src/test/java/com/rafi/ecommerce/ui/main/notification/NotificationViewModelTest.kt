package com.rafi.ecommerce.ui.main.notification

import com.nhaarman.mockitokotlin2.verify
import com.nhaarman.mockitokotlin2.whenever
import com.rafi.core.data.repository.NotificationRepository
import com.rafi.core.data.source.local.room.entity.dummyNotification
import com.rafi.core.util.DataMapper.toNotification
import com.rafi.ecommerce.util.TestDispatcherRule
import junit.framework.TestCase.assertEquals
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.flow.first
import kotlinx.coroutines.flow.flowOf
import kotlinx.coroutines.test.runTest
import org.junit.Before
import org.junit.Rule
import org.junit.Test
import org.junit.runner.RunWith
import org.junit.runners.JUnit4
import org.mockito.Mock
import org.mockito.Mockito.mock

@RunWith(JUnit4::class)
class NotificationViewModelTest {

    @OptIn(ExperimentalCoroutinesApi::class)
    @get:Rule
    val testDispatcher = TestDispatcherRule()

    @Mock
    private var notificationRepository: NotificationRepository = mock()
    private lateinit var notificationViewModel: NotificationViewModel

    @Before
    fun setUp() {
        notificationViewModel = NotificationViewModel(
            notificationRepository
        )
    }

    @Test
    fun `test getNotification`() = runTest {
        // Given
        whenever(notificationRepository.getNotification())
            .thenReturn(flowOf(dummyNotification.map { it.toNotification() }))

        // When
        val actualData = notificationViewModel.getNotificationData().first()

        // Then
        assertEquals(
            dummyNotification.map { it.toNotification() },
            actualData
        )
    }

    @Test
    fun `test readNotification`() = runTest {
        // Given
        val notification = dummyNotification[0].toNotification()

        // When
        notificationViewModel.readNotification(notification)

        // Then
        verify(notificationRepository).updateNotification(notification.copy(isRead = true))
    }
}
