package com.rafi.ecommerce.ui.prelogin.login

import com.rafi.core.data.repository.UserRepository
import com.rafi.core.util.Resources
import com.rafi.ecommerce.util.TestDispatcherRule
import junit.framework.TestCase.assertEquals
import junit.framework.TestCase.assertTrue
import kotlinx.coroutines.flow.first
import kotlinx.coroutines.flow.flowOf
import kotlinx.coroutines.test.advanceUntilIdle
import kotlinx.coroutines.test.runTest
import org.junit.Before
import org.junit.Rule
import org.junit.Test
import org.junit.runner.RunWith
import org.junit.runners.JUnit4
import org.mockito.Mockito.mock
import org.mockito.kotlin.whenever

@RunWith(JUnit4::class)
class LoginViewModelTest {

    @get:Rule
    val testDispatcher = TestDispatcherRule()

    private var userRepository: UserRepository = mock()
    private lateinit var loginViewModel: LoginViewModel

    @Before
    fun setUp() {
        loginViewModel = LoginViewModel(userRepository)
    }

    @Test
    fun `test getOnboarding`() = runTest {
        // Given
        whenever(userRepository.getOnboarding())
            .thenReturn(flowOf(true))

        // When
        val actualData = loginViewModel.getOnboarding().first()

        // Then
        assertTrue(actualData)
    }

    @Test
    fun `test PostLogin`() = runTest {
        // Given
        val email = "test@example.com"
        val password = "password"

        whenever(userRepository.login(email, password))
            .thenReturn(flowOf(Resources.Loading, Resources.Success(true)))

        // When
        loginViewModel.postLogin(email, password)
        advanceUntilIdle()

        // Then
        assertEquals(Resources.Success(true), loginViewModel.state.value)
    }
}
