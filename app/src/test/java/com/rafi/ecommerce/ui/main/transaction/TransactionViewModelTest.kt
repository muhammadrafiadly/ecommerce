package com.rafi.ecommerce.ui.main.transaction

import com.nhaarman.mockitokotlin2.whenever
import com.rafi.core.data.repository.FulfillmentRepository
import com.rafi.core.data.source.remote.response.dummyTransactionResponse
import com.rafi.core.util.DataMapper.toTransaction
import com.rafi.core.util.Resources
import com.rafi.ecommerce.util.TestDispatcherRule
import junit.framework.TestCase.assertEquals
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.flow.flowOf
import kotlinx.coroutines.test.advanceUntilIdle
import kotlinx.coroutines.test.runTest
import org.junit.Before
import org.junit.Rule
import org.junit.Test
import org.junit.runner.RunWith
import org.junit.runners.JUnit4
import org.mockito.Mock
import org.mockito.Mockito.mock

@RunWith(JUnit4::class)
class TransactionViewModelTest {

    @OptIn(ExperimentalCoroutinesApi::class)
    @get:Rule
    val testDispatcher = TestDispatcherRule()

    @Mock
    private var fulfillmentRepository: FulfillmentRepository = mock()
    private lateinit var transactionViewModel: TransactionViewModel

    @Before
    fun setup() {
        whenever(fulfillmentRepository.getTransaction())
            .thenReturn(
                flowOf(
                    Resources.Loading,
                    Resources.Success(dummyTransactionResponse.data!!.map { it.toTransaction() })
                )
            )
        transactionViewModel = TransactionViewModel(
            fulfillmentRepository
        )
    }

    @OptIn(ExperimentalCoroutinesApi::class)
    @Test
    fun getListTransaction_thenReturnTransactionData() = runTest {
        // Given
        whenever(fulfillmentRepository.getTransaction())
            .thenReturn(
                flowOf(
                    Resources.Loading,
                    Resources.Success(dummyTransactionResponse.data!!.map { it.toTransaction() })
                )
            )
        transactionViewModel = TransactionViewModel(fulfillmentRepository)

        // When
        transactionViewModel.getTransactionList()
        advanceUntilIdle()

        // Then
        assertEquals(
            Resources.Success(dummyTransactionResponse.data!!.map { it.toTransaction() }),
            transactionViewModel.state.value
        )
    }
}
