package com.rafi.ecommerce.ui.main.store.detail

import androidx.lifecycle.SavedStateHandle
import com.nhaarman.mockitokotlin2.verify
import com.nhaarman.mockitokotlin2.whenever
import com.rafi.core.data.repository.ProductRepository
import com.rafi.core.data.source.remote.response.dummyProductsDetailResponse
import com.rafi.core.util.DataMapper.toCart
import com.rafi.core.util.DataMapper.toProductDetails
import com.rafi.core.util.DataMapper.toWishlist
import com.rafi.core.util.Resources
import com.rafi.ecommerce.util.TestDispatcherRule
import junit.framework.TestCase.assertEquals
import junit.framework.TestCase.assertTrue
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.flow.flowOf
import kotlinx.coroutines.test.advanceUntilIdle
import kotlinx.coroutines.test.runTest
import org.junit.Before
import org.junit.Rule
import org.junit.Test
import org.junit.runner.RunWith
import org.junit.runners.JUnit4
import org.mockito.Mock
import org.mockito.Mockito

@RunWith(JUnit4::class)
class ProductDetailsViewModelTest {

    @get:Rule
    val testDispatcher = TestDispatcherRule()

    @Mock
    private var productRepository: ProductRepository = Mockito.mock()
    private var savedStateHandle: SavedStateHandle =
        SavedStateHandle(mapOf(ProductDetailFragment.BUNDLE_PRODUCT_ID_KEY to "productId"))
    private lateinit var productDetailsViewModel: ProductDetailsViewModel

    @Before
    fun setUp() {
        whenever(productRepository.productDetails("productId"))
            .thenReturn(
                flowOf(
                    Resources.Loading,
                    Resources.Success(dummyProductsDetailResponse.data!!.toProductDetails())
                )
            )
        productDetailsViewModel = ProductDetailsViewModel(
            productRepository, savedStateHandle
        )
    }

    @OptIn(ExperimentalCoroutinesApi::class)
    @Test
    fun `test getProductDetails`() = runTest {
        // Given
        whenever(productRepository.productDetails("productId"))
            .thenReturn(
                flowOf(
                    Resources.Loading,
                    Resources.Success(dummyProductsDetailResponse.data!!.toProductDetails())
                )
            )

        // When
        productDetailsViewModel.getDetailProduct()
        advanceUntilIdle()

        // Then
        assertEquals(
            Resources.Success(dummyProductsDetailResponse.data!!.toProductDetails()),
            productDetailsViewModel.productDetailsState.value
        )
    }

    @Test
    fun `test checkExistWishList`() = runTest {
        // Given
        whenever(productRepository.checkWishListProducts("productId"))
            .thenReturn(true)

        // When
        val expectedResult = productDetailsViewModel.checkExistWishlist()

        // Then
        assertTrue(expectedResult)
    }

    @Test
    fun `test insertToWishLst`() = runTest {
        // Given
        val detailProduct = dummyProductsDetailResponse.data!!.toProductDetails()
        val productVariant = detailProduct.productVariant[0]
        productDetailsViewModel.detailProduct = detailProduct
        productDetailsViewModel.productVariant = productVariant

        // When
        productDetailsViewModel.insertWishlist()

        // Then
        verify(productRepository).insertWishList(detailProduct.toWishlist(productVariant))
    }

    @Test
    fun `test deleteFromWishList`() = runTest {
        // Given
        val detailProduct = dummyProductsDetailResponse.data!!.toProductDetails()
        val productVariant = detailProduct.productVariant[0]
        productDetailsViewModel.detailProduct = detailProduct
        productDetailsViewModel.productVariant = productVariant

        // When
        productDetailsViewModel.deleteWishlist()

        // Then
        verify(productRepository).deleteWishList(detailProduct.toWishlist(productVariant))
    }

    @Test
    fun `test insertCart`() = runTest {
        // Given
        val detailProduct = dummyProductsDetailResponse.data!!.toProductDetails()
        val productVariant = detailProduct.productVariant[0]
        val cart = detailProduct.toCart(productVariant)
        productDetailsViewModel.detailProduct = detailProduct
        productDetailsViewModel.productVariant = productVariant
        whenever(productRepository.isReadyStock(cart)).thenReturn(true)

        // When
        productDetailsViewModel.insertCart()

        // Then
        verify(productRepository).insertCart(cart)
    }
}
