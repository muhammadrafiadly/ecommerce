package com.rafi.ecommerce.ui.main.home

import com.rafi.core.data.repository.UserRepository
import com.rafi.core.data.source.local.room.AppRoomDatabase
import com.rafi.ecommerce.util.TestDispatcherRule
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.test.runTest
import org.junit.Before
import org.junit.Rule
import org.junit.Test
import org.junit.runner.RunWith
import org.junit.runners.JUnit4
import org.mockito.Mock
import org.mockito.Mockito.mock
import org.mockito.kotlin.verify

@RunWith(JUnit4::class)
class HomeViewModelTest {

    @OptIn(ExperimentalCoroutinesApi::class)
    @get:Rule
    val testDispatcher = TestDispatcherRule()

    @Mock
    private var userRepository: UserRepository = mock()
    private var appRoomDatabase: AppRoomDatabase = mock()
    private lateinit var homeViewModel: HomeViewModel

    @Before
    fun setUp() {
        homeViewModel = HomeViewModel(
            userRepository,
            appRoomDatabase
        )
    }

    @Test
    fun `test postLogout`() = runTest {
        homeViewModel.postLogout()
        verify(userRepository).logout()
    }

    @Test
    fun `test clearDB`() = runTest {
        homeViewModel.clearDb()
        verify(appRoomDatabase).clearAllTables()
    }
}
