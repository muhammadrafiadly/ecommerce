package com.rafi.ecommerce.ui.main.fulfillment.rating

import androidx.lifecycle.SavedStateHandle
import com.nhaarman.mockitokotlin2.whenever
import com.rafi.core.data.repository.FulfillmentRepository
import com.rafi.core.data.source.remote.response.dummyFulfillmentResponse
import com.rafi.core.util.DataMapper.toFulfillment
import com.rafi.core.util.Resources
import com.rafi.ecommerce.util.TestDispatcherRule
import junit.framework.TestCase.assertEquals
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.flow.flowOf
import kotlinx.coroutines.test.advanceUntilIdle
import kotlinx.coroutines.test.runTest
import org.junit.Before
import org.junit.Rule
import org.junit.Test
import org.junit.runner.RunWith
import org.junit.runners.JUnit4
import org.mockito.Mock
import org.mockito.Mockito

@RunWith(JUnit4::class)
class RatingViewModelTest {

    @OptIn(ExperimentalCoroutinesApi::class)
    @get:Rule
    val testDispatcher = TestDispatcherRule()

    @Mock
    private var fulfillmentRepository: FulfillmentRepository = Mockito.mock()
    private var savedStateHandle: SavedStateHandle =
        SavedStateHandle(
            mapOf(
                RatingFragment.DATA_BUNDLE_KEY to dummyFulfillmentResponse.data!!.toFulfillment(),
                RatingFragment.RATING_BUNDLE_KEY to 5,
                RatingFragment.REVIEW_BUNDLE_KEY to "review"
            )
        )
    private lateinit var ratingViewModel: RatingViewModel

    @Before
    fun setUp() {
        ratingViewModel = RatingViewModel(
            fulfillmentRepository, savedStateHandle
        )
    }

    @OptIn(ExperimentalCoroutinesApi::class)
    @Test
    fun `test postRating`() = runTest {
        // Given
        val invoiceId = "test"
        val rating = 1
        val review = "test"

        whenever(fulfillmentRepository.rating(invoiceId, rating, review))
            .thenReturn(flowOf(Resources.Loading, Resources.Success(true)))

        // When
        ratingViewModel.postRating(invoiceId, rating, review)
        advanceUntilIdle()

        // Then
        assertEquals(
            Resources.Success(true),
            ratingViewModel.state.value
        )
    }
}
