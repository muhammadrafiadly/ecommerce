package com.rafi.ecommerce.ui.prelogin.register

import com.rafi.core.data.repository.UserRepository
import com.rafi.core.util.Resources
import com.rafi.ecommerce.util.TestDispatcherRule
import junit.framework.TestCase.assertEquals
import kotlinx.coroutines.flow.flowOf
import kotlinx.coroutines.test.advanceUntilIdle
import kotlinx.coroutines.test.runTest
import org.junit.Before
import org.junit.Rule
import org.junit.Test
import org.junit.runner.RunWith
import org.junit.runners.JUnit4
import org.mockito.Mockito
import org.mockito.kotlin.whenever

@RunWith(JUnit4::class)
class RegisterViewModelTest {

    @get:Rule
    val testDispatcher = TestDispatcherRule()

    private var userRepository: UserRepository = Mockito.mock()
    private lateinit var registerViewModel: RegisterViewModel

    @Before
    fun setUp() {
        registerViewModel = RegisterViewModel(userRepository)
    }

    @Test
    fun `test PostRegister`() = runTest {
        val email = "test@example.com"
        val password = "password"

        // Given
        whenever(userRepository.register(email, password))
            .thenReturn(flowOf(Resources.Loading, Resources.Success(true)))

        // When
        registerViewModel.postRegister(email, password)
        advanceUntilIdle()

        // Then
        assertEquals(Resources.Success(true), registerViewModel.state.value)
    }
}
