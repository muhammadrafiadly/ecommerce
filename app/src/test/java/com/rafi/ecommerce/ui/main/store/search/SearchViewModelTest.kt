package com.rafi.ecommerce.ui.main.store.search

import app.cash.turbine.test
import com.nhaarman.mockitokotlin2.whenever
import com.rafi.core.data.repository.ProductRepository
import com.rafi.core.data.source.remote.response.dummySearchResponse
import com.rafi.core.util.Resources
import com.rafi.ecommerce.util.TestDispatcherRule
import junit.framework.TestCase.assertEquals
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.flow.flowOf
import kotlinx.coroutines.test.runTest
import org.junit.Before
import org.junit.Rule
import org.junit.Test
import org.junit.runner.RunWith
import org.junit.runners.JUnit4
import org.mockito.Mock
import org.mockito.Mockito

@RunWith(JUnit4::class)
class SearchViewModelTest {

    @OptIn(ExperimentalCoroutinesApi::class)
    @get:Rule
    val testDispatcher = TestDispatcherRule()

    @Mock
    private var productRepository: ProductRepository = Mockito.mock()
    private lateinit var searchViewModel: SearchViewModel

    @Before
    fun setUp() {
        searchViewModel = SearchViewModel(
            productRepository
        )
    }

    @Test
    fun `test getSearchData`() = runTest {
        // Given
        val query = "test"
        whenever(productRepository.searchProducts(query))
            .thenReturn(
                flowOf(
                    Resources.Loading,
                    Resources.Success(dummySearchResponse.data!!)
                )
            )

        // When & Then
        searchViewModel.searchData.test {
            searchViewModel.getSearchData(query)
            assertEquals(Resources.Loading, awaitItem())
            assertEquals(Resources.Success(dummySearchResponse.data!!), awaitItem())
        }
    }
}
