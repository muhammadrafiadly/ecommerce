package com.rafi.ecommerce.ui.main.store.detail.compose

import androidx.lifecycle.SavedStateHandle
import com.nhaarman.mockitokotlin2.verify
import com.nhaarman.mockitokotlin2.whenever
import com.rafi.core.data.repository.ProductRepository
import com.rafi.core.data.source.remote.response.dummyProductsDetailResponse
import com.rafi.core.util.DataMapper.toCart
import com.rafi.core.util.DataMapper.toProductDetails
import com.rafi.core.util.DataMapper.toWishlist
import com.rafi.core.util.Resources
import com.rafi.ecommerce.util.TestDispatcherRule
import junit.framework.TestCase.assertEquals
import kotlinx.coroutines.flow.flowOf
import kotlinx.coroutines.runBlocking
import kotlinx.coroutines.test.advanceUntilIdle
import kotlinx.coroutines.test.runTest
import org.junit.Before
import org.junit.Rule
import org.junit.Test
import org.junit.runner.RunWith
import org.junit.runners.JUnit4
import org.mockito.Mockito.mock

@RunWith(JUnit4::class)
class ProductDetailsComposeViewModelTest {

    @get:Rule
    val testDispatcher = TestDispatcherRule()

    private lateinit var viewModel: ProductDetailsComposeViewModel
    private var productRepository: ProductRepository = mock()
    private var savedStateHandle: SavedStateHandle =
        SavedStateHandle(mapOf(ProductDetailFragmentCompose.BUNDLE_PRODUCT_ID_KEY to "productId"))

    @Before
    fun setUp() {
        whenever(productRepository.productDetails("productId"))
            .thenReturn(
                flowOf(
                    Resources.Loading,
                    Resources.Success(dummyProductsDetailResponse.data!!.toProductDetails())
                )
            )
        runBlocking {
            whenever(productRepository.checkWishListProducts("productId"))
                .thenReturn(false)
        }
        viewModel = ProductDetailsComposeViewModel(
            productRepository, savedStateHandle
        )
    }

    @Test
    fun `test getDetailProduct`() = runTest {
        val dummy = dummyProductsDetailResponse.data!!.toProductDetails()
        whenever(productRepository.productDetails("productId"))
            .thenReturn(
                flowOf(
                    Resources.Loading,
                    Resources.Success(dummy)
                )
            )
        whenever(productRepository.checkWishListProducts("productId"))
            .thenReturn(false)

        viewModel.getDetailProduct()
        advanceUntilIdle()

        val actualData = viewModel.composeState.value
        val expectedData = DetailComposeState(
            productId = "productId",
            isWishlist = false,
            detailProduct = dummy,
            productVariant = dummy.productVariant[0],
            resultState = Resources.Success(dummy)
        )

        assertEquals(expectedData, actualData)
    }

    @Test
    fun `test insertWishList`() = runTest {
        val detailProduct = dummyProductsDetailResponse.data!!.toProductDetails()
        val productVariant = detailProduct.productVariant[0]

        whenever(productRepository.productDetails("productId"))
            .thenReturn(
                flowOf(
                    Resources.Loading,
                    Resources.Success(detailProduct)
                )
            )
        whenever(productRepository.checkWishListProducts(("productId")))
            .thenReturn(false)

        viewModel = ProductDetailsComposeViewModel(
            productRepository, savedStateHandle
        )

        viewModel.insertWishlist()

        verify(productRepository).insertWishList(detailProduct.toWishlist(productVariant))
    }

    @Test
    fun `test deleteWishList`() = runTest {
        val detailProduct = dummyProductsDetailResponse.data!!.toProductDetails()
        val productVariant = detailProduct.productVariant[0]

        whenever(productRepository.productDetails("productId"))
            .thenReturn(
                flowOf(
                    Resources.Loading,
                    Resources.Success(detailProduct)
                )
            )
        whenever(productRepository.checkWishListProducts(("productId")))
            .thenReturn(false)

        viewModel = ProductDetailsComposeViewModel(
            productRepository, savedStateHandle
        )

        viewModel.deleteWishlist()

        verify(productRepository).deleteWishList(detailProduct.toWishlist(productVariant))
    }

    @Test
    fun `test updateVariant`() = runTest {
        val detailProduct = dummyProductsDetailResponse.data!!.toProductDetails()

        whenever(productRepository.productDetails("productId"))
            .thenReturn(
                flowOf(
                    Resources.Loading,
                    Resources.Success(detailProduct)
                )
            )
        whenever(productRepository.checkWishListProducts(("productId")))
            .thenReturn(false)

        viewModel = ProductDetailsComposeViewModel(
            productRepository, savedStateHandle
        )

        val productVariant = detailProduct.productVariant[1]
        viewModel.updateVariant(productVariant)

        val actualData = viewModel.composeState.value.productVariant

        assertEquals(productVariant, actualData)
    }

    @Test
    fun `test insertCart`() = runTest {
        val detailProduct = dummyProductsDetailResponse.data!!.toProductDetails()
        val productVariant = detailProduct.productVariant[0]
        val cart = detailProduct.toCart(productVariant)

        whenever(productRepository.productDetails("productId"))
            .thenReturn(
                flowOf(
                    Resources.Loading,
                    Resources.Success(detailProduct)
                )
            )
        whenever(productRepository.checkWishListProducts(("productId")))
            .thenReturn(false)

        viewModel = ProductDetailsComposeViewModel(
            productRepository, savedStateHandle
        )

        whenever(productRepository.isReadyStock(cart)).thenReturn(true)
        viewModel.insertCart()

        verify(productRepository).insertCart(cart)
    }
}
