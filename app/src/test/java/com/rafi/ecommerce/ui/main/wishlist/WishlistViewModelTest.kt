package com.rafi.ecommerce.ui.main.wishlist

import com.nhaarman.mockitokotlin2.verify
import com.nhaarman.mockitokotlin2.whenever
import com.rafi.core.data.repository.ProductRepository
import com.rafi.core.data.source.local.room.entity.dummyWishList
import com.rafi.core.util.DataMapper.toCart
import com.rafi.core.util.DataMapper.toWishlist
import com.rafi.ecommerce.util.TestDispatcherRule
import junit.framework.TestCase.assertEquals
import junit.framework.TestCase.assertFalse
import junit.framework.TestCase.assertTrue
import kotlinx.coroutines.flow.first
import kotlinx.coroutines.flow.flowOf
import kotlinx.coroutines.test.runTest
import org.junit.Before
import org.junit.Rule
import org.junit.Test
import org.junit.runner.RunWith
import org.junit.runners.JUnit4
import org.mockito.Mock
import org.mockito.Mockito.mock

@RunWith(JUnit4::class)
class WishlistViewModelTest {

    @get:Rule
    val testDispatcher = TestDispatcherRule()

    @Mock
    private var productRepository: ProductRepository = mock()
    private lateinit var wishlistViewModel: WishlistViewModel

    @Before
    fun setUp() {
        wishlistViewModel = WishlistViewModel(
            productRepository
        )
    }

    @Test
    fun `test wishListProduct`() = runTest {
        // Given
        whenever(productRepository.getWishListProducts())
            .thenReturn(flowOf(dummyWishList.map { it.toWishlist() }))

        // When
        val expectedResult = productRepository.getWishListProducts().first()

        // Then
        verify(productRepository).getWishListProducts()

        assertEquals(
            dummyWishList.map { it.toWishlist() },
            expectedResult
        )
    }

    @Test
    fun `test deleteItemFromWishList`() = runTest {
        wishlistViewModel.deleteItemFromWishList(dummyWishList[0].toWishlist())
        verify(productRepository).deleteWishList(dummyWishList[0].toWishlist())
    }

    @Test
    fun `test addToCart when stock available`() = runTest {
        // Given
        val item = dummyWishList[0].toWishlist()
        val cart = item.toCart()
        whenever(productRepository.isReadyStock(cart))
            .thenReturn(true)

        // When
        val expectedResult = wishlistViewModel.addToCart(item)

        // Then
        verify(productRepository).insertCart(cart)
        assertTrue(expectedResult)
    }

    @Test
    fun `test addToCart when stock unavailable`() = runTest {
        // Given
        val item = dummyWishList[0].toWishlist()
        val cart = item.toCart()
        whenever(productRepository.isReadyStock(cart))
            .thenReturn(false)

        // When
        val actualData = wishlistViewModel.addToCart(item)

        // Then
        assertFalse(actualData)
    }
}
