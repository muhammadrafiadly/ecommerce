package com.rafi.ecommerce.ui.main.store

import com.rafi.core.data.model.ProductQueryModel
import com.rafi.core.data.model.dummyProductQueryModel
import com.rafi.core.data.repository.ProductRepository
import com.rafi.ecommerce.util.TestDispatcherRule
import junit.framework.TestCase.assertEquals
import kotlinx.coroutines.flow.first
import kotlinx.coroutines.test.runTest
import org.junit.Before
import org.junit.Rule
import org.junit.Test
import org.junit.runner.RunWith
import org.junit.runners.JUnit4
import org.mockito.Mock
import org.mockito.Mockito.mock

@RunWith(JUnit4::class)
class StoreViewModelTest {

    @get:Rule
    val testDispatcher = TestDispatcherRule()

    @Mock
    private var productRepository: ProductRepository = mock()
    private lateinit var storeViewModel: StoreViewModel

    @Before
    fun setUp() {
        storeViewModel = StoreViewModel(productRepository)
    }

    @Test
    fun `test getProductFilter`() = runTest {
        // Given
        val queryModel = dummyProductQueryModel.copy(search = "MacBook Pro M1")

        // When
        storeViewModel.getProductFilter(queryModel)

        // Then
        assertEquals(
            queryModel.copy(search = storeViewModel.productsQuery.first().search),
            storeViewModel.productsQuery.first()
        )
    }

    @Test
    fun `test getProductSearch`() = runTest {
        // Given
        val query = "MacBook Pro M1"

        // When
        storeViewModel.getProductSearch(query)

        // Then
        assertEquals(query, storeViewModel.productsQuery.first().search)
    }

    @Test
    fun `test resetProduct`() = runTest {
        // Given
        storeViewModel.sortFilterProduct = 1
        storeViewModel.brandFilterProduct = 2

        // When
        storeViewModel.resetProduct()

        // Then
        assertEquals(ProductQueryModel(), storeViewModel.productsQuery.first())
        assertEquals(null, storeViewModel.sortFilterProduct)
        assertEquals(null, storeViewModel.brandFilterProduct)
    }
}
