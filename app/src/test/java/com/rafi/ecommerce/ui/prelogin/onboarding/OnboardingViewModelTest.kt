package com.rafi.ecommerce.ui.prelogin.onboarding

import com.rafi.core.data.repository.UserRepository
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.test.runTest
import kotlinx.coroutines.test.setMain
import org.junit.Before
import org.junit.Test
import org.junit.runner.RunWith
import org.junit.runners.JUnit4
import org.mockito.Mock
import org.mockito.MockitoAnnotations
import org.mockito.kotlin.verify

@Suppress("DEPRECATION")
@RunWith(JUnit4::class)
class OnboardingViewModelTest {

    @Mock
    private lateinit var userRepository: UserRepository
    private lateinit var onboardingViewModel: OnboardingViewModel

    @OptIn(ExperimentalCoroutinesApi::class)
    @Before
    fun setUp() {
        MockitoAnnotations.initMocks(this)
        Dispatchers.setMain(Dispatchers.Unconfined)
        onboardingViewModel = OnboardingViewModel(userRepository)
    }

    @Test
    fun `test setOnboard`() = runTest {
        userRepository.setOnboarding(false)
        verify(userRepository).setOnboarding(false)
    }
}
