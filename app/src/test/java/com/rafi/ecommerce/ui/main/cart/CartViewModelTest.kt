package com.rafi.ecommerce.ui.main.cart

import com.nhaarman.mockitokotlin2.verify
import com.nhaarman.mockitokotlin2.whenever
import com.rafi.core.data.repository.ProductRepository
import com.rafi.core.data.source.local.room.entity.dummyCartList
import com.rafi.core.util.DataMapper.toCart
import com.rafi.ecommerce.util.TestDispatcherRule
import junit.framework.TestCase.assertEquals
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.flow.first
import kotlinx.coroutines.flow.flowOf
import kotlinx.coroutines.test.runTest
import org.junit.Before
import org.junit.Rule
import org.junit.Test
import org.junit.runner.RunWith
import org.junit.runners.JUnit4
import org.mockito.Mock
import org.mockito.Mockito.mock

@RunWith(JUnit4::class)
class CartViewModelTest {

    @OptIn(ExperimentalCoroutinesApi::class)
    @get:Rule
    val testDispatcher = TestDispatcherRule()

    @Mock
    private var productRepository: ProductRepository = mock()
    private lateinit var cartViewModel: CartViewModel

    @Before
    fun setUp() {
        cartViewModel = CartViewModel(
            productRepository
        )
    }

    @Test
    fun `test getCartData`() = runTest {
        // Given
        whenever(productRepository.getProductCart())
            .thenReturn(flowOf(dummyCartList.map { it.toCart() }))

        // When
        val expectedResult = productRepository.getProductCart().first()

        // Then
        verify(productRepository).getProductCart()

        assertEquals(
            dummyCartList.map { it.toCart() },
            expectedResult
        )
    }

    @Test
    fun `test deleteItemFromCart one item`() = runTest {
        // Given
        val cart = dummyCartList[0].toCart()

        // When
        cartViewModel.deleteItemFromCart(cart)

        // Then
        verify(productRepository).deleteCart(cart)
    }

    @Test
    fun `test deleteItemFromCart many item`() = runTest {
        // Given
        val itemChecked = dummyCartList.map { it.copy(isChecked = (true)).toCart() }
        whenever(productRepository.getProductCart())
            .thenReturn(flowOf(itemChecked))

        // When
        cartViewModel.deleteItemFromCart()

        // Then
        verify(productRepository).deleteCart(*itemChecked.toTypedArray())
    }

    @Test
    fun `test updateCartQuantity insert item`() = runTest {
        val cart = dummyCartList[0].toCart()
        val isInsert = true

        cartViewModel.updateCartQuantity(cart, isInsert)

        verify(productRepository).updateCartQuantity(cart, isInsert)
    }

    @Test
    fun `test updateCartQuantity remove item`() = runTest {
        val cart = dummyCartList[0].toCart()
        val isInsert = false

        cartViewModel.updateCartQuantity(cart, isInsert)

        verify(productRepository).updateCartQuantity(cart, isInsert)
    }

    @Test
    fun `test updateCartChecked one item`() = runTest {
        val cart = dummyCartList[0].toCart()
        val isChecked = true

        cartViewModel.updateCartChecked(isChecked, cart)
        verify(productRepository).updateCartChecked(isChecked, cart)
    }

    @Test
    fun `test updateCartChecked many item`() = runTest {
        val isChecked = true
        val listCheckedCart = dummyCartList.map { it.toCart() }
        whenever(productRepository.getProductCart())
            .thenReturn(flowOf(listCheckedCart))

        cartViewModel.updateCartChecked(isChecked)

        verify(productRepository).updateCartChecked(isChecked, *listCheckedCart.toTypedArray())
    }
}
