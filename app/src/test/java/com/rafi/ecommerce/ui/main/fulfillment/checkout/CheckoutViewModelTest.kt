package com.rafi.ecommerce.ui.main.fulfillment.checkout

import androidx.lifecycle.SavedStateHandle
import com.nhaarman.mockitokotlin2.verify
import com.nhaarman.mockitokotlin2.whenever
import com.rafi.core.data.model.PaymentModel
import com.rafi.core.data.repository.FulfillmentRepository
import com.rafi.core.data.repository.ProductRepository
import com.rafi.core.data.source.local.room.entity.dummyCartList
import com.rafi.core.data.source.remote.response.dummyFulfillmentResponse
import com.rafi.core.util.DataMapper.toCart
import com.rafi.core.util.DataMapper.toFulfillment
import com.rafi.core.util.Resources
import com.rafi.ecommerce.util.TestDispatcherRule
import junit.framework.TestCase.assertEquals
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.flow.first
import kotlinx.coroutines.flow.flowOf
import kotlinx.coroutines.test.advanceUntilIdle
import kotlinx.coroutines.test.runTest
import org.junit.Before
import org.junit.Rule
import org.junit.Test
import org.junit.runner.RunWith
import org.junit.runners.JUnit4
import org.mockito.Mock
import org.mockito.Mockito.mock

@RunWith(JUnit4::class)
class CheckoutViewModelTest {

    @get:Rule
    val testDispatcher = TestDispatcherRule()

    @Mock
    private var fulfillmentRepository: FulfillmentRepository = mock()
    private var productRepository: ProductRepository = mock()
    private var savedStateHandle: SavedStateHandle =
        SavedStateHandle(mapOf(CheckoutFragment.ARG_DATA to dummyCartList.map { it.toCart() }))
    private lateinit var checkoutViewModel: CheckoutViewModel

    @Before
    fun setUp() {
        checkoutViewModel = CheckoutViewModel(
            fulfillmentRepository,
            productRepository,
            savedStateHandle
        )
    }

    @Test
    fun `test getCartData`() = runTest {
        // Given
        whenever(productRepository.getProductCart())
            .thenReturn(flowOf(dummyCartList.map { it.toCart() }))

        // When
        val expectedResult = productRepository.getProductCart().first()

        // Then
        verify(productRepository).getProductCart()

        assertEquals(
            dummyCartList.map { it.toCart() },
            expectedResult
        )
    }

    @Test
    fun `test setData`() = runTest {
        // Given
        val expectedResult = checkoutViewModel.listData.first()

        // When & Then
        assertEquals(dummyCartList.map { it.toCart() }, expectedResult)
    }

    @Test
    fun `test updateProductQuantity when insert item`() {
        // Given
        var listCart = dummyCartList.map { it.toCart() }
        val cart = dummyCartList[0].toCart()

        // WHen
        checkoutViewModel.updateProductQuantity(cart, true)

        // Then
        listCart = listCart.map {
            if (it == cart) {
                it.copy(quantity = it.quantity!!.plus(1))
            } else {
                it
            }
        }
        assertEquals(
            listCart,
            checkoutViewModel.listData.value
        )
    }

    @Test
    fun `test updateProductQuantity when remove item`() {
        // Given
        var listCart = dummyCartList.map { it.toCart() }
        val cart = dummyCartList[0].toCart()

        // WHen
        checkoutViewModel.updateProductQuantity(cart, false)

        // Then
        listCart = listCart.map {
            if (it == cart) {
                it.copy(quantity = it.quantity!!.plus(-1))
            } else {
                it
            }
        }
        assertEquals(
            listCart,
            checkoutViewModel.listData.value
        )
    }

    @OptIn(ExperimentalCoroutinesApi::class)
    @Test
    fun `test postPayment`() = runTest {
        // Given
        val dummyPaymentItem = PaymentModel.PaymentItem(
            "test",
            "test",
            true
        )
        val data = dummyCartList.map { it.toCart() }
        checkoutViewModel.paymentItem = dummyPaymentItem
        whenever(fulfillmentRepository.fulfillment(dummyPaymentItem.label, data))
            .thenReturn(
                flowOf(
                    Resources.Loading,
                    Resources.Success(dummyFulfillmentResponse.data!!.toFulfillment())
                )
            )

        // When
        checkoutViewModel.postPayment()
        advanceUntilIdle()

        // Then
        assertEquals(
            Resources.Success(
                dummyFulfillmentResponse.data!!.toFulfillment()
            ),
            checkoutViewModel.state.value
        )
    }

    @Test
    fun `test deleteItemFromCart when checkout`() = runTest {
        // Given
        val item = dummyCartList.map { it.copy(isChecked = (true)).toCart() }
        whenever(productRepository.getProductCart())
            .thenReturn(flowOf(item))

        // When
        checkoutViewModel.deleteItemFromCart()

        // Then
        verify(productRepository).deleteCart(*item.toTypedArray())
    }
}
