package com.rafi.ecommerce.ui.profile

import com.nhaarman.mockitokotlin2.mock
import com.rafi.core.data.repository.UserRepository
import com.rafi.core.util.Resources
import com.rafi.ecommerce.util.TestDispatcherRule
import junit.framework.TestCase.assertEquals
import kotlinx.coroutines.flow.flowOf
import kotlinx.coroutines.test.advanceUntilIdle
import kotlinx.coroutines.test.runTest
import okhttp3.RequestBody.Companion.toRequestBody
import org.junit.Before
import org.junit.Rule
import org.junit.Test
import org.junit.runner.RunWith
import org.junit.runners.JUnit4
import org.mockito.kotlin.whenever

@RunWith(JUnit4::class)
class ProfileViewModelTest {

    @get:Rule
    val testDispatcherRule = TestDispatcherRule()

    private lateinit var viewModel: ProfileViewModel
    private var userRepository: UserRepository = mock()

    @Before
    fun setup() {
        viewModel = ProfileViewModel(
            userRepository
        )
    }

    @Test
    fun `test uploadProfile`() = runTest {
        val userName = "name".toRequestBody()
        val userImage = null
        whenever(userRepository.uploadProfile(userName, userImage))
            .thenReturn(flowOf(Resources.Loading, Resources.Success(true)))

        viewModel.uploadProfile(userName, userImage)
        advanceUntilIdle()

        assertEquals(Resources.Success(true), viewModel.state.value)
    }
}
