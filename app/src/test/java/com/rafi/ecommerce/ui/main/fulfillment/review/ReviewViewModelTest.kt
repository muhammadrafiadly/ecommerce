package com.rafi.ecommerce.ui.main.fulfillment.review

import androidx.lifecycle.SavedStateHandle
import com.nhaarman.mockitokotlin2.whenever
import com.rafi.core.data.repository.ProductRepository
import com.rafi.core.data.source.remote.response.dummyProductsReviewResponse
import com.rafi.core.util.DataMapper.toProductReviews
import com.rafi.core.util.Resources
import com.rafi.ecommerce.util.TestDispatcherRule
import junit.framework.TestCase.assertEquals
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.flow.flowOf
import kotlinx.coroutines.test.advanceUntilIdle
import kotlinx.coroutines.test.runTest
import org.junit.Before
import org.junit.Rule
import org.junit.Test
import org.junit.runner.RunWith
import org.junit.runners.JUnit4
import org.mockito.Mock
import org.mockito.Mockito

@RunWith(JUnit4::class)
class ReviewViewModelTest {

    @OptIn(ExperimentalCoroutinesApi::class)
    @get:Rule
    val testDispatcher = TestDispatcherRule()

    @Mock
    private var productRepository: ProductRepository = Mockito.mock()
    private var savedStateHandle: SavedStateHandle =
        SavedStateHandle(mapOf(ReviewFragment.BUNDLE_PRODUCT_ID_KEY to "productId"))
    private lateinit var reviewViewModel: ReviewViewModel

    @Before
    fun setup() {
        whenever(productRepository.reviewProduct("productId"))
            .thenReturn(
                flowOf(
                    Resources.Loading,
                    Resources.Success(dummyProductsReviewResponse.data!!.map { it.toProductReviews() })
                )
            )
        reviewViewModel = ReviewViewModel(
            productRepository, savedStateHandle
        )
    }

    @OptIn(ExperimentalCoroutinesApi::class)
    @Test
    fun `test getListReview`() = runTest {
        // Given
        whenever(productRepository.reviewProduct("productId"))
            .thenReturn(
                flowOf(
                    Resources.Loading,
                    Resources.Success(dummyProductsReviewResponse.data!!.map { it.toProductReviews() })
                )
            )

        // When
        reviewViewModel.getListReview()
        advanceUntilIdle()

        // Then
        assertEquals(
            Resources.Success(dummyProductsReviewResponse.data!!.map { it.toProductReviews() }),
            reviewViewModel.reviewProductState.value
        )
    }
}
