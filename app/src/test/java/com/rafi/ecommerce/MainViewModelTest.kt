package com.rafi.ecommerce

import com.rafi.core.data.repository.UserRepository
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.test.runTest
import kotlinx.coroutines.test.setMain
import org.junit.Before
import org.junit.Test
import org.junit.runner.RunWith
import org.junit.runners.JUnit4
import org.mockito.Mock
import org.mockito.MockitoAnnotations
import org.mockito.kotlin.verify

@Suppress("DEPRECATION")
@RunWith(JUnit4::class)
class MainViewModelTest {

    @Mock
    private lateinit var userRepository: UserRepository
    private lateinit var mainViewModel: MainViewModel

    @OptIn(ExperimentalCoroutinesApi::class)
    @Before
    fun setUp() {
        MockitoAnnotations.initMocks(this)
        Dispatchers.setMain(Dispatchers.Unconfined)
        mainViewModel = MainViewModel(userRepository)
    }

    @Test
    fun `test checkLogin`() = runTest {
        userRepository.userAuth()
        verify(userRepository).userAuth()
    }
}
