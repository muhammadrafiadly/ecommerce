package com.rafi.ecommerce

import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.rafi.core.data.repository.UserRepository
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.delay
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.asStateFlow
import kotlinx.coroutines.launch
import javax.inject.Inject

@HiltViewModel
class MainViewModel @Inject constructor(
    private val userRepository: UserRepository
) : ViewModel() {

    private val _isReady = MutableStateFlow(false)
    val isReady = _isReady.asStateFlow()

    init {
        viewModelScope.launch {
            delay(1500L)
            _isReady.value = true
        }
    }

    fun checkUserAuthorization(): Flow<Boolean> {
        return userRepository.userAuth()
    }

    fun getAppTheme() = userRepository.getAppTheme()

    fun getAppLanguage() = userRepository.getAppLanguage()

    fun makeLogout() {
        viewModelScope.launch { userRepository.logout() }
    }
}
