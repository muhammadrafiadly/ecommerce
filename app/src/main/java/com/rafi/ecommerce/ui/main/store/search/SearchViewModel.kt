package com.rafi.ecommerce.ui.main.store.search

import androidx.lifecycle.ViewModel
import com.rafi.core.data.repository.ProductRepository
import com.rafi.core.util.Resources
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.FlowPreview
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.StateFlow
import kotlinx.coroutines.flow.asStateFlow
import kotlinx.coroutines.flow.debounce
import kotlinx.coroutines.flow.flatMapLatest
import kotlinx.coroutines.runBlocking
import javax.inject.Inject

@HiltViewModel
class SearchViewModel @Inject constructor(
    private val productRepository: ProductRepository
) : ViewModel() {

    private val _state = MutableStateFlow<Resources<Boolean>?>(null)
    val state: StateFlow<Resources<Boolean>?> = _state.asStateFlow()

    private val _searchEditText: MutableStateFlow<String> = MutableStateFlow("")

    @OptIn(FlowPreview::class)
    private val _searchQuery: Flow<String> = _searchEditText.debounce(1000)

    @OptIn(ExperimentalCoroutinesApi::class)
    val searchData: Flow<Resources<List<String>>> = _searchQuery.flatMapLatest { query ->
        productRepository.searchProducts(query)
    }

    fun getSearchData(query: String) {
        runBlocking {
            _searchEditText.value = query
        }
    }
}
