package com.rafi.ecommerce.ui.main.cart

import android.annotation.SuppressLint
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.core.content.ContextCompat
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.ListAdapter
import androidx.recyclerview.widget.RecyclerView
import coil.load
import com.rafi.core.data.model.ProductCartModel
import com.rafi.ecommerce.R
import com.rafi.ecommerce.databinding.ItemProductCartBinding
import com.rafi.ecommerce.util.toCurrencyFormat

class CartAdapter(private val callback: CartCallback) :
    ListAdapter<ProductCartModel, CartAdapter.CartViewHolder>(CartComparator) {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): CartViewHolder {
        val binding = ItemProductCartBinding.inflate(
            LayoutInflater
                .from(parent.context),
            parent,
            false
        )
        return CartViewHolder(binding, callback)
    }

    override fun onBindViewHolder(holder: CartViewHolder, position: Int) {
        holder.bind(getItem(position))
    }

    class CartViewHolder(
        private val binding: ItemProductCartBinding,
        private val callback: CartCallback
    ) :
        RecyclerView.ViewHolder(binding.root) {
        @SuppressLint("StringFormatMatches")
        fun bind(data: ProductCartModel) {
            with(binding) {
                ivCartImage.load(data.image) {
                    crossfade(true)
                    placeholder(R.drawable.product_image_placeholder)
                    error(R.drawable.product_image_placeholder)
                }
                tvCartName.text = data.productName
                tvCartVariant.text = data.variantName
                tvCartStock.text = if (data.stock <= 10) {
                    tvCartStock.setTextColor(ContextCompat.getColor(itemView.context, R.color.red))
                    itemView.context.getString(R.string.stock, data.stock)
                } else {
                    itemView.context.getString(R.string.stock, data.stock)
                }
                tvCartPrice.text = data.productPrice.plus(data.variantPrice).toCurrencyFormat()
                tvCartQuantity.text = data.quantity.toString()
                cbItemCart.isChecked = data.isChecked
                cbItemCart.setOnClickListener {
                    callback.checkItemCallback(data, !data.isChecked)
                }
                btnMinusCart.setOnClickListener {
                    callback.decreaseQuantityCallback(data)
                }
                btnPlusCart.setOnClickListener {
                    callback.increaseQuantityCallback(data)
                }
                btnDeleteCart.setOnClickListener {
                    callback.deleteItemCallback(data)
                }
            }
            itemView.setOnClickListener {
                callback.itemClickCallback(data.productId)
            }
        }
    }

    interface CartCallback {
        fun itemClickCallback(productId: String)
        fun increaseQuantityCallback(cart: ProductCartModel)
        fun deleteItemCallback(cart: ProductCartModel)
        fun decreaseQuantityCallback(cart: ProductCartModel)
        fun checkItemCallback(cart: ProductCartModel, isCheck: Boolean)
    }

    object CartComparator : DiffUtil.ItemCallback<ProductCartModel>() {
        override fun areItemsTheSame(
            oldItem: ProductCartModel,
            newItem: ProductCartModel
        ): Boolean =
            oldItem.productId == newItem.productId

        override fun areContentsTheSame(
            oldItem: ProductCartModel,
            newItem: ProductCartModel
        ): Boolean =
            oldItem == newItem
    }
}
