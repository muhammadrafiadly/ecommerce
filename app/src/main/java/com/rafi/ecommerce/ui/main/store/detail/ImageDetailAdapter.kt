package com.rafi.ecommerce.ui.main.store.detail

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.ListAdapter
import androidx.recyclerview.widget.RecyclerView
import coil.load
import com.rafi.ecommerce.databinding.ItemImageDetailBinding

class ImageDetailAdapter :
    ListAdapter<String, ImageDetailAdapter.ImageDetailViewHolder>(ImageDetailDiffUtil()) {

    class ImageDetailViewHolder(private val binding: ItemImageDetailBinding) :
        RecyclerView.ViewHolder(binding.root) {
        fun bind(data: String) {
            binding.ivImageDetail.load(data) {
                crossfade(true)
            }
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ImageDetailViewHolder {
        val binding = ItemImageDetailBinding.inflate(
            LayoutInflater.from(parent.context),
            parent,
            false
        )
        return ImageDetailViewHolder(binding)
    }

    override fun onBindViewHolder(holder: ImageDetailViewHolder, position: Int) {
        holder.bind(getItem(position))
    }

    class ImageDetailDiffUtil : DiffUtil.ItemCallback<String>() {
        override fun areItemsTheSame(oldItem: String, newItem: String): Boolean = oldItem == newItem

        override fun areContentsTheSame(oldItem: String, newItem: String): Boolean =
            oldItem == newItem
    }
}
