package com.rafi.ecommerce.ui.main.store

import android.os.Bundle
import android.view.View
import androidx.core.os.bundleOf
import androidx.core.view.isVisible
import androidx.fragment.app.viewModels
import androidx.lifecycle.Lifecycle
import androidx.lifecycle.lifecycleScope
import androidx.lifecycle.repeatOnLifecycle
import androidx.navigation.Navigation
import androidx.paging.LoadState
import androidx.recyclerview.widget.GridLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.google.android.material.chip.Chip
import com.google.firebase.analytics.FirebaseAnalytics
import com.google.firebase.analytics.FirebaseAnalytics.Param
import com.google.firebase.analytics.ktx.analytics
import com.google.firebase.analytics.ktx.logEvent
import com.google.firebase.ktx.Firebase
import com.rafi.core.data.model.ProductModel
import com.rafi.core.data.model.ProductQueryModel
import com.rafi.core.util.Constant
import com.rafi.ecommerce.R
import com.rafi.ecommerce.databinding.FragmentStoreBinding
import com.rafi.ecommerce.ui.main.store.detail.compose.ProductDetailFragmentCompose
import com.rafi.ecommerce.ui.main.store.search.SearchFragment
import com.rafi.ecommerce.ui.main.store.sheet.BottomSheetFragment
import com.rafi.ecommerce.util.BaseFragment
import com.rafi.ecommerce.util.toCurrencyFormat
import dagger.hilt.android.AndroidEntryPoint
import kotlinx.coroutines.launch
import retrofit2.HttpException
import java.io.IOException

@Suppress("DEPRECATION")
@AndroidEntryPoint
class StoreFragment : BaseFragment<FragmentStoreBinding>(FragmentStoreBinding::inflate) {

    private val storeViewModel: StoreViewModel by viewModels()

    private val productAdapter by lazy {
        ProductAdapter { product ->
            logSelectItemEvent(product)
            Navigation.findNavController(requireActivity(), R.id.navHostFragment).navigate(
                R.id.action_mainFragment_to_productDetailFragmentCompose,
                bundleOf(
                    ProductDetailFragmentCompose.BUNDLE_PRODUCT_ID_KEY to product.productId
                )
            )
        }
    }

    private val loadingStateAdapter = LoadingStateAdapter()

    private lateinit var gridLayoutManager: GridLayoutManager

    private val firebaseAnalytics: FirebaseAnalytics by lazy {
        Firebase.analytics
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        initFragmentResultListener()
        initAdapter()
        initChangeButton()
//        initRecyclerView()
        initChipView()
        initSearchEditText()
        initAction()
        observeProducts()
        changeRecyclerViewType()
    }

    private fun logViewItemEvent(productList: List<ProductModel>) {
        firebaseAnalytics.logEvent(FirebaseAnalytics.Event.VIEW_ITEM_LIST) {
            val bundle = ArrayList<Bundle>()
            productList.map { product ->
                val itemBundle = Bundle().apply {
                    putString(Param.ITEM_ID, product.productId)
                    putString(Param.ITEM_NAME, product.productName)
                    putString(Param.ITEM_BRAND, product.brand)
                }
                bundle.add(itemBundle)
            }
            param(Param.ITEMS, bundle.toTypedArray())
        }
    }

    private fun logSelectItemEvent(product: ProductModel) {
        firebaseAnalytics.logEvent(FirebaseAnalytics.Event.SELECT_ITEM) {
            val itemBundle = Bundle().apply {
                putString(Param.ITEM_ID, product.productId)
                putString(Param.ITEM_NAME, product.productName)
                putString(Param.ITEM_BRAND, product.brand)
            }
            param(Param.ITEMS, arrayOf(itemBundle))
        }
    }

    private fun initAdapter() {
        gridLayoutManager = GridLayoutManager(requireActivity(), 1)

        binding.includeProduct.rvProduct.adapter =
            productAdapter.withLoadStateFooter(loadingStateAdapter)
        binding.includeProduct.rvProduct.layoutManager = gridLayoutManager

        productAdapter.registerAdapterDataObserver(object : RecyclerView.AdapterDataObserver() {
            override fun onItemRangeMoved(fromPosition: Int, toPosition: Int, itemCount: Int) {
                super.onItemRangeMoved(fromPosition, toPosition, itemCount)
                binding.includeProduct.rvProduct.scrollToPosition(0)
            }

            override fun onItemRangeInserted(positionStart: Int, itemCount: Int) {
                super.onItemRangeInserted(positionStart, itemCount)
                logViewItemEvent(productAdapter.snapshot().items)
            }
        })

        gridLayoutManager.spanSizeLookup = object : GridLayoutManager.SpanSizeLookup() {
            override fun getSpanSize(position: Int): Int {
                return if (storeViewModel.viewType == ProductAdapter.GRID_TYPE) {
                    if (position == productAdapter.itemCount && loadingStateAdapter.itemCount > 0) {
                        2
                    } else {
                        1
                    }
                } else {
                    1
                }
            }
        }
    }

    private fun initFragmentResultListener() {
        getSearchFragmentResult()
        getFilterFragmentResult()
    }

    private fun getSearchFragmentResult() {
        childFragmentManager.setFragmentResultListener(
            SearchFragment.FRAGMENT_REQUEST_KEY,
            viewLifecycleOwner
        ) { _, bundle ->
            val query = bundle.getString(SearchFragment.BUNDLE_QUERY_KEY)
            storeViewModel.getProductSearch(query)
            initSearchEditText()
        }
    }

    private fun getFilterFragmentResult() {
        childFragmentManager.setFragmentResultListener(
            BottomSheetFragment.FRAGMENT_REQUEST_KEY,
            viewLifecycleOwner
        ) { _, bundle ->
            val productFilter = ProductQueryModel(
                sort = bundle.getString(Constant.Filter.SORT_KEY),
                brand = bundle.getString(Constant.Filter.BRAND_KEY),
                lowest = bundle.getString(Constant.Filter.LOWEST_KEY),
                highest = bundle.getString(Constant.Filter.HIGHEST_KEY)
            )
            storeViewModel.sortFilterProduct =
                bundle.getInt(Constant.Filter.SORT_ID_KEY, -1).let {
                    if (it == -1) null else it
                }
            storeViewModel.brandFilterProduct =
                bundle.getInt(Constant.Filter.BRAND_ID_KEY, -1).let {
                    if (it == -1) null else it
                }

            storeViewModel.getProductFilter(productFilter)
            initChipView()
        }
    }

    private fun initAction() {
        binding.apply {
            includeProduct.btnChangeRecyclerView.setOnClickListener {
                actionChangeRecyclerView()
            }
            includeProduct.chipFilter.setOnClickListener {
                actionOpenFilter()
            }
            etSearch.setOnClickListener {
                actionOpenSearch()
            }
            swipeRefresh.setOnRefreshListener {
                productAdapter.refresh()
                swipeRefresh.isRefreshing = false
            }
        }
    }

    private fun initSearchEditText() {
        binding.etSearch.setText(storeViewModel.productsQuery.value.search ?: "")
    }

    private fun actionOpenFilter() {
        with(storeViewModel.productsQuery.value) {
            val filterSheetFragment = BottomSheetFragment.newInstance(
                storeViewModel.sortFilterProduct?.let { getString(it) },
                storeViewModel.brandFilterProduct?.let { getString(it) },
                lowest,
                highest
            )
            filterSheetFragment.show(childFragmentManager, BottomSheetFragment.TAG)
        }
    }

    private fun actionOpenSearch() {
        val searchFragment = SearchFragment.newInstance(
            storeViewModel.productsQuery.value.search
        )
        searchFragment.show(childFragmentManager, SearchFragment.TAG)
    }

    private fun actionChangeRecyclerView() {
        storeViewModel.viewType =
            if (storeViewModel.viewType == ProductAdapter.LINEAR_TYPE) {
                ProductAdapter.GRID_TYPE
            } else {
                ProductAdapter.LINEAR_TYPE
            }
        changeRecyclerViewType()
        initChangeButton()
    }

    private fun initChangeButton() {
        binding.includeProduct.btnChangeRecyclerView.setImageResource(
            if (storeViewModel.viewType == ProductAdapter.LINEAR_TYPE) {
                R.drawable.ic_list_bulleted
            } else {
                R.drawable.ic_grid
            }
        )
    }

    private fun initRecyclerView() {
        if (storeViewModel.viewType == ProductAdapter.LINEAR_TYPE) {
            productAdapter.viewType = ProductAdapter.LINEAR_TYPE
            binding.includeProduct.rvProduct.layoutManager =
                GridLayoutManager(requireActivity(), 1)
        } else {
            productAdapter.viewType = ProductAdapter.GRID_TYPE
            val lmGrid = GridLayoutManager(requireActivity(), 2)
            lmGrid.spanSizeLookup = object : GridLayoutManager.SpanSizeLookup() {
                override fun getSpanSize(position: Int): Int {
                    return if (position == productAdapter.itemCount && loadingStateAdapter.itemCount > 0) {
                        2
                    } else {
                        1
                    }
                }
            }
            binding.includeProduct.rvProduct.layoutManager = lmGrid
        }
    }

    private fun changeRecyclerViewType() {
        if (storeViewModel.viewType == ProductAdapter.LINEAR_TYPE) {
            productAdapter.viewType = ProductAdapter.LINEAR_TYPE
            gridLayoutManager.spanCount = 1
        } else {
            productAdapter.viewType = ProductAdapter.GRID_TYPE
            gridLayoutManager.spanCount = 2
        }
    }

    private fun observeProducts() {
        productAdapter.addLoadStateListener { state ->
            val loading = state.refresh
            initShimmer(loading is LoadState.Loading)
            if (loading is LoadState.Error) {
                initErrorMessage(loading.error)
            }
        }
        viewLifecycleOwner.lifecycleScope.launch {
            repeatOnLifecycle(Lifecycle.State.STARTED) {
                storeViewModel.productsData.collect { pagingData ->
                    productAdapter.submitData(pagingData)
//                    initRecyclerView()
                }
            }
        }
    }

    private fun initChipView() {
        binding.includeProduct.chipFilterGroup.removeAllViews()

        val filterList = arrayListOf<String>()
        with(storeViewModel.productsQuery.value) {
            this.sort?.let { filterList.add(getString(storeViewModel.sortFilterProduct!!)) }
            this.brand?.let { filterList.add(getString(storeViewModel.brandFilterProduct!!)) }
            this.lowest?.let { filterList.add("> ${it.toInt().toCurrencyFormat()}") }
            this.highest?.let { filterList.add("< ${it.toInt().toCurrencyFormat()}") }
        }

        for (filter in filterList) {
            val chip = Chip(binding.includeProduct.chipFilterGroup.context)
            chip.text = filter
            chip.isClickable = false
            chip.isCheckable = false
            binding.includeProduct.chipFilterGroup.addView(chip)
        }
    }

    private fun initShimmer(isLoading: Boolean) {
        with(binding) {
            includeShimmer.shimmerLinear.isVisible =
                storeViewModel.viewType == ProductAdapter.LINEAR_TYPE
            includeShimmer.shimmerGrid.isVisible =
                storeViewModel.viewType == ProductAdapter.GRID_TYPE

            includeShimmer.containerShimmer.isVisible = isLoading
            includeProduct.containerProduct.isVisible = !isLoading
            includeErrorMessage.containerErrorMessage.isVisible = false

            if (isLoading) {
                includeShimmer.containerShimmer.startShimmer()
            } else if (includeShimmer.containerShimmer.isShimmerStarted) {
                includeShimmer.containerShimmer.stopShimmer()
            }
        }
    }

    private fun initErrorMessage(error: Throwable) {
        val title: String
        val desc: String
        val buttonTitle: String
        when (error) {
            is HttpException -> {
                if (error.response()?.code() == 404) {
                    title = getString(R.string.empty_title)
                    desc = getString(R.string.empty_data_desc)
                    buttonTitle = getString(R.string.reset_btn)
                } else {
                    title = error.code().toString()
                    desc = error.response()?.message().toString()
                    buttonTitle = getString(R.string.refresh_btn)
                }
            }

            is IOException -> {
                title = getString(R.string.connection_title)
                desc = getString(R.string.io_exception_desc)
                buttonTitle = getString(R.string.refresh_btn)
            }

            else -> {
                title = getString(R.string.error_title)
                desc = error.message.toString()
                buttonTitle = getString(R.string.refresh_btn)
            }
        }

        binding.includeProduct.containerProduct.isVisible = false
        with(binding.includeErrorMessage) {
            containerErrorMessage.isVisible = true

            tvErrorTitle.text = title
            tvErrorDesc.text = desc
            btnError.text = buttonTitle

            btnError.setOnClickListener {
                when (buttonTitle) {
                    getString(R.string.reset_btn) -> {
                        storeViewModel.resetProduct()
                        initChipView()
                        initSearchEditText()
                    }

                    getString(R.string.refresh_btn) -> {
                        productAdapter.refresh()
                    }
                }
            }
        }
    }
}
