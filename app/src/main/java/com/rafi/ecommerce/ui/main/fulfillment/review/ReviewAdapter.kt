package com.rafi.ecommerce.ui.main.fulfillment.review

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.ListAdapter
import androidx.recyclerview.widget.RecyclerView
import coil.load
import coil.transform.CircleCropTransformation
import com.rafi.core.data.model.ProductReviewsModel
import com.rafi.ecommerce.R
import com.rafi.ecommerce.databinding.ItemBuyerReviewBinding

class ReviewAdapter :
    ListAdapter<ProductReviewsModel, ReviewAdapter.ReviewViewHolder>(ReviewImageDiffUtil()) {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ReviewViewHolder {
        val binding =
            ItemBuyerReviewBinding.inflate(LayoutInflater.from(parent.context), parent, false)
        return ReviewViewHolder(binding)
    }

    override fun onBindViewHolder(holder: ReviewViewHolder, position: Int) {
        holder.bind(getItem(position))
    }

    class ReviewViewHolder(private val binding: ItemBuyerReviewBinding) :
        RecyclerView.ViewHolder(binding.root) {
        fun bind(data: ProductReviewsModel) {
            with(binding) {
                ivReviewer.load(data.userImage) {
                    crossfade(true)
                    placeholder(R.drawable.product_image_placeholder)
                    error(R.drawable.product_image_placeholder)
                    transformations(CircleCropTransformation())
                }
                tvReviewerName.text = data.userName
                tvReviewDesc.text = data.userReview
                ratingBar.rating = data.userRating.toFloat()
            }
        }
    }

    class ReviewImageDiffUtil : DiffUtil.ItemCallback<ProductReviewsModel>() {
        override fun areItemsTheSame(
            oldItem: ProductReviewsModel,
            newItem: ProductReviewsModel
        ): Boolean =
            oldItem.userName == newItem.userName

        override fun areContentsTheSame(
            oldItem: ProductReviewsModel,
            newItem: ProductReviewsModel
        ): Boolean =
            oldItem == newItem
    }
}
