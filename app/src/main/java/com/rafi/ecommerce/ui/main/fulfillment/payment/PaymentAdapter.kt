package com.rafi.ecommerce.ui.main.fulfillment.payment

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.ListAdapter
import androidx.recyclerview.widget.RecyclerView
import com.rafi.core.data.model.PaymentModel
import com.rafi.ecommerce.databinding.ItemPaymentTypeBinding

class PaymentAdapter(
    private val subItemCallback: (PaymentModel.PaymentItem) -> Unit
) : ListAdapter<PaymentModel, PaymentAdapter.PaymentViewHolder>(PaymentComparator) {
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): PaymentViewHolder {
        val binding = ItemPaymentTypeBinding.inflate(
            LayoutInflater.from(parent.context),
            parent,
            false
        )
        return PaymentViewHolder(binding, subItemCallback)
    }

    override fun onBindViewHolder(holder: PaymentViewHolder, position: Int) {
        holder.bind(getItem(position))
    }

    class PaymentViewHolder(
        private val binding: ItemPaymentTypeBinding,
        subItemClickCallback: (PaymentModel.PaymentItem) -> Unit
    ) : RecyclerView.ViewHolder(binding.root) {
        private val paymentItemAdapter = PaymentItemAdapter(subItemClickCallback)

        fun bind(data: PaymentModel) {
            paymentItemAdapter.submitList(data.paymentItem)
            binding.tvTransferType.text = data.title
            binding.rvPaymentList.apply {
                adapter = paymentItemAdapter
                layoutManager = object : LinearLayoutManager(itemView.context) {
                    override fun canScrollVertically(): Boolean {
                        return false
                    }
                }
            }
        }
    }

    object PaymentComparator : DiffUtil.ItemCallback<PaymentModel>() {
        override fun areItemsTheSame(oldItem: PaymentModel, newItem: PaymentModel): Boolean =
            oldItem.title == newItem.title

        override fun areContentsTheSame(oldItem: PaymentModel, newItem: PaymentModel): Boolean =
            oldItem == newItem
    }
}
