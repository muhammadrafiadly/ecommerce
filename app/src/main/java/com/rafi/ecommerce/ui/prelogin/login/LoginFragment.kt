package com.rafi.ecommerce.ui.prelogin.login

import android.os.Bundle
import android.util.Patterns
import android.view.View
import androidx.core.view.isInvisible
import androidx.core.widget.doOnTextChanged
import androidx.fragment.app.viewModels
import androidx.lifecycle.lifecycleScope
import androidx.navigation.fragment.findNavController
import com.google.firebase.analytics.FirebaseAnalytics
import com.google.firebase.analytics.ktx.analytics
import com.google.firebase.analytics.ktx.logEvent
import com.google.firebase.ktx.Firebase
import com.rafi.core.util.Resources
import com.rafi.ecommerce.R
import com.rafi.ecommerce.databinding.FragmentLoginBinding
import com.rafi.ecommerce.util.BaseFragment
import com.rafi.ecommerce.util.Helper
import com.rafi.ecommerce.util.Helper.sendLogButtonClicked
import com.rafi.ecommerce.util.ThrowableExt.getErrorMessage
import com.rafi.ecommerce.util.showSnackbar
import dagger.hilt.android.AndroidEntryPoint
import kotlinx.coroutines.flow.first
import kotlinx.coroutines.launch
import kotlinx.coroutines.runBlocking

@Suppress("DEPRECATION")
@AndroidEntryPoint
class LoginFragment :
    BaseFragment<FragmentLoginBinding>(FragmentLoginBinding::inflate) {

    private val loginViewModel: LoginViewModel by viewModels()

    private lateinit var firebaseAnalytics: FirebaseAnalytics

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        isOnboarding()

        firebaseAnalytics = Firebase.analytics
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        initAppBar()
        observeViewModel()
        initAction()
        validation()
        spannableText()
    }

    private fun initAppBar() {
        val toolbar = binding.appBarLayout.toolbar
        toolbar.title = resources.getString(R.string.login)
        toolbar.isTitleCentered = true
    }

    private fun observeViewModel() {
        viewLifecycleOwner.lifecycleScope.launch {
            loginViewModel.state.collect { value ->
                if (value != null) {
                    handleLoginViewState(value)
                }
            }
        }
    }

    private fun handleLoginViewState(value: Resources<Boolean>) {
        when (value) {
            is Resources.Loading -> showLoading(true)
            is Resources.Success -> {
                showLoading(false)
                if (value.data) {
                    navigateToMainNavigation()
                }
            }
            is Resources.Error -> {
                showLoading(false)
                val errorMessage = value.message.getErrorMessage()
                binding.root.showSnackbar(errorMessage)
            }
        }
        firebaseAnalytics.logEvent(FirebaseAnalytics.Event.LOGIN) {
            param(FirebaseAnalytics.Param.METHOD, "email")
        }
    }

    private fun navigateToMainNavigation() {
        findNavController().navigate(R.id.action_main_navigation)
    }

    private fun isOnboarding() {
        val firstOnboarding = runBlocking {
            loginViewModel.getOnboarding().first()
        }
        if (firstOnboarding) {
            findNavController().navigate(R.id.action_loginFragment_to_onboardingFragment)
        }
    }

    private fun postLogin() {
        binding.apply {
            loginViewModel.postLogin(
                emailLoginInputText.text.toString(),
                passwordLoginInputText.text.toString()
            )
        }
    }

    private fun initAction() {
        binding.apply {
            btnRegister.setOnClickListener {
                firebaseAnalytics.logEvent("login_to_register", null)
                findNavController().navigate(R.id.action_loginFragment_to_registerFragment)
//                throw RuntimeException("Test Crash")
            }
            btnLogin.setOnClickListener {
                firebaseAnalytics.sendLogButtonClicked("Login: Login")
                postLogin()
            }
        }
    }

    private fun validation() {
        binding.apply {
            btnLogin.isEnabled = false

            emailLoginContainer.editText?.doOnTextChanged { text, _, _, _ ->
                if (text.isNullOrEmpty()) {
                    emailLoginContainer.error = null
                } else if (!Patterns.EMAIL_ADDRESS.matcher(text.toString()).matches()) {
                    emailLoginContainer.error = getString(R.string.invalid_email)
                    btnLogin.isEnabled = false
                } else {
                    emailLoginContainer.error = null
                    enabledButton()
                }
            }

            passwordLoginContainer.editText?.doOnTextChanged { text, _, _, _ ->
                if (text.isNullOrEmpty()) {
                    passwordLoginContainer.error = null
                } else if (text.toString().length in 1 until 8) {
                    passwordLoginContainer.error = getString(R.string.invalid_password)
                    btnLogin.isEnabled = false
                } else {
                    passwordLoginContainer.error = null
                    enabledButton()
                }
            }
        }
    }

    private fun enabledButton() {
        binding.apply {
            btnLogin.isEnabled =
                emailLoginContainer.error == null && passwordLoginContainer.error == null &&
                emailLoginInputText.text.toString() != "" &&
                passwordLoginInputText.text.toString() != ""
        }
    }

    private fun showLoading(isLoading: Boolean) {
        binding.apply {
            loginProgressBar.isInvisible = !isLoading
            btnLogin.isInvisible = isLoading
        }
    }

    private fun spannableText() {
        binding.tvTerms.text = Helper.getTncPrivacyPolicySpanText(requireActivity())
    }
}
