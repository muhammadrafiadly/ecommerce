package com.rafi.ecommerce.ui.main.fulfillment.review.compose

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.lazy.LazyColumn
import androidx.compose.foundation.lazy.items
import androidx.compose.material3.CircularProgressIndicator
import androidx.compose.material3.Scaffold
import androidx.compose.runtime.Composable
import androidx.compose.runtime.collectAsState
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.platform.ComposeView
import androidx.compose.ui.tooling.preview.Devices
import androidx.compose.ui.tooling.preview.Preview
import androidx.fragment.app.Fragment
import androidx.fragment.app.viewModels
import androidx.navigation.fragment.findNavController
import com.rafi.core.data.model.ProductReviewsModel
import com.rafi.core.util.Resources
import com.rafi.ecommerce.ui.main.fulfillment.review.ReviewViewModel
import com.rafi.ecommerce.ui.main.fulfillment.review.compose.components.ErrorMessage
import com.rafi.ecommerce.ui.main.fulfillment.review.compose.components.ReviewAppBar
import com.rafi.ecommerce.ui.main.fulfillment.review.compose.components.ReviewItem
import com.rafi.ecommerce.ui.theme.ComposeAppTheme
import dagger.hilt.android.AndroidEntryPoint

@AndroidEntryPoint
class ReviewFragmentCompose : Fragment() {

    private val reviewViewModel: ReviewViewModel by viewModels()

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        return ComposeView(requireContext()).apply {
            setContent {
                ComposeAppTheme {
                    val resultState = reviewViewModel.reviewProductState.collectAsState().value
                    ReviewFragmentScreen(
                        resultState = resultState,
                        onBackAction = {
                            findNavController().navigateUp()
                        },
                        onErrorAction = { reviewViewModel.getListReview() }
                    )
                }
            }
        }
    }

    companion object {
        const val BUNDLE_PRODUCT_ID_KEY = "product_id"
    }
}

@Composable
fun ReviewFragmentScreen(
    resultState: Resources<List<ProductReviewsModel>>?,
    onBackAction: () -> Unit,
    onErrorAction: () -> Unit
) {
    Scaffold(
        topBar = { ReviewAppBar(onBackAction) }
    ) {
        Column(
            Modifier
                .padding(it)
                .fillMaxSize()
        ) {
            when (resultState) {
                is Resources.Loading -> Loading(Modifier.fillMaxSize())
                is Resources.Success -> ReviewList(resultState.data)
                is Resources.Error -> ErrorMessage(
                    Modifier.fillMaxSize(),
                    resultState.message,
                    onErrorAction
                )

                else -> {}
            }
        }
    }
}

@Composable
fun Loading(modifier: Modifier = Modifier) {
    Column(
        modifier,
        verticalArrangement = Arrangement.Center,
        horizontalAlignment = Alignment.CenterHorizontally
    ) {
        CircularProgressIndicator()
    }
}

@Composable
fun ReviewList(listData: List<ProductReviewsModel>) {
    LazyColumn {
        items(listData) { data ->
            ReviewItem(data)
        }
    }
}

@Preview(showBackground = true, device = Devices.PIXEL_4)
@Composable
fun ReviewFragmentPreview() {
    val sampleReviewList = listOf(
        ProductReviewsModel(
            "url_to_user_image",
            "John Doe",
            "Great product!",
            4.5f.toInt()
        ),
        ProductReviewsModel(
            "url_to_user_image",
            "John Doe",
            "Great product!",
            4.5f.toInt()
        ),
        ProductReviewsModel(
            "url_to_user_image",
            "John Doe",
            "Great product!",
            4.5f.toInt()
        )
    )

    ReviewFragmentScreen(
        resultState = Resources.Success(sampleReviewList),
        onBackAction = {},
        onErrorAction = {}
    )
}
