package com.rafi.ecommerce.ui.main.fulfillment.payment

import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.rafi.core.data.firebase.AppFirebaseRemoteConfig
import com.rafi.core.data.model.PaymentModel
import com.rafi.core.util.Resources
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.StateFlow
import kotlinx.coroutines.launch
import javax.inject.Inject

@HiltViewModel
class PaymentViewModel @Inject constructor(
    private val remoteConfig: AppFirebaseRemoteConfig
) : ViewModel() {

    private val _state = MutableStateFlow<Resources<List<PaymentModel>>?>(null)
    val state: StateFlow<Resources<List<PaymentModel>>?> = _state

    init {
        getPayment()
        remoteConfig.onConfigUpdate { result ->
            _state.value = result
        }
    }

    fun getPayment() {
        viewModelScope.launch {
            remoteConfig.fetchPaymentMethod().collect { resultState ->
                _state.value = resultState
            }
        }
    }
}
