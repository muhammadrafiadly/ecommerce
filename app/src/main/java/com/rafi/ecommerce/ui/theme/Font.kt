package com.rafi.ecommerce.ui.theme

import androidx.compose.ui.text.font.Font
import androidx.compose.ui.text.font.FontFamily
import androidx.compose.ui.text.font.FontWeight
import com.rafi.ecommerce.R

val poppinsFamily = FontFamily(
    Font(R.font.poppins_regular_400, FontWeight.Normal),
    Font(R.font.poppins_medium_500, FontWeight.Medium),
    Font(R.font.poppins_semi_bold_600, FontWeight.SemiBold),
    Font(R.font.poppins_bold_700, FontWeight.Bold)
)
