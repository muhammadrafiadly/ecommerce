package com.rafi.ecommerce.ui.main.home

import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.rafi.core.data.repository.UserRepository
import com.rafi.core.data.source.local.room.AppRoomDatabase
import com.rafi.core.util.Resources
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.StateFlow
import kotlinx.coroutines.flow.asStateFlow
import kotlinx.coroutines.launch
import kotlinx.coroutines.runBlocking
import javax.inject.Inject

@HiltViewModel
class HomeViewModel @Inject constructor(
    private val userRepository: UserRepository,
    private val appDatabase: AppRoomDatabase
) : ViewModel() {

    private val _state = MutableStateFlow<Resources<Boolean>?>(null)
    val state: StateFlow<Resources<Boolean>?> = _state.asStateFlow()

    fun postLogout() {
        viewModelScope.launch {
            userRepository.logout()
        }
    }

    fun setAppTheme(darkTheme: Boolean) {
        runBlocking {
            userRepository.setAppTheme(darkTheme)
        }
    }

    fun getAppTheme() = userRepository.getAppTheme()

    fun getAppLanguage() = userRepository.getAppLanguage()

    fun setAppLanguage(isIdLanguage: Boolean) {
        runBlocking { userRepository.setAppLanguage(if (isIdLanguage) "id" else null) }
    }

    fun clearDb() {
        appDatabase.clearAllTables()
    }
}
