package com.rafi.ecommerce.ui.main.store.detail.compose

import androidx.lifecycle.SavedStateHandle
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.rafi.core.data.model.ProductDetailsModel
import com.rafi.core.data.repository.ProductRepository
import com.rafi.core.util.DataMapper.toCart
import com.rafi.core.util.DataMapper.toWishlist
import com.rafi.core.util.Resources
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.asStateFlow
import kotlinx.coroutines.flow.update
import kotlinx.coroutines.launch
import kotlinx.coroutines.runBlocking
import javax.inject.Inject

@HiltViewModel
class ProductDetailsComposeViewModel @Inject constructor(
    private val productRepository: ProductRepository,
    savedStateHandle: SavedStateHandle
) : ViewModel() {

    private val _composeState = MutableStateFlow(DetailComposeState())
    val composeState = _composeState.asStateFlow()

    val productId: String =
        savedStateHandle[ProductDetailFragmentCompose.BUNDLE_PRODUCT_ID_KEY] ?: ""

    init {
        initState()
        getDetailProduct()
    }

    private fun initState() {
        _composeState.update {
            it.copy(productId = productId)
        }
    }

    fun getDetailProduct() {
        viewModelScope.launch {
            productRepository.productDetails(productId).collect { resultState ->
                _composeState.update {
                    if (resultState is Resources.Success) {
                        it.copy(
                            resultState = resultState,
                            detailProduct = resultState.data,
                            productVariant = resultState.data.productVariant[0],
                            isWishlist = checkExistWishlist()
                        )
                    } else {
                        it.copy(resultState = resultState)
                    }
                }
            }
        }
    }

    private fun checkExistWishlist(): Boolean {
        return runBlocking {
            productRepository.checkWishListProducts(productId)
        }
    }

    fun insertWishlist() {
        viewModelScope.launch {
            val product = _composeState.value.detailProduct
            val variant = _composeState.value.productVariant
            productRepository.insertWishList(product!!.toWishlist(variant!!))
        }
        _composeState.update {
            it.copy(isWishlist = true)
        }
    }

    fun deleteWishlist() {
        viewModelScope.launch {
            val product = _composeState.value.detailProduct
            val variant = _composeState.value.productVariant
            productRepository.deleteWishList(product!!.toWishlist(variant!!))
        }
        _composeState.update {
            it.copy(isWishlist = false)
        }
    }

    fun updateVariant(variant: ProductDetailsModel.ProductVariant) {
        _composeState.update {
            it.copy(productVariant = variant)
        }
    }

    fun insertCart(): Boolean {
        val product = composeState.value.detailProduct!!
        val variant = composeState.value.productVariant!!
        val cart = product.toCart(variant)
        val isReadyStock = runBlocking { productRepository.isReadyStock(cart) }
        return if (isReadyStock) {
            viewModelScope.launch {
                productRepository.insertCart(cart)
            }
            true
        } else {
            false
        }
    }
}

data class DetailComposeState(
    val productId: String = "",
    val isWishlist: Boolean = false,
    val detailProduct: ProductDetailsModel? = null,
    val productVariant: ProductDetailsModel.ProductVariant? = null,
    val resultState: Resources<ProductDetailsModel>? = null
)
