package com.rafi.ecommerce.ui.main.store.detail

import androidx.lifecycle.SavedStateHandle
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.rafi.core.data.model.ProductDetailsModel
import com.rafi.core.data.repository.ProductRepository
import com.rafi.core.util.DataMapper.toCart
import com.rafi.core.util.DataMapper.toWishlist
import com.rafi.core.util.Resources
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.asStateFlow
import kotlinx.coroutines.launch
import kotlinx.coroutines.runBlocking
import javax.inject.Inject

@HiltViewModel
class ProductDetailsViewModel @Inject constructor(
    private val productRepository: ProductRepository,
    savedStateHandle: SavedStateHandle
) : ViewModel() {

    val productId: String = savedStateHandle[ProductDetailFragment.BUNDLE_PRODUCT_ID_KEY] ?: ""
    var isWishlist: Boolean = false
    var detailProduct: ProductDetailsModel? = null
    var productVariant: ProductDetailsModel.ProductVariant? = null

    private val _productDetailsState = MutableStateFlow<Resources<ProductDetailsModel>?>(null)
    val productDetailsState = _productDetailsState.asStateFlow()

    init {
        getDetailProduct()
    }

    fun getDetailProduct() {
        viewModelScope.launch {
            productRepository.productDetails(productId).collect { resultState ->
                _productDetailsState.value = resultState
            }
        }
    }

    fun checkExistWishlist(): Boolean {
        return runBlocking {
            productRepository.checkWishListProducts(productId)
        }
    }

    fun insertWishlist() {
        if (detailProduct != null) {
            viewModelScope.launch {
                productRepository.insertWishList(detailProduct!!.toWishlist(productVariant!!))
            }
            isWishlist = true
        }
    }

    fun deleteWishlist() {
        if (detailProduct != null) {
            viewModelScope.launch {
                productRepository.deleteWishList(detailProduct!!.toWishlist(productVariant!!))
            }
            isWishlist = false
        }
    }

    fun insertCart(): Boolean {
        return if (detailProduct != null) {
            val cart = detailProduct!!.toCart(productVariant!!)
            val isStockReady = runBlocking { productRepository.isReadyStock(cart) }
            if (isStockReady) {
                viewModelScope.launch {
                    productRepository.insertCart(cart)
                }
                true
            } else {
                false
            }
        } else {
            false
        }
    }
}
