package com.rafi.ecommerce.ui.main

import androidx.lifecycle.SavedStateHandle
import androidx.lifecycle.ViewModel
import com.rafi.core.data.repository.NotificationRepository
import com.rafi.core.data.repository.ProductRepository
import com.rafi.core.data.repository.UserRepository
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.first
import kotlinx.coroutines.runBlocking
import javax.inject.Inject

@HiltViewModel
class MainViewModel @Inject constructor(
    private val userRepository: UserRepository,
    private val productRepository: ProductRepository,
    private val notificationRepository: NotificationRepository,
    savedStateHandle: SavedStateHandle
) : ViewModel() {

    var navigateToMainFragment =
        savedStateHandle.get<Boolean>(
            MainFragment.MOVE_TRANSACTION_BUNDLE_KEY
        ) ?: false

    val user by lazy {
        runBlocking { getSession().first() }
    }

    fun productCartSize(): Flow<Int> = productRepository.getProductCartSize()

    fun wishListProductSize(): Flow<Int> = productRepository.getWishListProductsSize()

    fun notificationSize(): Flow<Int> = notificationRepository.getNotificationDataSize()

    private fun getSession() = userRepository.getSession()

    fun checkUserData(): Boolean {
        if (!user.userName.isNullOrEmpty()) return true
        return false
    }

    fun checkLogin(): Boolean {
        if (user.accessToken.isNullOrEmpty() or user.refreshToken.isNullOrEmpty()) {
            return false
        }
        return true
    }
}
