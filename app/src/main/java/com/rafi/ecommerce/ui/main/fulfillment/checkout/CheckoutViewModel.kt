package com.rafi.ecommerce.ui.main.fulfillment.checkout

import androidx.lifecycle.SavedStateHandle
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.rafi.core.data.model.FulfillmentModel
import com.rafi.core.data.model.PaymentModel
import com.rafi.core.data.model.ProductCartModel
import com.rafi.core.data.repository.FulfillmentRepository
import com.rafi.core.data.repository.ProductRepository
import com.rafi.core.util.Resources
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.StateFlow
import kotlinx.coroutines.flow.asStateFlow
import kotlinx.coroutines.flow.first
import kotlinx.coroutines.flow.update
import kotlinx.coroutines.launch
import javax.inject.Inject

@HiltViewModel
class CheckoutViewModel @Inject constructor(
    private val fulfillmentRepository: FulfillmentRepository,
    private val productRepository: ProductRepository,
    savedStateHandle: SavedStateHandle
) : ViewModel() {

    private val _listData: MutableStateFlow<List<ProductCartModel>> =
        MutableStateFlow(ArrayList())
    val listData: StateFlow<List<ProductCartModel>> = _listData.asStateFlow()

    private val _state = MutableStateFlow<Resources<FulfillmentModel>?>(null)
    val state: StateFlow<Resources<FulfillmentModel>?> = _state.asStateFlow()

    var paymentItem: PaymentModel.PaymentItem? = null
    var totalPrice = 0

    private fun getCartData(): Flow<List<ProductCartModel>> = productRepository.getProductCart()

    init {
        setData(
            savedStateHandle.get<ArrayList<ProductCartModel>>(
                CheckoutFragment.ARG_DATA
            ) ?: ArrayList()
        )
    }

    private fun setData(data: ArrayList<ProductCartModel>) {
        _listData.value = data.map { it.copy(quantity = it.quantity ?: 1) }
    }

    fun updateProductQuantity(cart: ProductCartModel, isInsert: Boolean) {
        if (isInsert && cart.stock > cart.quantity!!) {
            updateQuantity(cart, 1)
        } else if (!isInsert && cart.quantity!! > 1) {
            updateQuantity(cart, -1)
        }
    }

    private fun updateQuantity(cart: ProductCartModel, number: Int) {
        _listData.update {
            it.map { data ->
                if (data.productId == cart.productId) {
                    data.copy(quantity = data.quantity?.plus(number))
                } else {
                    data
                }
            }
        }
    }

    fun postPayment() {
        if (paymentItem != null) {
            viewModelScope.launch {
                val data = listData.first()
                fulfillmentRepository.fulfillment(paymentItem!!.label, data).collect { result ->
                    _state.value = result
                }
            }
        }
    }

    fun deleteItemFromCart(cart: ProductCartModel? = null) {
        viewModelScope.launch {
            if (cart != null) {
                productRepository.deleteCart(cart)
            } else {
                val filteredCart = getCartData().first().filter { it.isChecked }
                    .toTypedArray()
                productRepository.deleteCart(*filteredCart)
            }
        }
    }
}
