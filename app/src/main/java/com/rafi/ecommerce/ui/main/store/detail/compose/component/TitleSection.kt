package com.rafi.ecommerce.ui.main.store.detail.compose.component

import androidx.compose.foundation.BorderStroke
import androidx.compose.foundation.border
import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.Spacer
import androidx.compose.foundation.layout.height
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.layout.size
import androidx.compose.foundation.layout.width
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.material3.Icon
import androidx.compose.material3.IconButton
import androidx.compose.material3.MaterialTheme
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.text.PlatformTextStyle
import androidx.compose.ui.text.TextStyle
import androidx.compose.ui.text.font.FontWeight
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.tooling.preview.PreviewParameter
import androidx.compose.ui.tooling.preview.PreviewParameterProvider
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import com.rafi.core.data.model.ProductDetailsModel
import com.rafi.ecommerce.R
import com.rafi.ecommerce.ui.theme.poppinsFamily
import com.rafi.ecommerce.util.toCurrencyFormat

@Composable
fun TitleSection(
    product: ProductDetailsModel,
    variant: ProductDetailsModel.ProductVariant,
    isWishlist: Boolean,
    onSharedClick: () -> Unit,
    onWishlistClick: () -> Unit,
    modifier: Modifier = Modifier
) {
    val price = product.productPrice + variant.variantPrice
    Column(modifier) {
        Row(verticalAlignment = Alignment.CenterVertically) {
            Text(
                text = price.toCurrencyFormat(),
                fontSize = 20.sp,
                style = TextStyle(
                    fontFamily = poppinsFamily,
                    fontWeight = FontWeight.SemiBold,
                    platformStyle = PlatformTextStyle(
                        includeFontPadding = false,
                    ),
                ),
                modifier = Modifier.weight(1f)
            )
            IconButton(
                onClick = { onSharedClick() },
                modifier = Modifier.size(24.dp)
            ) {
                Icon(
                    painter = painterResource(id = R.drawable.ic_share),
                    contentDescription = null
                )
            }
            Spacer(modifier = Modifier.width(16.dp))
            IconButton(
                onClick = { onWishlistClick() },
                modifier = Modifier.size(24.dp)
            ) {
                if (isWishlist) {
                    Icon(
                        painter = painterResource(id = R.drawable.ic_favorite),
                        contentDescription = null,
                        tint = MaterialTheme.colorScheme.primary
                    )
                } else {
                    Icon(
                        painter = painterResource(id = R.drawable.ic_favorite_border),
                        contentDescription = null,
                        tint = MaterialTheme.colorScheme.onSurface
                    )
                }
            }
        }
        Spacer(modifier = Modifier.height(8.dp))
        Text(
            text = product.productName,
            style = TextStyle(
                fontFamily = poppinsFamily,
                fontWeight = FontWeight.Normal,
                platformStyle = PlatformTextStyle(
                    includeFontPadding = false,
                ),
            )
        )
        Spacer(modifier = Modifier.height(8.dp))
        Row(verticalAlignment = Alignment.CenterVertically) {
            Text(
                text = stringResource(id = R.string.total_sales, product.sale),
                fontSize = 12.sp,
                style = TextStyle(
                    fontFamily = poppinsFamily,
                    fontWeight = FontWeight.Normal,
                    platformStyle = PlatformTextStyle(
                        includeFontPadding = false,
                    ),
                )
            )
            Spacer(modifier = Modifier.width(8.dp))
            Box(
                Modifier.border(
                    border = BorderStroke(1.dp, MaterialTheme.colorScheme.outline),
                    shape = RoundedCornerShape(4.dp),
                )
            ) {
                Row(
                    verticalAlignment = Alignment.CenterVertically,
                    modifier = Modifier.padding(4.dp, 2.dp, 8.dp, 2.dp)
                ) {
                    Icon(
                        painter = painterResource(id = R.drawable.ic_star),
                        contentDescription = null,
                        modifier = Modifier.size(14.dp)
                    )
                    Spacer(modifier = Modifier.width(4.dp))
                    Text(
                        text = stringResource(
                            id = R.string.detail_rating_and_total_rating,
                            product.productRating,
                            product.totalRating
                        ),
                        fontSize = 12.sp,
                        style = TextStyle(
                            fontFamily = poppinsFamily,
                            fontWeight = FontWeight.Normal,
                            platformStyle = PlatformTextStyle(
                                includeFontPadding = false,
                            ),
                        )
                    )
                }
            }
        }
    }
}

@Preview(showBackground = true)
@Composable
fun TitleSectionPreview(
    @PreviewParameter(TitleSectionPreviewProvider::class) data: TitleSectionPreviewData
) {
    TitleSection(
        product = data.product,
        variant = data.variant,
        isWishlist = data.isWishlist,
        onSharedClick = {},
        onWishlistClick = {}
    )
}

data class TitleSectionPreviewData(
    val product: ProductDetailsModel,
    val variant: ProductDetailsModel.ProductVariant,
    val isWishlist: Boolean
)

class TitleSectionPreviewProvider : PreviewParameterProvider<TitleSectionPreviewData> {
    override val values: Sequence<TitleSectionPreviewData> = sequenceOf(
        TitleSectionPreviewData(
            product = ProductDetailsModel(
                productId = "your_product_id",
                productName = "ASUS ROG Strix G17 G713RM-R736H6G-O - Eclipse Gray",
                productPrice = 24499000,
                image = listOf(
                    "https://example.com/image1.jpg",
                    "https://example.com/image2.jpg",
                    "https://example.com/image3.jpg"
                ),
                brand = "Asus",
                description = "Your product description goes here.",
                store = "AsusStore",
                sale = 12,
                stock = 2,
                totalRating = 7,
                totalReview = 5,
                totalSatisfaction = 100,
                productRating = 5.0F,
                productVariant = listOf(
                    ProductDetailsModel.ProductVariant(
                        variantName = "Variant 1",
                        variantPrice = 0
                    ),
                    ProductDetailsModel.ProductVariant(
                        variantName = "Variant 2",
                        variantPrice = 10000
                    )
                )
            ),
            variant = ProductDetailsModel.ProductVariant(
                variantName = "Variant 1",
                variantPrice = 0
            ),
            isWishlist = true
        ),
    )
}
