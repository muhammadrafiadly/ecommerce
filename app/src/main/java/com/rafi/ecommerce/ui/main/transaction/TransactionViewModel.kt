package com.rafi.ecommerce.ui.main.transaction

import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.rafi.core.data.model.TransactionModel
import com.rafi.core.data.repository.FulfillmentRepository
import com.rafi.core.util.Resources
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.StateFlow
import kotlinx.coroutines.launch
import javax.inject.Inject

@HiltViewModel
class TransactionViewModel @Inject constructor(
    private val fulfillmentRepository: FulfillmentRepository
) : ViewModel() {

    private val _state = MutableStateFlow<Resources<List<TransactionModel>>?>(null)
    val state: StateFlow<Resources<List<TransactionModel>>?> = _state

    init {
        getTransactionList()
    }

    fun getTransactionList() {
        viewModelScope.launch {
            fulfillmentRepository.getTransaction().collect {
                _state.value = it
            }
        }
    }
}
