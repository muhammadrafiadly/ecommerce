package com.rafi.ecommerce.ui.prelogin.register

import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.rafi.core.data.repository.UserRepository
import com.rafi.core.util.Resources
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.StateFlow
import kotlinx.coroutines.flow.asStateFlow
import kotlinx.coroutines.launch
import javax.inject.Inject

@HiltViewModel
class RegisterViewModel @Inject constructor(
    private val userRepository: UserRepository
) : ViewModel() {

    private val _state = MutableStateFlow<Resources<Boolean>?>(null)
    val state: StateFlow<Resources<Boolean>?> = _state.asStateFlow()

    fun postRegister(email: String, password: String) {
        viewModelScope.launch {
            userRepository.register(email, password).collect { value ->
                _state.value = value
            }
        }
    }
}
