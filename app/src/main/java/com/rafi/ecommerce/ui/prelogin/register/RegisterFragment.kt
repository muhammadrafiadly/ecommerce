package com.rafi.ecommerce.ui.prelogin.register

import android.os.Bundle
import android.util.Patterns
import android.view.View
import androidx.core.view.isInvisible
import androidx.core.widget.doOnTextChanged
import androidx.fragment.app.viewModels
import androidx.lifecycle.Lifecycle
import androidx.lifecycle.lifecycleScope
import androidx.lifecycle.repeatOnLifecycle
import androidx.navigation.fragment.findNavController
import com.google.firebase.analytics.FirebaseAnalytics
import com.google.firebase.analytics.ktx.analytics
import com.google.firebase.analytics.ktx.logEvent
import com.google.firebase.ktx.Firebase
import com.rafi.core.util.Resources
import com.rafi.ecommerce.R
import com.rafi.ecommerce.databinding.FragmentRegisterBinding
import com.rafi.ecommerce.util.BaseFragment
import com.rafi.ecommerce.util.Helper
import com.rafi.ecommerce.util.ThrowableExt.getErrorMessage
import com.rafi.ecommerce.util.showSnackbar
import dagger.hilt.android.AndroidEntryPoint
import kotlinx.coroutines.launch

@Suppress("DEPRECATION")
@AndroidEntryPoint
class RegisterFragment :
    BaseFragment<FragmentRegisterBinding>(FragmentRegisterBinding::inflate) {

    private val registerViewModel: RegisterViewModel by viewModels()

    private lateinit var firebaseAnalytics: FirebaseAnalytics

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        firebaseAnalytics = Firebase.analytics
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        initAppBar()
        observeViewModel()
        initAction()
        validation()
        spannableText()
    }

    private fun initAppBar() {
        val toolbar = binding.appBarLayout.toolbar
        toolbar.title = resources.getString(R.string.register)
        toolbar.isTitleCentered = true
    }

    private fun observeViewModel() {
        viewLifecycleOwner.lifecycleScope.launch {
            repeatOnLifecycle(Lifecycle.State.STARTED) {
                registerViewModel.state.collect { value ->
                    if (value != null) {
                        handleRegisterViewState(value)
                    }

                    firebaseAnalytics.logEvent(FirebaseAnalytics.Event.SIGN_UP) {
                        param(FirebaseAnalytics.Param.METHOD, "email")
                    }
                }
            }
        }
    }

    private fun handleRegisterViewState(value: Resources<Boolean>) {
        when (value) {
            is Resources.Loading -> showLoading(true)
            is Resources.Success -> {
                showLoading(false)
                if (value.data) {
                    navigateToMainNavigation()
                }
            }

            is Resources.Error -> {
                showLoading(false)
                val errorMessage = value.message.getErrorMessage()
                binding.root.showSnackbar(errorMessage)
            }
        }
    }

    private fun navigateToMainNavigation() {
        findNavController().navigate(R.id.action_main_navigation)
    }

    private fun postRegister() {
        binding.apply {
            registerViewModel.postRegister(
                emailRegisterInputText.text.toString(),
                passwordRegisterInputText.text.toString()
            )
        }
    }

    private fun initAction() {
        binding.apply {
            btnLogin1.setOnClickListener {
                firebaseAnalytics.logEvent("register_to_login", null)
                findNavController().navigate(R.id.action_registerFragment_to_loginFragment)
            }

            btnRegister1.setOnClickListener {
                postRegister()
            }
        }
    }

    private fun validation() {
        binding.apply {
            btnRegister1.isEnabled = false

            emailRegisterContainer.editText?.doOnTextChanged { text, _, _, _ ->
                if (text.isNullOrEmpty()) {
                    emailRegisterContainer.error = null
                } else if (!Patterns.EMAIL_ADDRESS.matcher(text.toString()).matches()) {
                    emailRegisterContainer.error = getString(R.string.invalid_email)
                    btnRegister1.isEnabled = false
                } else {
                    emailRegisterContainer.error = null
                    enabledButton()
                }
            }

            passwordRegisterContainer.editText?.doOnTextChanged { text, _, _, _ ->
                if (text.isNullOrEmpty()) {
                    passwordRegisterContainer.error = null
                } else if (text.toString().length in 1 until 8) {
                    passwordRegisterContainer.error = getString(R.string.invalid_password)
                    btnRegister1.isEnabled = false
                } else {
                    passwordRegisterContainer.error = null
                    enabledButton()
                }
            }
        }
    }

    private fun enabledButton() {
        binding.apply {
            btnRegister1.isEnabled =
                emailRegisterContainer.error == null &&
                passwordRegisterContainer.error == null &&
                emailRegisterInputText.text.toString() != "" &&
                passwordRegisterInputText.text.toString() != ""
        }
    }

    private fun showLoading(isLoading: Boolean) {
        binding.apply {
            registerProgressBar.isInvisible = !isLoading
            btnRegister1.isInvisible = isLoading
        }
    }

    private fun spannableText() {
        binding.tvTerms.text = Helper.getTncPrivacyPolicySpanText(requireActivity())
    }
}
