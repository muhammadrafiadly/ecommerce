package com.rafi.ecommerce.ui.main.store.detail.compose.component

import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.PaddingValues
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.Spacer
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.height
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.layout.width
import androidx.compose.material3.Icon
import androidx.compose.material3.Text
import androidx.compose.material3.TextButton
import androidx.compose.runtime.Composable
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.text.PlatformTextStyle
import androidx.compose.ui.text.TextStyle
import androidx.compose.ui.text.font.FontWeight
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import com.rafi.core.data.model.ProductDetailsModel
import com.rafi.ecommerce.R
import com.rafi.ecommerce.ui.theme.poppinsFamily
import java.util.Locale

@Composable
fun ReviewSection(
    product: ProductDetailsModel,
    onMoreClicked: () -> Unit,
    modifier: Modifier = Modifier
) {
    Column(modifier) {
        Row(
            horizontalArrangement = Arrangement.SpaceBetween,
            modifier = Modifier.fillMaxWidth()
        ) {
            Text(
                text = stringResource(id = R.string.buyer_reviews),
                fontSize = 16.sp,
                style = TextStyle(
                    fontFamily = poppinsFamily,
                    fontWeight = FontWeight.Medium,
                    platformStyle = PlatformTextStyle(
                        includeFontPadding = false,
                    ),
                ),
            )
            TextButton(
                onClick = { onMoreClicked() },
                contentPadding = PaddingValues(vertical = 0.dp, horizontal = 16.dp),
                modifier = Modifier.height(22.dp),
            ) {
                Text(
                    text = stringResource(id = R.string.see_all_btn),
                    fontSize = 12.sp,
                    style = TextStyle(
                        fontFamily = poppinsFamily,
                        fontWeight = FontWeight.Medium,
                        platformStyle = PlatformTextStyle(
                            includeFontPadding = false,
                        ),
                    ),
                )
            }
        }
        Spacer(modifier = Modifier.height(8.dp))
        Row(verticalAlignment = Alignment.CenterVertically) {
            Icon(painter = painterResource(id = R.drawable.ic_star), contentDescription = null)
            Spacer(modifier = Modifier.width(2.dp))
            Text(
                text = String.format(Locale.getDefault(), "%.1f", product.productRating),
                fontSize = 20.sp,
                style = TextStyle(
                    fontFamily = poppinsFamily,
                    fontWeight = FontWeight.SemiBold,
                    platformStyle = PlatformTextStyle(
                        includeFontPadding = false,
                    ),
                ),
            )
            Text(
                text = stringResource(id = R.string.max_rating),
                fontSize = 10.sp,
                style = TextStyle(
                    fontFamily = poppinsFamily,
                    fontWeight = FontWeight.Normal,
                    platformStyle = PlatformTextStyle(
                        includeFontPadding = false,
                    ),
                ),
                modifier = Modifier.padding(top = 10.dp)
            )
            Spacer(modifier = Modifier.width(32.dp))
            Column {
                Text(
                    text = stringResource(
                        id = R.string.text_satisfaction,
                        product.totalSatisfaction
                    ),
                    fontSize = 12.sp,
                    style = TextStyle(
                        fontFamily = poppinsFamily,
                        fontWeight = FontWeight.SemiBold,
                        platformStyle = PlatformTextStyle(
                            includeFontPadding = false,
                        ),
                    ),
                )
                Text(
                    text = stringResource(
                        id = R.string.total_rating_and_review,
                        product.totalRating,
                        product.totalReview
                    ),
                    fontSize = 12.sp,
                    style = TextStyle(
                        fontFamily = poppinsFamily,
                        fontWeight = FontWeight.Normal,
                        platformStyle = PlatformTextStyle(
                            includeFontPadding = false,
                        ),
                    ),
                )
            }
        }
    }
}

@Preview(showBackground = true)
@Composable
fun ReviewSectionPreview() {
    val product = ProductDetailsModel(
        productId = "17b4714d-527a-4be2-84e2-e4c37c2b3292",
        productName = "ASUS ROG Strix G17 G713RM-R736H6G-O - Eclipse Gray",
        productPrice = 24499000,
        image = arrayListOf(
            "https://images.tokopedia.net/img/cache/900/VqbcmM/2022/4/6/0a49c399-cf6b-47f5-91c9-8cbd0b86462d.jpg",
            "https://images.tokopedia.net/img/cache/900/VqbcmM/2022/3/25/0cc3d06c-b09d-4294-8c3f-1c37e60631a6.jpg",
            "https://images.tokopedia.net/img/cache/900/VqbcmM/2022/3/25/33a06657-9f88-4108-8676-7adafaa94921.jpg"
        ),
        brand = "Asus",
        description = "ASUS ROG Strix G17 G713RM-R736H6G-O - Eclipse Gray [AMD Ryzen™ 7 6800H / NVIDIA® GeForce RTX™ 3060 / 8G*2 / 512GB / 17.3inch / WIN11 / OHS]\n\nCPU : AMD Ryzen™ 7 6800H Mobile Processor (8-core/16-thread, 20MB cache, up to 4.7 GHz max boost)\nGPU : NVIDIA® GeForce RTX™ 3060 Laptop GPU\nGraphics Memory : 6GB GDDR6\nDiscrete/Optimus : MUX Switch + Optimus\nTGP ROG Boost : 1752MHz* at 140W (1702MHz Boost Clock+50MHz OC, 115W+25W Dynamic Boost)\nPanel : 17.3-inch FHD (1920 x 1080) 16:9 360Hz IPS-level 300nits sRGB % 100.00%",
        store = "AsusStore",
        sale = 12,
        stock = 2,
        totalRating = 7,
        totalReview = 5,
        totalSatisfaction = 100,
        productRating = 5.0F,
        productVariant = arrayListOf()
    )

    ReviewSection(product = product, onMoreClicked = {})
}
