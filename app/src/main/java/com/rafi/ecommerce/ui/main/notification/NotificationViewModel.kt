package com.rafi.ecommerce.ui.main.notification

import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.rafi.core.data.model.NotificationModel
import com.rafi.core.data.repository.NotificationRepository
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.launch
import javax.inject.Inject

@HiltViewModel
class NotificationViewModel @Inject constructor(
    private val notificationRepository: NotificationRepository
) : ViewModel() {

    fun getNotificationData(): Flow<List<NotificationModel>> =
        notificationRepository.getNotification()

    fun readNotification(data: NotificationModel) {
        viewModelScope.launch {
            notificationRepository.updateNotification(data.copy(isRead = true))
        }
    }
}
