package com.rafi.ecommerce.ui.main.notification

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.ListAdapter
import androidx.recyclerview.widget.RecyclerView
import coil.load
import com.rafi.core.data.model.NotificationModel
import com.rafi.ecommerce.R
import com.rafi.ecommerce.databinding.ItemNotificationBinding
import com.rafi.ecommerce.util.Helper

class NotificationAdapter(
    private val callback: (NotificationModel) -> Unit
) : ListAdapter<NotificationModel, NotificationAdapter.NotificationViewHolder>(
    NotificationComparator
) {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): NotificationViewHolder {
        val binding = ItemNotificationBinding.inflate(
            LayoutInflater
                .from(parent.context),
            parent,
            false
        )
        return NotificationViewHolder(binding, callback)
    }

    override fun onBindViewHolder(holder: NotificationViewHolder, position: Int) {
        holder.bind(getItem(position))
    }

    class NotificationViewHolder(
        private val binding: ItemNotificationBinding,
        private val callback: (NotificationModel) -> Unit
    ) : RecyclerView.ViewHolder(binding.root) {
        fun bind(data: NotificationModel) {
            with(binding) {
                ivNotification.load(data.image)
                tvNotificationTitle.text = data.title
                tvNotificationBody.text = data.body
                tvType.text = data.type
                tvNotificationDate.text = itemView.context
                    .getString(R.string.date_time, data.date, data.time)

                if (!data.isRead) {
                    layoutItemNotification.setBackgroundColor(
                        Helper.getColorTheme(
                            itemView.context,
                            com.google.android.material.R.attr.colorOutlineVariant
                        )
                    )
                } else {
                    layoutItemNotification.setBackgroundColor(
                        Helper.getColorTheme(
                            itemView.context,
                            com.google.android.material.R.attr.colorSurface
                        )
                    )
                    layoutItemNotification.isClickable = false
                    layoutItemNotification.isFocusable = false
                    layoutItemNotification.foreground = null
                }
            }
            itemView.setOnClickListener {
                callback(data)
            }
        }
    }

    object NotificationComparator : DiffUtil.ItemCallback<NotificationModel>() {
        override fun areItemsTheSame(
            oldItem: NotificationModel,
            newItem: NotificationModel
        ): Boolean {
            return oldItem.id == newItem.id
        }

        override fun areContentsTheSame(
            oldItem: NotificationModel,
            newItem: NotificationModel
        ): Boolean {
            return oldItem == newItem
        }
    }
}
