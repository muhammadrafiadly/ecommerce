package com.rafi.ecommerce.ui.main.store.search

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.RecyclerView
import com.rafi.ecommerce.databinding.ItemSearchProductBinding

class SearchAdapter(private val itemClickCallback: (String) -> Unit) :
    androidx.recyclerview.widget.ListAdapter<String, SearchAdapter.SearchViewHolder>(SearchDiffUtil) {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): SearchViewHolder {
        val binding =
            ItemSearchProductBinding.inflate(LayoutInflater.from(parent.context), parent, false)
        return SearchViewHolder(binding, itemClickCallback)
    }

    override fun onBindViewHolder(holder: SearchViewHolder, position: Int) {
        val data = getItem(position)
        holder.bind(data)
    }

    class SearchViewHolder(
        private val binding: ItemSearchProductBinding,
        private val itemClickCallback: (String) -> Unit
    ) :
        RecyclerView.ViewHolder(binding.root) {
        fun bind(data: String) {
            binding.tvSearchKeyword.text = data
            itemView.setOnClickListener { itemClickCallback(data) }
        }
    }

    object SearchDiffUtil : DiffUtil.ItemCallback<String>() {
        override fun areItemsTheSame(oldItem: String, newItem: String): Boolean =
            oldItem == newItem

        override fun areContentsTheSame(oldItem: String, newItem: String): Boolean =
            oldItem == newItem
    }
}
