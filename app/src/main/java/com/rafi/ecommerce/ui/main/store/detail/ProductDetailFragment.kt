package com.rafi.ecommerce.ui.main.store.detail

import android.content.Intent
import android.os.Bundle
import android.view.View
import androidx.core.os.bundleOf
import androidx.core.view.isVisible
import androidx.fragment.app.viewModels
import androidx.lifecycle.Lifecycle
import androidx.lifecycle.lifecycleScope
import androidx.lifecycle.repeatOnLifecycle
import androidx.navigation.fragment.findNavController
import com.google.android.material.chip.Chip
import com.google.android.material.tabs.TabLayoutMediator
import com.google.firebase.analytics.FirebaseAnalytics
import com.google.firebase.analytics.FirebaseAnalytics.Param
import com.google.firebase.analytics.ktx.analytics
import com.google.firebase.analytics.logEvent
import com.google.firebase.ktx.Firebase
import com.rafi.core.data.model.ProductDetailsModel
import com.rafi.core.util.DataMapper.toCart
import com.rafi.core.util.Resources
import com.rafi.ecommerce.R
import com.rafi.ecommerce.databinding.FragmentProductDetailsBinding
import com.rafi.ecommerce.ui.main.fulfillment.checkout.CheckoutFragment
import com.rafi.ecommerce.ui.main.fulfillment.review.ReviewFragment
import com.rafi.ecommerce.util.BaseFragment
import com.rafi.ecommerce.util.ThrowableExt.getErrorSubtitle
import com.rafi.ecommerce.util.ThrowableExt.getErrorTitle
import com.rafi.ecommerce.util.showSnackbar
import com.rafi.ecommerce.util.toCurrencyFormat
import dagger.hilt.android.AndroidEntryPoint
import kotlinx.coroutines.launch

@AndroidEntryPoint
class ProductDetailFragment :
    BaseFragment<FragmentProductDetailsBinding>(FragmentProductDetailsBinding::inflate) {

    private val productDetailsViewModel: ProductDetailsViewModel by viewModels()
    private val adapter = ImageDetailAdapter()
    private val firebaseAnalytics: FirebaseAnalytics by lazy {
        Firebase.analytics
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        initAppBar()
        checkIsWishList()
        observeDetailProduct()
        initButton()
    }

    private fun logAddToCartEvent() {
        firebaseAnalytics.logEvent(FirebaseAnalytics.Event.ADD_TO_CART) {
            val itemBundle = Bundle().apply {
                putString(Param.ITEM_ID, productDetailsViewModel.detailProduct!!.productId)
                putString(Param.ITEM_NAME, productDetailsViewModel.detailProduct!!.productName)
                putString(Param.ITEM_BRAND, productDetailsViewModel.detailProduct!!.brand)
                putString(Param.ITEM_VARIANT, productDetailsViewModel.productVariant!!.variantName)
            }
            param(Param.ITEMS, arrayOf(itemBundle))
        }
    }

    private fun logAddToWishlistEvent() {
        firebaseAnalytics.logEvent(FirebaseAnalytics.Event.ADD_TO_WISHLIST) {
            val itemBundle = Bundle().apply {
                putString(Param.ITEM_ID, productDetailsViewModel.detailProduct!!.productId)
                putString(Param.ITEM_NAME, productDetailsViewModel.detailProduct!!.productName)
                putString(Param.ITEM_BRAND, productDetailsViewModel.detailProduct!!.brand)
                putString(Param.ITEM_VARIANT, productDetailsViewModel.productVariant!!.variantName)
            }
            param(Param.ITEMS, arrayOf(itemBundle))
        }
    }

    private fun initAppBar() {
        val toolbar = binding.appBarLayout.toolbar
        toolbar.title = getString(R.string.product_detail_title)
        toolbar.setNavigationIcon(R.drawable.ic_arrow_back)
        toolbar.setNavigationOnClickListener {
            findNavController().navigateUp()
        }
    }

    private fun initButton() {
        binding.includeProductDetail.apply {
            ivShare.setOnClickListener {
                initShare()
            }
            ivFavorite.setOnClickListener {
                initWishList()
                logAddToWishlistEvent()
            }
            btnSeeAllReview.setOnClickListener {
                initReview()
            }
            btnBuy.setOnClickListener {
                initCheckout()
            }
            btnToCart.setOnClickListener {
                initCart()
                logAddToCartEvent()
            }
        }
    }

    private fun initData(data: ProductDetailsModel) {
        productDetailsViewModel.detailProduct = data
        if (productDetailsViewModel.productVariant == null) {
            productDetailsViewModel.productVariant = data.productVariant[0]
        }
        setView(data)
    }

    private fun observeDetailProduct() {
        viewLifecycleOwner.lifecycleScope.launch {
            repeatOnLifecycle(Lifecycle.State.STARTED) {
                productDetailsViewModel.productDetailsState.collect { result ->
                    if (result != null) {
                        showLoading(result is Resources.Loading)
                        when (result) {
                            is Resources.Loading -> {}
                            is Resources.Success -> {
                                initData(result.data)
                                binding.includeErrorMessage.containerEmptyData.isVisible = false
                            }

                            is Resources.Error -> {
                                initErrorMessage(result.message)
                            }
                        }
                    }
                }
            }
        }
    }

    private fun setView(data: ProductDetailsModel) {
        setImageView(data.image)
        setChipVariants(data.productVariant, data.productPrice)
        binding.includeProductDetail.apply {
            tvDetailPrice.text =
                data.productPrice.plus(productDetailsViewModel.productVariant!!.variantPrice)
                    .toCurrencyFormat()
            tvDetailName.text = data.productName
            tvDetailSale.text = getString(R.string.total_sales, data.sale)
            chipRating.text = getString(
                R.string.detail_rating_and_total_rating,
                data.productRating,
                data.totalRating
            )
            tvDetailSale.text = getString(R.string.total_sales, data.sale)
            tvDetailDescription.text = data.description
            tvDetailSatisfaction.text =
                getString(R.string.text_satisfaction, data.totalSatisfaction)
            tvDetailRatingReview.text =
                getString(R.string.total_rating_and_review, data.totalRating, data.totalReview)
            tvDetailRating.text = String.format("%.1f", data.productRating)
        }
    }

    private fun setImageView(image: List<String>) {
        adapter.submitList(image)
        val viewPager = binding.includeProductDetail.pagerDetail
        viewPager.adapter = adapter

        if (image.size > 1) {
            TabLayoutMediator(binding.includeProductDetail.tabLayout, viewPager) { _, _ -> }
                .attach()
        }
    }

    private fun setChipVariants(
        productVariant: List<ProductDetailsModel.ProductVariant>,
        price: Int
    ) {
        productVariant.forEach { variant ->
            val chip = Chip(binding.includeProductDetail.chipVariantGroup.context)
            chip.setTextAppearanceResource(R.style.TextAppearance_Medium)
            chip.text = variant.variantName
            chip.isCheckable = true
            chip.isChecked = variant.variantName == productVariant[0].variantName

            chip.setOnClickListener {
                binding.includeProductDetail.tvDetailPrice.text =
                    price.plus(variant.variantPrice).toCurrencyFormat()
                productDetailsViewModel.productVariant = variant
            }

            binding.includeProductDetail.chipVariantGroup.addView(chip)
        }
    }

    private fun checkIsWishList() {
        productDetailsViewModel.isWishlist =
            productDetailsViewModel.checkExistWishlist()
        changeWishListIcon()
    }

    private fun changeWishListIcon() {
        binding.includeProductDetail.ivFavorite.setImageResource(
            if (productDetailsViewModel.isWishlist) {
                R.drawable.ic_favorite
            } else {
                R.drawable.ic_favorite_border
            }
        )
    }

    private fun initShare() {
        val sendIntent: Intent = Intent().apply {
            val text = """
                Name : ${productDetailsViewModel.detailProduct?.productName}
                Price : ${productDetailsViewModel.detailProduct?.productPrice?.toCurrencyFormat()}
                Link : https://ecommerce.rafi.com/products/${productDetailsViewModel.productId}
            """.trimIndent()
            action = Intent.ACTION_SEND
            putExtra(Intent.EXTRA_TEXT, text)
            type = "text/plain"
        }

        val shareIntent = Intent.createChooser(sendIntent, null)
        startActivity(shareIntent)
    }

    private fun initWishList() {
        if (productDetailsViewModel.isWishlist) {
            productDetailsViewModel.deleteWishlist()
            binding.root.showSnackbar(getString(R.string.remove_wishlist))
        } else {
            productDetailsViewModel.insertWishlist()
            binding.root.showSnackbar(getString(R.string.add_wishlist))
        }
        changeWishListIcon()
    }

    private fun initCart() {
        if (productDetailsViewModel.insertCart()) {
            binding.root.showSnackbar(getString(R.string.add_cart))
        } else {
            binding.root.showSnackbar(getString(R.string.stock_is_unavailable))
        }
    }

    private fun initReview() {
        findNavController().navigate(
            R.id.action_productDetailFragment2_to_reviewFragment,
            bundleOf(ReviewFragment.BUNDLE_PRODUCT_ID_KEY to productDetailsViewModel.productId)
        )
    }

    private fun initCheckout() {
        if (productDetailsViewModel.detailProduct != null && productDetailsViewModel.productVariant != null) {
            val data =
                productDetailsViewModel.detailProduct!!.toCart(productDetailsViewModel.productVariant!!)
            val bundle = Bundle().apply {
                putParcelableArrayList(CheckoutFragment.ARG_DATA, arrayListOf(data))
            }
            findNavController().navigate(
                R.id.action_productDetailFragment2_to_checkoutFragment,
                bundle
            )
        }
    }

    private fun showLoading(isLoading: Boolean) {
        with(binding) {
            pbProductDetail.isVisible = isLoading
            includeProductDetail.containerProductDetail.isVisible = !isLoading
        }
    }

    private fun initErrorMessage(error: Throwable) {
        val title = error.getErrorTitle(requireActivity())
        val subtitle = error.getErrorSubtitle(requireActivity())
        val action = getString(R.string.refresh_btn)

        binding.includeProductDetail.containerProductDetail.isVisible = false
        with(binding.includeErrorMessage) {
            tvErrorTitle.text = title
            tvErrorDesc.text = subtitle
            btnError.text = action

            btnError.setOnClickListener { productDetailsViewModel.getDetailProduct() }
            containerEmptyData.isVisible = true
        }
    }

    companion object {
        const val BUNDLE_PRODUCT_ID_KEY = "product_id"
    }
}
