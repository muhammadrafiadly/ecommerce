package com.rafi.ecommerce.ui.main.cart

import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.rafi.core.data.model.ProductCartModel
import com.rafi.core.data.repository.ProductRepository
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.first
import kotlinx.coroutines.launch
import javax.inject.Inject

@HiltViewModel
class CartViewModel @Inject constructor(
    private val productRepository: ProductRepository
) : ViewModel() {

    var totalPrice = 0

    fun getCartData(): Flow<List<ProductCartModel>> = productRepository.getProductCart()

    fun deleteItemFromCart(cart: ProductCartModel? = null) {
        viewModelScope.launch {
            if (cart != null) {
                productRepository.deleteCart(cart)
            } else {
                val filteredCart = getCartData().first().filter { it.isChecked }
                    .toTypedArray()
                productRepository.deleteCart(*filteredCart)
            }
        }
    }

    fun updateCartQuantity(cart: ProductCartModel, isInsert: Boolean) {
        viewModelScope.launch {
            if (isInsert && cart.stock > cart.quantity!!) {
                productRepository.updateCartQuantity(cart, true)
            } else if (!isInsert && cart.quantity!! > 1) {
                productRepository.updateCartQuantity(cart, false)
            }
        }
    }

    fun updateCartChecked(isChecked: Boolean, cart: ProductCartModel? = null) {
        viewModelScope.launch {
            if (cart != null) {
                productRepository.updateCartChecked(isChecked, cart)
            } else {
                productRepository.updateCartChecked(
                    isChecked,
                    *getCartData().first()
                        .toTypedArray()
                )
            }
        }
    }
}
