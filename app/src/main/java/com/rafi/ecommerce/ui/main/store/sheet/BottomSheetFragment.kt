package com.rafi.ecommerce.ui.main.store.sheet

import android.content.Context
import android.os.Bundle
import android.view.ContextThemeWrapper
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.os.bundleOf
import androidx.core.view.isInvisible
import androidx.core.widget.doOnTextChanged
import androidx.fragment.app.setFragmentResult
import com.google.android.material.bottomsheet.BottomSheetBehavior
import com.google.android.material.bottomsheet.BottomSheetDialog
import com.google.android.material.bottomsheet.BottomSheetDialogFragment
import com.google.firebase.analytics.FirebaseAnalytics
import com.google.firebase.analytics.ktx.analytics
import com.google.firebase.analytics.logEvent
import com.google.firebase.ktx.Firebase
import com.rafi.core.util.Constant
import com.rafi.ecommerce.R
import com.rafi.ecommerce.databinding.FragmentBottomSheetBinding

class BottomSheetFragment : BottomSheetDialogFragment() {

    private var sort: String? = null
    private var sortId: Int? = null
    private var brand: String? = null
    private var brandId: Int? = null
    private var lowest: String? = null
    private var highest: String? = null

    private var _binding: FragmentBottomSheetBinding? = null
    private val binding get() = _binding!!

    private lateinit var firebaseAnalytics: FirebaseAnalytics

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        arguments?.let {
            sort = it.getString(Constant.Filter.SORT)
            brand = it.getString(Constant.Filter.BRAND)
            lowest = it.getString(Constant.Filter.LOWEST)
            highest = it.getString(Constant.Filter.HIGHEST)
        }

        firebaseAnalytics = Firebase.analytics
    }

    override fun onGetLayoutInflater(savedInstanceState: Bundle?): LayoutInflater {
        val inflater = super.onGetLayoutInflater(savedInstanceState)
        val contextThemeWrapper: Context =
            ContextThemeWrapper(requireContext(), R.style.Theme_Ecommerce)
        return inflater.cloneInContext(contextThemeWrapper)
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        _binding = FragmentBottomSheetBinding.inflate(inflater, container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        val modalBottomSheetBehavior = (dialog as BottomSheetDialog).behavior
        modalBottomSheetBehavior.state = BottomSheetBehavior.STATE_EXPANDED
        initView()
        initAction()
    }

    private fun initAction() {
        binding.apply {
            tvReset.setOnClickListener {
                actionResetFilter()
            }
            btnShowProducts.setOnClickListener {
                actionReturnFilterData()
                initAnalytics()
            }
            etLowest.doOnTextChanged { _, _, _, _ -> changeResetButtonVisibility() }
            etHighest.doOnTextChanged { _, _, _, _ -> changeResetButtonVisibility() }
        }
    }

    private fun initAnalytics() {
        firebaseAnalytics.logEvent(FirebaseAnalytics.Event.SELECT_ITEM) {
            val logSort = bundleOf(
                FirebaseAnalytics.Param.ITEM_NAME to "$sort",
                FirebaseAnalytics.Param.ITEM_CATEGORY to "sort"
            )
            val logBrand = bundleOf(
                FirebaseAnalytics.Param.ITEM_NAME to "$brand",
                FirebaseAnalytics.Param.ITEM_CATEGORY to "brand"
            )
            val logLowest = bundleOf(
                FirebaseAnalytics.Param.ITEM_NAME to "$lowest",
                FirebaseAnalytics.Param.ITEM_CATEGORY to "lowest_price"
            )
            val logHighest = bundleOf(
                FirebaseAnalytics.Param.ITEM_NAME to "$highest",
                FirebaseAnalytics.Param.ITEM_CATEGORY to "highest_price"
            )
            param(FirebaseAnalytics.Param.ITEMS, arrayOf(logSort, logBrand, logLowest, logHighest))
        }
    }

    private fun actionReturnFilterData() {
        lowest = binding.etLowest.text.toString().let {
            if (it.isNullOrEmpty()) {
                null
            } else {
                it
            }
        }
        highest = binding.etHighest.text.toString().let {
            if (it.isNullOrEmpty()) {
                null
            } else {
                it
            }
        }
        val bundle = bundleOf(
            Constant.Filter.SORT_KEY to sort,
            Constant.Filter.SORT_ID_KEY to sortId,
            Constant.Filter.BRAND_KEY to brand,
            Constant.Filter.BRAND_ID_KEY to brandId,
            Constant.Filter.LOWEST_KEY to lowest,
            Constant.Filter.HIGHEST_KEY to highest
        )
        setFragmentResult(FRAGMENT_REQUEST_KEY, bundle)
        dismiss()
    }

    private fun actionResetFilter() {
        sort = null
        brand = null
        lowest = null
        highest = null
        sortId = null
        brandId = null
        binding.apply {
            cgSort.clearCheck()
            cgBrand.clearCheck()
            etLowest.setText("")
            etHighest.setText("")
        }
    }

    private fun initView() {
        binding.apply {
            etLowest.setText(lowest ?: "")
            etHighest.setText(highest ?: "")
        }
        initSortChipView()
        initBrandChipView()
        changeResetButtonVisibility()
    }

    private fun initBrandChipView() {
        val listBrandChips = mapOf(
            binding.chipApple to R.string.apple,
            binding.chipAsus to R.string.asus,
            binding.chipDell to R.string.dell,
            binding.chipLenovo to R.string.lenovo
        )

        listBrandChips.forEach { (chip, stringId) ->
            chip.setOnClickListener {
                val checkChipId = binding.cgBrand.checkedChipId
                if (checkChipId == -1) {
                    brand = null
                    brandId = null
                } else {
                    brand = chip.text.toString()
                    brandId = stringId
                }
                changeResetButtonVisibility()
            }

            if (!brand.isNullOrEmpty() && brand == getString(stringId)) {
                chip.isChecked = true
                brandId = stringId
            }
        }
    }

    private fun initSortChipView() {
        val listSortChips = mapOf(
            binding.chipReview to R.string.review,
            binding.chipSale to R.string.sale,
            binding.chipLowest to R.string.lowest_price,
            binding.chipHighest to R.string.highest_price
        )

        listSortChips.forEach { (chip, stringId) ->
            chip.setOnClickListener {
                val checkChipId = binding.cgSort.checkedChipId
                if (checkChipId == -1) {
                    sort = null
                    sortId = null
                } else {
                    sort = chip.text.toString()
                    sortId = stringId
                }
                changeResetButtonVisibility()
            }

            if (!sort.isNullOrEmpty() && sort == getString(stringId)) {
                chip.isChecked = true
                sortId = stringId
            }
        }
    }

    private fun changeResetButtonVisibility() {
        with(binding) {
            val isSortChipExisted = cgSort.checkedChipId == -1
            val isBrandChipExisted = cgBrand.checkedChipId == -1
            val isLowestPriceTextEmpty = etLowest.text.toString().isEmpty()
            val isHighestPriceTextEmpty = etHighest.text.toString().isEmpty()

            tvReset.isInvisible =
                isSortChipExisted && isBrandChipExisted && isLowestPriceTextEmpty && isHighestPriceTextEmpty
        }
    }

    override fun onDestroyView() {
        super.onDestroyView()
        _binding = null
    }

    companion object {
        const val TAG = "FilterSheetFragment"
        const val FRAGMENT_REQUEST_KEY = "filter_sheet_fragment_request_key"

        @JvmStatic
        fun newInstance(sort: String?, brand: String?, lowest: String?, highest: String?) =
            BottomSheetFragment().apply {
                arguments = Bundle().apply {
                    putString(Constant.Filter.SORT, sort)
                    putString(Constant.Filter.BRAND, brand)
                    putString(Constant.Filter.LOWEST, lowest)
                    putString(Constant.Filter.HIGHEST, highest)
                }
            }
    }
}
