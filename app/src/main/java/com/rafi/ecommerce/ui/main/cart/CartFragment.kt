package com.rafi.ecommerce.ui.main.cart

import android.os.Bundle
import android.view.View
import androidx.core.os.bundleOf
import androidx.core.view.isVisible
import androidx.fragment.app.viewModels
import androidx.lifecycle.Lifecycle
import androidx.lifecycle.lifecycleScope
import androidx.lifecycle.repeatOnLifecycle
import androidx.navigation.Navigation
import androidx.navigation.fragment.findNavController
import androidx.recyclerview.widget.LinearLayoutManager
import com.google.firebase.analytics.FirebaseAnalytics
import com.google.firebase.analytics.ktx.analytics
import com.google.firebase.analytics.ktx.logEvent
import com.google.firebase.ktx.Firebase
import com.rafi.core.data.model.ProductCartModel
import com.rafi.ecommerce.R
import com.rafi.ecommerce.databinding.FragmentCartBinding
import com.rafi.ecommerce.ui.main.fulfillment.checkout.CheckoutFragment
import com.rafi.ecommerce.ui.main.store.detail.ProductDetailFragment
import com.rafi.ecommerce.util.BaseFragment
import com.rafi.ecommerce.util.toCurrencyFormat
import dagger.hilt.android.AndroidEntryPoint
import kotlinx.coroutines.flow.first
import kotlinx.coroutines.launch
import kotlinx.coroutines.runBlocking

@Suppress("DEPRECATION")
@AndroidEntryPoint
class CartFragment :
    BaseFragment<FragmentCartBinding>(FragmentCartBinding::inflate) {

    private lateinit var firebaseAnalytics: FirebaseAnalytics
    private val cartViewModel: CartViewModel by viewModels()
    private val cartAdapter: CartAdapter by lazy {
        CartAdapter(object : CartAdapter.CartCallback {
            override fun deleteItemCallback(cart: ProductCartModel) {
                cartViewModel.deleteItemFromCart(cart)
                logCartEvent(arrayOf(cart), FirebaseAnalytics.Event.REMOVE_FROM_CART)
            }

            override fun itemClickCallback(productId: String) {
                moveToProductDetails(productId)
            }

            override fun increaseQuantityCallback(cart: ProductCartModel) {
                cartViewModel.updateCartQuantity(cart, true)
            }

            override fun decreaseQuantityCallback(cart: ProductCartModel) {
                cartViewModel.updateCartQuantity(cart, false)
            }

            override fun checkItemCallback(cart: ProductCartModel, isCheck: Boolean) {
                cartViewModel.updateCartChecked(isCheck, cart)
            }
        })
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        firebaseAnalytics = Firebase.analytics

        initAppBar()
        observeCartData()
        initAdapter()
        initBtn()
    }

    private fun logCartEvent(cart: Array<ProductCartModel>, event: String) {
        firebaseAnalytics.logEvent(event) {
            val bundle = ArrayList<Bundle>()
            cart.map { product ->
                val itemBundle = Bundle().apply {
                    putString(FirebaseAnalytics.Param.ITEM_ID, product.productId)
                    putString(FirebaseAnalytics.Param.ITEM_NAME, product.productName)
                    putString(FirebaseAnalytics.Param.ITEM_BRAND, product.brand)
                    putString(FirebaseAnalytics.Param.ITEM_VARIANT, product.variantName)
                }
                bundle.add(itemBundle)
            }
            param(FirebaseAnalytics.Param.ITEMS, bundle.toTypedArray())
        }
    }

    private fun initAppBar() {
        val toolbar = binding.appBarLayout.toolbar
        toolbar.title = getString(R.string.cart)
        toolbar.setNavigationIcon(R.drawable.ic_arrow_back)
        toolbar.setNavigationOnClickListener {
            findNavController().navigateUp()
        }
    }

    private fun observeCartData() {
        viewLifecycleOwner.lifecycleScope.launch {
            repeatOnLifecycle(Lifecycle.State.STARTED) {
                cartViewModel.getCartData().collect { data ->
                    val reversedData = data.reversed()
                    val itemCount = reversedData.size
                    if (itemCount == 0) {
                        showEmptyDataMessage()
                    } else {
                        hideEmptyDataMessage()
                    }
                    cartAdapter.submitList(reversedData)
                    logCartEvent(
                        reversedData.toTypedArray(),
                        FirebaseAnalytics.Event.VIEW_CART
                    )
                    initView(reversedData)
                }
            }
        }
    }

    private fun initBtn() {
        binding.includeCart.apply {
            btnDelete.setOnClickListener {
                cartViewModel.deleteItemFromCart()
                viewLifecycleOwner.lifecycleScope.launch {
                    val cart = cartViewModel.getCartData().first()
                        .filter { it.isChecked }.toTypedArray()
                    logCartEvent(cart, FirebaseAnalytics.Event.REMOVE_FROM_CART)
                }
            }
            checkboxAllCart.setOnClickListener {
                val isAllChecked = checkboxAllCart.isChecked
                cartViewModel.updateCartChecked(isAllChecked)
            }
            btnBuyFromCart.setOnClickListener {
                initCheckout()
            }
        }
    }

    private fun initView(listCart: List<ProductCartModel>) {
        binding.includeCart.checkboxAllCart.isChecked = listCart.all {
            it.isChecked
        }
        binding.includeCart.btnDelete.isVisible = listCart.any { it.isChecked }
        binding.includeCart.btnBuyFromCart.isEnabled = listCart.any { it.isChecked }
        cartViewModel.totalPrice = listCart.filter { it.isChecked }.sumOf {
            (it.productPrice + it.variantPrice) * it.quantity!!
        }
        binding.includeCart.tvCartPrice.text = cartViewModel.totalPrice.toCurrencyFormat()
    }

    private fun initAdapter() {
        binding.includeCart.rvCart.apply {
            adapter = cartAdapter
            layoutManager = LinearLayoutManager(requireActivity())
            itemAnimator = null
        }
    }

    private fun moveToProductDetails(productId: String) {
        Navigation.findNavController(requireActivity(), R.id.navHostFragment)
            .navigate(
                R.id.action_cartFragment2_to_productDetailFragmentCompose,
                bundleOf(
                    ProductDetailFragment.BUNDLE_PRODUCT_ID_KEY to productId
                )
            )
    }

    private fun initCheckout() {
        val listData = runBlocking {
            cartViewModel.getCartData().first().filter {
                it.isChecked
            }
        }
        val bundle = Bundle().apply {
            putParcelableArrayList(CheckoutFragment.ARG_DATA, ArrayList<ProductCartModel>(listData))
        }
        findNavController().navigate(R.id.action_cartFragment2_to_checkoutFragment, bundle)
    }

    private fun showEmptyDataMessage() {
        binding.includeEmptyData.root.visibility = View.VISIBLE
        binding.includeCart.root.visibility = View.GONE
        with(binding.includeEmptyData) {
            tvErrorTitle.text = getString(R.string.empty_title)
            tvErrorDesc.text = getString(R.string.empty_data_desc)
        }
    }

    private fun hideEmptyDataMessage() {
        binding.includeEmptyData.root.visibility = View.GONE
        binding.includeCart.root.visibility = View.VISIBLE
    }
}
