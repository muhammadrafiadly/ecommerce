package com.rafi.ecommerce.ui.main.store.detail.compose.component

import androidx.compose.foundation.ExperimentalFoundationApi
import androidx.compose.foundation.background
import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.height
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.layout.size
import androidx.compose.foundation.layout.wrapContentHeight
import androidx.compose.foundation.pager.HorizontalPager
import androidx.compose.foundation.pager.rememberPagerState
import androidx.compose.foundation.shape.CircleShape
import androidx.compose.material3.MaterialTheme
import androidx.compose.runtime.Composable
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.draw.clip
import androidx.compose.ui.platform.LocalContext
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.dp
import coil.compose.AsyncImage
import coil.request.ImageRequest
import com.rafi.ecommerce.R

@OptIn(ExperimentalFoundationApi::class)
@Composable
fun ProductImage(dataImage: List<String>) {
    val pagerState = rememberPagerState(pageCount = {
        dataImage.size
    })
    Box {
        HorizontalPager(state = pagerState) {
            AsyncImage(
                model = ImageRequest.Builder(LocalContext.current)
                    .data(dataImage[it])
                    .crossfade(true)
                    .build(),
                placeholder = painterResource(id = R.drawable.image_blank_product),
                error = painterResource(id = R.drawable.image_blank_product),
                contentDescription = null,
                modifier = Modifier
                    .height(309.dp)
                    .fillMaxWidth()
            )
        }
        if (dataImage.size > 1) {
            Row(
                Modifier
                    .wrapContentHeight()
                    .fillMaxWidth()
                    .align(Alignment.BottomCenter)
                    .padding(bottom = 8.dp),
                horizontalArrangement = Arrangement.Center
            ) {
                repeat(pagerState.pageCount) { iteration ->
                    val color =
                        if (pagerState.currentPage == iteration) {
                            MaterialTheme.colorScheme.primary
                        } else {
                            MaterialTheme.colorScheme.outlineVariant
                        }
                    Box(
                        modifier = Modifier
                            .padding(8.dp)
                            .clip(CircleShape)
                            .background(color)
                            .size(8.dp)
                    )
                }
            }
        }
    }
}

@Preview(showBackground = true)
@Composable
fun ProductImagePreview() {
    val dummyImages = listOf(
        "url_of_image_1",
        "url_of_image_2",
        "url_of_image_3"
    )
    ProductImage(dataImage = dummyImages)
}
