package com.rafi.ecommerce.ui.main.store.detail.compose

import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.imePadding
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.rememberScrollState
import androidx.compose.foundation.verticalScroll
import androidx.compose.material3.CircularProgressIndicator
import androidx.compose.material3.DismissValue
import androidx.compose.material3.Divider
import androidx.compose.material3.ExperimentalMaterial3Api
import androidx.compose.material3.Scaffold
import androidx.compose.material3.SnackbarHost
import androidx.compose.material3.SnackbarHostState
import androidx.compose.material3.SwipeToDismiss
import androidx.compose.material3.rememberDismissState
import androidx.compose.runtime.Composable
import androidx.compose.runtime.LaunchedEffect
import androidx.compose.runtime.collectAsState
import androidx.compose.runtime.remember
import androidx.compose.runtime.rememberCoroutineScope
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.platform.ComposeView
import androidx.compose.ui.platform.LocalContext
import androidx.compose.ui.unit.dp
import androidx.core.os.bundleOf
import androidx.fragment.app.Fragment
import androidx.fragment.app.viewModels
import androidx.navigation.fragment.findNavController
import com.google.firebase.analytics.FirebaseAnalytics
import com.google.firebase.analytics.ktx.analytics
import com.google.firebase.analytics.logEvent
import com.google.firebase.ktx.Firebase
import com.rafi.core.data.model.ProductDetailsModel
import com.rafi.core.util.Constant
import com.rafi.core.util.DataMapper.toCart
import com.rafi.core.util.Resources
import com.rafi.ecommerce.R
import com.rafi.ecommerce.ui.main.fulfillment.checkout.CheckoutFragment
import com.rafi.ecommerce.ui.main.fulfillment.review.compose.ReviewFragmentCompose
import com.rafi.ecommerce.ui.main.store.detail.compose.component.BottomButton
import com.rafi.ecommerce.ui.main.store.detail.compose.component.DetailAppBar
import com.rafi.ecommerce.ui.main.store.detail.compose.component.ErrorMessage
import com.rafi.ecommerce.ui.main.store.detail.compose.component.ProductDescription
import com.rafi.ecommerce.ui.main.store.detail.compose.component.ProductImage
import com.rafi.ecommerce.ui.main.store.detail.compose.component.ProductVariant
import com.rafi.ecommerce.ui.main.store.detail.compose.component.ReviewSection
import com.rafi.ecommerce.ui.main.store.detail.compose.component.TitleSection
import com.rafi.ecommerce.ui.theme.ComposeAppTheme
import com.rafi.ecommerce.util.Helper.sendLogButtonClicked
import com.rafi.ecommerce.util.toCurrencyFormat
import dagger.hilt.android.AndroidEntryPoint
import kotlinx.coroutines.delay
import kotlinx.coroutines.launch

@AndroidEntryPoint
class ProductDetailFragmentCompose : Fragment() {

    private val productDetailsComposeViewModel: ProductDetailsComposeViewModel by viewModels()

    private val firebaseAnalytics: FirebaseAnalytics by lazy {
        Firebase.analytics
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        return ComposeView(requireContext()).apply {
            setContent {
                ComposeAppTheme {
                    DetailFragmentScreen(
                        firebaseAnalytics = firebaseAnalytics,
                        viewModel = productDetailsComposeViewModel,
                        onBackPressed = {
                            findNavController().navigateUp()
                        },
                        onClickBuyNow = {
                            val product = productDetailsComposeViewModel.composeState
                                .value.detailProduct
                            val variant = productDetailsComposeViewModel.composeState
                                .value.productVariant
                            val data = product!!.toCart(variant!!)
                            val bundle = Bundle().apply {
                                putParcelableArrayList(
                                    CheckoutFragment.ARG_DATA,
                                    arrayListOf(data)
                                )
                            }
                            findNavController().navigate(
                                R.id.action_productDetailFragmentCompose_to_checkoutFragment,
                                bundle
                            )
                        },
                        onErrorAction = { productDetailsComposeViewModel.getDetailProduct() }
                    ) {
                        findNavController().navigate(
                            R.id.action_productDetailFragmentCompose_to_reviewFragmentCompose,
                            bundleOf(
                                ReviewFragmentCompose.BUNDLE_PRODUCT_ID_KEY
                                    to productDetailsComposeViewModel.productId
                            )
                        )
                    }
                }
            }
        }
    }

    companion object {
        const val BUNDLE_PRODUCT_ID_KEY = "product_id"
    }
}

@OptIn(ExperimentalMaterial3Api::class)
@Composable
fun DetailFragmentScreen(
    firebaseAnalytics: FirebaseAnalytics,
    viewModel: ProductDetailsComposeViewModel,
    onBackPressed: () -> Unit,
    onClickBuyNow: () -> Unit,
    onErrorAction: () -> Unit,
    onMoreClicked: () -> Unit
) {
    val context = LocalContext.current

    val state = viewModel.composeState.collectAsState()
    val resultState = state.value.resultState

    val scope = rememberCoroutineScope()
    val snackBarHostState = remember { SnackbarHostState() }
    val dismissSnackBarState = rememberDismissState()

    LaunchedEffect(dismissSnackBarState.currentValue) {
        if (dismissSnackBarState.currentValue != DismissValue.Default) {
            snackBarHostState.currentSnackbarData?.dismiss()
            delay(Constant.Number.SNACK_BAR_DELAY)
            dismissSnackBarState.snapTo(DismissValue.Default)
        }
    }

    Scaffold(
        topBar = { DetailAppBar(onBackPressed) },
        bottomBar = {
            if (resultState is Resources.Success) {
                BottomButton(
                    onClickBuyNow = { onClickBuyNow() },
                    onClickAddCart = {
                        if (viewModel.insertCart()) {
                            sendLogWishlistOrCart(
                                firebaseAnalytics,
                                FirebaseAnalytics.Event.ADD_TO_CART,
                                state.value
                            )
                            scope.launch {
                                snackBarHostState.currentSnackbarData?.dismiss()
                                snackBarHostState.showSnackbar(
                                    context.getString(R.string.add_cart)
                                )
                            }
                        } else {
                            scope.launch {
                                snackBarHostState.currentSnackbarData?.dismiss()
                                snackBarHostState.showSnackbar(
                                    context.getString(R.string.stock_is_unavailable)
                                )
                            }
                        }
                    },
                )
            }
        },
        snackbarHost = {
            SwipeToDismiss(
                state = dismissSnackBarState,
                background = {},
                dismissContent = {
                    SnackbarHost(hostState = snackBarHostState, modifier = Modifier.imePadding())
                }
            )
        }
    ) { innerPadding ->
        Column(
            Modifier
                .padding(innerPadding)
                .fillMaxSize()
        ) {
            if (resultState is Resources.Success) {
                sendLogViewItem(firebaseAnalytics, resultState.data)
            }
            when (resultState) {
                is Resources.Loading -> Loading(Modifier.fillMaxSize())
                is Resources.Success -> DetailContent(
                    dataState = state.value,
                    onSharedClick = {
                        firebaseAnalytics.sendLogButtonClicked("Detail: Share")
                        actionShare(context, state.value)
                    },
                    onWishlistClick = {
                        firebaseAnalytics.sendLogButtonClicked("Detail: Wishlist")
                        if (state.value.isWishlist) {
                            viewModel.deleteWishlist()
                            scope.launch {
                                snackBarHostState.currentSnackbarData?.dismiss()
                                snackBarHostState.showSnackbar(
                                    context.getString(R.string.remove_wishlist)
                                )
                            }
                        } else {
                            viewModel.insertWishlist()
                            sendLogWishlistOrCart(
                                firebaseAnalytics,
                                FirebaseAnalytics.Event.ADD_TO_WISHLIST,
                                state.value
                            )
                            scope.launch {
                                snackBarHostState.currentSnackbarData?.dismiss()
                                snackBarHostState.showSnackbar(
                                    context.getString(R.string.add_wishlist)
                                )
                            }
                        }
                    },
                    onVariantClicked = { viewModel.updateVariant(it) },
                    onMoreClicked = { onMoreClicked() },
                    modifier = Modifier
                        .fillMaxSize()
                        .verticalScroll(rememberScrollState())
                )

                is Resources.Error -> ErrorMessage(
                    Modifier
                        .fillMaxSize()
                        .verticalScroll(rememberScrollState()),
                    resultState.message,
                    onErrorAction
                )

                else -> {}
            }
        }
    }
}

private fun sendLogViewItem(firebaseAnalytics: FirebaseAnalytics, data: ProductDetailsModel) {
    firebaseAnalytics.logEvent(FirebaseAnalytics.Event.VIEW_ITEM) {
        val bundleProduct = Bundle().apply {
            putString(FirebaseAnalytics.Param.ITEM_ID, data.productId)
            putString(FirebaseAnalytics.Param.ITEM_NAME, data.productName)
            putString(FirebaseAnalytics.Param.ITEM_BRAND, data.brand)
        }
        param(FirebaseAnalytics.Param.ITEMS, arrayOf(bundleProduct))
    }
}

private fun sendLogWishlistOrCart(
    firebaseAnalytics: FirebaseAnalytics,
    event: String,
    state: DetailComposeState
) {
    firebaseAnalytics.logEvent(event) {
        val bundleProduct = Bundle().apply {
            putString(FirebaseAnalytics.Param.ITEM_ID, state.detailProduct!!.productId)
            putString(FirebaseAnalytics.Param.ITEM_NAME, state.detailProduct.productName)
            putString(FirebaseAnalytics.Param.ITEM_BRAND, state.detailProduct.brand)
            putString(FirebaseAnalytics.Param.ITEM_VARIANT, state.productVariant!!.variantName)
        }
        param(FirebaseAnalytics.Param.ITEMS, arrayOf(bundleProduct))
    }
}

private fun actionShare(context: Context, state: DetailComposeState) {
    val sendIntent: Intent = Intent().apply {
        val text = """
                Name : ${state.detailProduct?.productName}
                Price : ${state.detailProduct?.productPrice?.toCurrencyFormat()}
                Link : https://ecommerce.rafi.com/products/${state.productId}
        """.trimIndent()
        action = Intent.ACTION_SEND
        putExtra(Intent.EXTRA_TEXT, text)
        type = "text/plain"
    }

    val shareIntent = Intent.createChooser(sendIntent, null)
    context.startActivity(shareIntent)
}

@Composable
fun Loading(modifier: Modifier = Modifier) {
    Column(
        modifier,
        verticalArrangement = Arrangement.Center,
        horizontalAlignment = Alignment.CenterHorizontally
    ) {
        CircularProgressIndicator()
    }
}

@Composable
fun DetailContent(
    dataState: DetailComposeState,
    onSharedClick: () -> Unit,
    onWishlistClick: () -> Unit,
    onVariantClicked: (ProductDetailsModel.ProductVariant) -> Unit,
    onMoreClicked: () -> Unit,
    modifier: Modifier = Modifier
) {
    Column(modifier) {
        ProductImage(dataState.detailProduct!!.image)
        TitleSection(
            dataState.detailProduct,
            dataState.productVariant!!,
            dataState.isWishlist,
            onSharedClick,
            onWishlistClick,
            Modifier.padding(horizontal = 16.dp, vertical = 12.dp)
        )
        Divider()
        ProductVariant(
            dataState.detailProduct,
            dataState.productVariant,
            onVariantClicked,
            Modifier.padding(horizontal = 16.dp, vertical = 12.dp)
        )
        Divider()
        ProductDescription(
            dataState.detailProduct,
            Modifier.padding(horizontal = 16.dp, vertical = 12.dp)
        )
        Divider()
        ReviewSection(
            dataState.detailProduct,
            onMoreClicked,
            Modifier.padding(horizontal = 16.dp, vertical = 12.dp)
        )
    }
}
