package com.rafi.ecommerce.ui.main.fulfillment.review.compose.components

import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.layout.size
import androidx.compose.foundation.shape.CircleShape
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.filled.Star
import androidx.compose.material3.Divider
import androidx.compose.material3.Icon
import androidx.compose.material3.MaterialTheme
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.ui.Modifier
import androidx.compose.ui.draw.clip
import androidx.compose.ui.layout.ContentScale
import androidx.compose.ui.platform.LocalContext
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.text.PlatformTextStyle
import androidx.compose.ui.text.TextStyle
import androidx.compose.ui.text.font.FontWeight
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import coil.compose.AsyncImage
import coil.request.ImageRequest
import com.rafi.core.data.model.ProductReviewsModel
import com.rafi.ecommerce.R
import com.rafi.ecommerce.ui.theme.ComposeAppTheme
import com.rafi.ecommerce.ui.theme.poppinsFamily

@Composable
fun ReviewItem(data: ProductReviewsModel) {
    Column {
        ReviewHeader(data = data)
        Text(
            text = data.userReview,
            modifier = Modifier.padding(16.dp, 8.dp, 16.dp, 0.dp),
            fontSize = 12.sp,
            style = reviewItemTextStyle()
        )
        Divider(modifier = Modifier.padding(top = 16.dp))
    }
}

@Composable
fun ReviewHeader(data: ProductReviewsModel) {
    Row(Modifier.padding(16.dp, 16.dp, 16.dp, 0.dp)) {
        UserImage(imageUrl = data.userImage)
        UserInfo(userName = data.userName, userRating = data.userRating)
    }
}

@Composable
fun UserInfo(userName: String, userRating: Int) {
    Column(Modifier.padding(start = 8.dp)) {
        Text(
            text = userName,
            fontSize = 12.sp,
            style = reviewItemTextStyle(fontWeight = FontWeight.SemiBold)
        )
        RatingStars(userRating = userRating)
    }
}

@Composable
fun RatingStars(userRating: Int) {
    Row {
        for (i in 1..5) {
            val color =
                if (i <= userRating) {
                    MaterialTheme.colorScheme.onSurfaceVariant
                } else {
                    MaterialTheme.colorScheme.outlineVariant
                }
            Icon(
                imageVector = Icons.Default.Star,
                contentDescription = null,
                tint = color,
                modifier = Modifier.size(16.dp)
            )
        }
    }
}

@Composable
fun UserImage(imageUrl: String) {
    AsyncImage(
        model = ImageRequest.Builder(LocalContext.current)
            .data(imageUrl)
            .crossfade(true)
            .build(),
        placeholder = painterResource(id = R.drawable.product_image_placeholder),
        error = painterResource(id = R.drawable.product_image_placeholder),
        contentDescription = null,
        contentScale = ContentScale.Crop,
        modifier = Modifier
            .size(36.dp)
            .clip(CircleShape)
    )
}

@Composable
fun reviewItemTextStyle(fontWeight: FontWeight = FontWeight.Normal): TextStyle {
    return TextStyle(
        fontFamily = poppinsFamily,
        fontWeight = fontWeight,
        platformStyle = PlatformTextStyle(
            includeFontPadding = false,
        )
    )
}

@Preview(showBackground = true)
@Composable
fun ReviewItemPreview() {
    val sampleReview = ProductReviewsModel(
        userImage = "url_to_user_image",
        userName = "John Doe",
        userReview = "This is a great product!",
        userRating = 4.5f.toInt()
    )

    ComposeAppTheme {
        ReviewItem(data = sampleReview)
    }
}
