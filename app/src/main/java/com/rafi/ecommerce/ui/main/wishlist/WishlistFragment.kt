package com.rafi.ecommerce.ui.main.wishlist

import android.os.Bundle
import android.util.Log
import android.view.View
import androidx.core.os.bundleOf
import androidx.fragment.app.viewModels
import androidx.lifecycle.Lifecycle
import androidx.lifecycle.lifecycleScope
import androidx.lifecycle.repeatOnLifecycle
import androidx.navigation.Navigation
import androidx.recyclerview.widget.GridLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.rafi.core.data.model.WishListProductsModel
import com.rafi.ecommerce.R
import com.rafi.ecommerce.databinding.FragmentWishlistBinding
import com.rafi.ecommerce.ui.main.store.detail.ProductDetailFragment
import com.rafi.ecommerce.util.BaseFragment
import com.rafi.ecommerce.util.showSnackbar
import dagger.hilt.android.AndroidEntryPoint
import kotlinx.coroutines.launch

@AndroidEntryPoint
class WishlistFragment : BaseFragment<FragmentWishlistBinding>(FragmentWishlistBinding::inflate) {

    private val wishlistViewModel: WishlistViewModel by viewModels()
    private lateinit var gridLayoutManager: GridLayoutManager

    private val wishlistAdapter: WishlistAdapter by lazy {
        WishlistAdapter(object : WishlistAdapter.WishlistCallback {
            override fun itemClickCallback(productId: String) {
                moveToProductDetails(productId)
            }

            override fun deleteCallback(wishlist: WishListProductsModel) {
                wishlistViewModel.deleteItemFromWishList(wishlist)
            }

            override fun addToCartCallback(wishlist: WishListProductsModel) {
                initAddToCart(wishlist)
            }
        })
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        observerWishList()
        initAdapter()
        initChangeButton()
        initRecyclerView()
        initButton()
    }

    private fun observerWishList() {
        viewLifecycleOwner.lifecycleScope.launch {
            repeatOnLifecycle(Lifecycle.State.STARTED) {
                wishlistViewModel.wishListProduct().collect { data ->
                    val reverseData = data.reversed()
                    val itemCount = reverseData.size
                    if (itemCount == 0) {
                        showEmptyDataMessage()
                    } else {
                        binding.includeWishlist.tvTotalWishListItem.text =
                            getString(R.string.total_item, itemCount)
                        hideEmptyDataMessage()
                    }
                    wishlistAdapter.submitList(reverseData)
                }
            }
        }
    }

    private fun moveToProductDetails(productId: String) {
        Navigation.findNavController(requireActivity(), R.id.navHostFragment)
            .navigate(
                R.id.action_mainFragment_to_productDetailFragmentCompose,
                bundleOf(
                    ProductDetailFragment.BUNDLE_PRODUCT_ID_KEY to productId
                )
            )
    }

    private fun initButton() {
        binding.includeWishlist.apply {
            btnChangeLayout.setOnClickListener {
                wishlistViewModel.recyclerViewType =
                    if (wishlistViewModel.recyclerViewType == WishlistAdapter.LINEAR_TYPE) {
                        WishlistAdapter.GRID_TYPE
                    } else {
                        WishlistAdapter.LINEAR_TYPE
                    }
                initChangeButton()
                initRecyclerView()
            }
        }
    }

    private fun initAdapter() {
        gridLayoutManager = GridLayoutManager(requireActivity(), 1)

        binding.includeWishlist.apply {
            rvWishList.adapter = wishlistAdapter
            rvWishList.layoutManager = gridLayoutManager
        }

        wishlistAdapter.registerAdapterDataObserver(object : RecyclerView
            .AdapterDataObserver() {
            override fun onItemRangeInserted(positionStart: Int, itemCount: Int) {
                super.onItemRangeInserted(positionStart, itemCount)
                binding.includeWishlist.rvWishList.scrollToPosition(0)

                Log.d(
                    "WishlistFragment",
                    "Item count in adapter: ${wishlistAdapter.itemCount}"
                )
            }
        })
    }

    private fun initChangeButton() {
        binding.includeWishlist.btnChangeLayout.setImageResource(
            if (wishlistViewModel.recyclerViewType == WishlistAdapter.LINEAR_TYPE) {
                R.drawable.ic_list_bulleted
            } else {
                R.drawable.ic_grid
            }
        )
    }

    private fun initRecyclerView() {
        if (wishlistViewModel.recyclerViewType == WishlistAdapter.LINEAR_TYPE) {
            wishlistAdapter.viewType = WishlistAdapter.LINEAR_TYPE
            gridLayoutManager.spanCount = 1
        } else {
            wishlistAdapter.viewType = WishlistAdapter.GRID_TYPE
            gridLayoutManager.spanCount = 2
        }
    }

    private fun initAddToCart(wishList: WishListProductsModel) {
        if (wishlistViewModel.addToCart(wishList)) {
            binding.root.showSnackbar(getString(R.string.add_cart))
        } else {
            binding.root.showSnackbar(getString(R.string.stock_is_unavailable))
        }
    }

    private fun showEmptyDataMessage() {
        binding.includeEmptyData.root.visibility = View.VISIBLE
        binding.includeWishlist.root.visibility = View.GONE
        with(binding.includeEmptyData) {
            tvErrorTitle.text = getString(R.string.empty_title)
            tvErrorDesc.text = getString(R.string.empty_data_desc)
        }
    }

    private fun hideEmptyDataMessage() {
        binding.includeEmptyData.root.visibility = View.GONE
        binding.includeWishlist.root.visibility = View.VISIBLE
    }
}
