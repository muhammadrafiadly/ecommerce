package com.rafi.ecommerce.ui.main.fulfillment.rating

import androidx.lifecycle.SavedStateHandle
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.rafi.core.data.model.FulfillmentModel
import com.rafi.core.data.repository.FulfillmentRepository
import com.rafi.core.util.Resources
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.StateFlow
import kotlinx.coroutines.launch
import javax.inject.Inject

@HiltViewModel
class RatingViewModel @Inject constructor(
    private val fulfillmentRepository: FulfillmentRepository,
    savedStateHandle: SavedStateHandle
) : ViewModel() {

    val transactionDetail = savedStateHandle.get<FulfillmentModel>(RatingFragment.DATA_BUNDLE_KEY)
    val flag = savedStateHandle.get<String>("flag")
    val rating = savedStateHandle.get<Int>(RatingFragment.RATING_BUNDLE_KEY)
    val review = savedStateHandle.get<String>(RatingFragment.REVIEW_BUNDLE_KEY)

    private val _state = MutableStateFlow<Resources<Boolean>?>(null)
    val state: StateFlow<Resources<Boolean>?> = _state

    fun postRating(
        invoiceId: String,
        rating: Int?,
        review: String?
    ) {
        viewModelScope.launch {
            fulfillmentRepository.rating(invoiceId, rating, review).collect { result ->
                _state.value = result
            }
        }
    }
}
