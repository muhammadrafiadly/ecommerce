package com.rafi.ecommerce.ui.main.fulfillment.payment

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.ListAdapter
import androidx.recyclerview.widget.RecyclerView
import coil.load
import com.rafi.core.data.model.PaymentModel
import com.rafi.ecommerce.R
import com.rafi.ecommerce.databinding.ItemPaymentListBinding

class PaymentItemAdapter(
    private val itemCallback: (PaymentModel.PaymentItem) -> Unit
) : ListAdapter<PaymentModel.PaymentItem, PaymentItemAdapter.PaymentItemViewHolder>(
    PaymentItemComparator
) {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): PaymentItemViewHolder {
        val binding = ItemPaymentListBinding
            .inflate(LayoutInflater.from(parent.context), parent, false)
        return PaymentItemViewHolder(binding, itemCallback)
    }

    override fun onBindViewHolder(holder: PaymentItemViewHolder, position: Int) {
        holder.bind(getItem(position))
    }

    class PaymentItemViewHolder(
        private val binding: ItemPaymentListBinding,
        private val itemClickCallback: (PaymentModel.PaymentItem) -> Unit
    ) : RecyclerView.ViewHolder(binding.root) {
        fun bind(data: PaymentModel.PaymentItem) {
            binding.ivPaymentLogo.load(data.image) {
                crossfade(true)
                placeholder(R.drawable.product_image_placeholder)
                error(R.drawable.product_image_placeholder)
            }
            binding.tvPaymentName.text = data.label
            if (!data.status) {
                with(binding.cvPaymentList) {
                    alpha = 0.5F
                    isClickable = false
                    isFocusable = false
//                    foreground = null
//                    setBackgroundColor(
//                        Helper.getColorTheme(
//                            itemView.context,
//                            com.google.android.material.R.attr.colorTertiaryContainer
//                        )
//                    )
                }
                binding.cvPaymentList.alpha = 0.5F
            } else {
                itemView.setOnClickListener { itemClickCallback(data) }
            }
        }
    }

    object PaymentItemComparator : DiffUtil.ItemCallback<PaymentModel.PaymentItem>() {
        override fun areItemsTheSame(
            oldItem: PaymentModel.PaymentItem,
            newItem: PaymentModel.PaymentItem
        ): Boolean = oldItem.label == newItem.label

        override fun areContentsTheSame(
            oldItem: PaymentModel.PaymentItem,
            newItem: PaymentModel.PaymentItem
        ): Boolean = oldItem == newItem
    }
}
