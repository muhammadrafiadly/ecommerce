package com.rafi.ecommerce.ui.main.fulfillment.review.compose.components

import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.material3.Divider
import androidx.compose.material3.ExperimentalMaterial3Api
import androidx.compose.material3.Icon
import androidx.compose.material3.IconButton
import androidx.compose.material3.Text
import androidx.compose.material3.TopAppBar
import androidx.compose.runtime.Composable
import androidx.compose.ui.Modifier
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.text.PlatformTextStyle
import androidx.compose.ui.text.TextStyle
import androidx.compose.ui.text.font.FontWeight
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.sp
import com.rafi.ecommerce.R
import com.rafi.ecommerce.ui.theme.poppinsFamily

@OptIn(ExperimentalMaterial3Api::class)
@Composable
fun ReviewAppBar(onBackAction: () -> Unit) {
    Column(Modifier.fillMaxWidth()) {
        TopAppBar(
            title = {
                Text(
                    stringResource(id = R.string.buyer_reviews),
                    fontSize = 22.sp,
                    style = TextStyle(
                        fontFamily = poppinsFamily,
                        fontWeight = FontWeight.Normal,
                        platformStyle = PlatformTextStyle(
                            includeFontPadding = false,
                        ),
                    )
                )
            },
            navigationIcon = {
                IconButton(onClick = { onBackAction() }) {
                    Icon(
                        painter = painterResource(id = R.drawable.ic_arrow_back),
                        contentDescription = null
                    )
                }
            }
        )
        Divider()
    }
}

@Preview(showBackground = true)
@Composable
fun ReviewAppBarPreview() {
    val dummyOnBackAction: () -> Unit = {}
    ReviewAppBar(onBackAction = dummyOnBackAction)
}
