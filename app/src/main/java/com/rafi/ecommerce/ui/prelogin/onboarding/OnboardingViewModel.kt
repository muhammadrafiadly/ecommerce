package com.rafi.ecommerce.ui.prelogin.onboarding

import androidx.lifecycle.ViewModel
import com.rafi.core.data.repository.UserRepository
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.runBlocking
import javax.inject.Inject

@HiltViewModel
class OnboardingViewModel @Inject constructor(
    private val userRepository: UserRepository
) : ViewModel() {

    fun setOnboard(isOnboard: Boolean = true) {
        runBlocking {
            userRepository.setOnboarding(isOnboard)
        }
    }
}
