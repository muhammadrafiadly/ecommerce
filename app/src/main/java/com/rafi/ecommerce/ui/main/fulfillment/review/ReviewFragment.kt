package com.rafi.ecommerce.ui.main.fulfillment.review

import android.os.Bundle
import android.view.View
import androidx.core.view.isVisible
import androidx.fragment.app.viewModels
import androidx.lifecycle.Lifecycle
import androidx.lifecycle.lifecycleScope
import androidx.lifecycle.repeatOnLifecycle
import androidx.navigation.fragment.findNavController
import androidx.recyclerview.widget.LinearLayoutManager
import com.rafi.core.util.Resources
import com.rafi.ecommerce.R
import com.rafi.ecommerce.databinding.FragmentReviewBinding
import com.rafi.ecommerce.util.BaseFragment
import com.rafi.ecommerce.util.ThrowableExt.getErrorSubtitle
import com.rafi.ecommerce.util.ThrowableExt.getErrorTitle
import dagger.hilt.android.AndroidEntryPoint
import kotlinx.coroutines.launch

@AndroidEntryPoint
class ReviewFragment :
    BaseFragment<FragmentReviewBinding>(FragmentReviewBinding::inflate) {

    private val reviewViewModel: ReviewViewModel by viewModels()

    private val reviewAdapter = ReviewAdapter()

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        initAppBar()
        initRecyclerView()
        observeReviewProduct()
    }

    private fun initAppBar() {
        val toolbar = binding.appBarLayout.toolbar
        toolbar.title = getString(R.string.buyer_reviews)
        toolbar.setNavigationIcon(R.drawable.ic_arrow_back)
        toolbar.setNavigationOnClickListener {
            findNavController().navigateUp()
        }
    }

    private fun initRecyclerView() {
        binding.rvReview.apply {
            adapter = reviewAdapter
            layoutManager = LinearLayoutManager(requireActivity())
        }
    }

    private fun observeReviewProduct() {
        viewLifecycleOwner.lifecycleScope.launch {
            repeatOnLifecycle(Lifecycle.State.STARTED) {
                reviewViewModel.reviewProductState.collect { result ->
                    if (result != null) {
                        showLoading(result is Resources.Loading)
                        when (result) {
                            is Resources.Loading -> {}
                            is Resources.Success -> {
                                reviewAdapter.submitList(result.data)
                            }

                            is Resources.Error -> {
                                initErrorMessage(result.message)
                            }
                        }
                    }
                }
            }
        }
    }

    private fun showLoading(isLoading: Boolean) {
        with(binding) {
            pbReview.isVisible = isLoading
            rvReview.isVisible = !isLoading
            includeErrorMessage.containerEmptyData.isVisible = false
        }
    }

    private fun initErrorMessage(error: Throwable) {
        val title = error.getErrorTitle(requireActivity())
        val subtitle = error.getErrorSubtitle(requireActivity())
        val action = getString(R.string.refresh_btn)

        binding.rvReview.isVisible = false
        with(binding.includeErrorMessage) {
            tvErrorTitle.text = title
            tvErrorDesc.text = subtitle
            btnError.text = action

            btnError.setOnClickListener { reviewViewModel.getListReview() }
            containerEmptyData.isVisible = true
        }
    }

    companion object {
        const val BUNDLE_PRODUCT_ID_KEY = "product_id"
    }
}
