package com.rafi.ecommerce.ui.main

import android.content.res.Configuration
import android.os.Bundle
import android.util.Log
import android.view.View
import android.view.ViewGroup
import androidx.core.content.ContextCompat
import androidx.fragment.app.viewModels
import androidx.lifecycle.Lifecycle
import androidx.lifecycle.lifecycleScope
import androidx.lifecycle.repeatOnLifecycle
import androidx.navigation.fragment.NavHostFragment
import androidx.navigation.fragment.findNavController
import androidx.navigation.ui.setupWithNavController
import androidx.window.layout.WindowMetricsCalculator
import com.google.android.material.badge.BadgeDrawable
import com.google.android.material.badge.BadgeUtils
import com.google.android.material.badge.ExperimentalBadgeUtils
import com.google.firebase.analytics.FirebaseAnalytics
import com.google.firebase.analytics.ktx.analytics
import com.google.firebase.ktx.Firebase
import com.rafi.ecommerce.R
import com.rafi.ecommerce.databinding.FragmentMainBinding
import com.rafi.ecommerce.util.BaseFragment
import com.rafi.ecommerce.util.Helper.sendLogButtonClicked
import com.rafi.ecommerce.util.showSnackbar
import dagger.hilt.android.AndroidEntryPoint
import kotlinx.coroutines.launch

@androidx.annotation.OptIn(ExperimentalBadgeUtils::class)
@AndroidEntryPoint
class MainFragment : BaseFragment<FragmentMainBinding>(FragmentMainBinding::inflate) {

    private val mainViewModel: MainViewModel by viewModels()

    private val firebaseAnalytics: FirebaseAnalytics by lazy {
        Firebase.analytics
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        if (!mainViewModel.checkLogin()) {
            findNavController().navigate(R.id.action_pre_login_nav)
        } else if (!mainViewModel.checkUserData()) {
            findNavController().navigate(R.id.action_mainFragment_to_profileFragment)
        }
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        setAppbar()
        setNavBar()
        observeWishlist()
        observeCart()
        observeNotification()
    }

    private fun observeWishlist() {
        val badgeCompact = binding.bottomNavigation?.getOrCreateBadge(R.id.wishlistFragment)
        val badgeMedium = binding.navRail?.getOrCreateBadge(R.id.wishlistFragment)
        viewLifecycleOwner.lifecycleScope.launch {
            repeatOnLifecycle(Lifecycle.State.STARTED) {
                mainViewModel.wishListProductSize().collect { size ->
                    if (size >= 1) {
                        badgeCompact?.isVisible = true
                        badgeCompact?.number = size
                        badgeMedium?.isVisible = true
                        badgeMedium?.number = size
                    } else {
                        badgeCompact?.isVisible = false
                        badgeMedium?.isVisible = false
                    }
                }
            }
        }
    }

    private fun observeCart() {
        val badgeDrawable = BadgeDrawable.create(requireActivity())
        BadgeUtils.attachBadgeDrawable(
            badgeDrawable,
            binding.appBarLayout.toolbar,
            R.id.cart
        )
        viewLifecycleOwner.lifecycleScope.launch {
            repeatOnLifecycle(Lifecycle.State.STARTED) {
                mainViewModel.productCartSize().collect { size ->
                    if (size >= 1) {
                        badgeDrawable.isVisible = true
                        badgeDrawable.number = size
                    } else {
                        badgeDrawable.isVisible = false
                    }
                }
            }
        }
    }

    private fun observeNotification() {
        val badgeDrawable = BadgeDrawable.create(requireActivity())
        BadgeUtils.attachBadgeDrawable(
            badgeDrawable,
            binding.appBarLayout.toolbar,
            R.id.notification
        )
        viewLifecycleOwner.lifecycleScope.launch {
            repeatOnLifecycle(Lifecycle.State.STARTED) {
                mainViewModel.notificationSize().collect { size ->
                    if (size >= 1) {
                        badgeDrawable.isVisible = true
                        badgeDrawable.number = size
                    } else {
                        badgeDrawable.isVisible = false
                    }
                }
            }
        }
    }

    private fun setAppbar() {
        val toolbar = binding.appBarLayout.toolbar
        toolbar.title = mainViewModel.user.userName
        toolbar.navigationIcon = ContextCompat.getDrawable(
            requireActivity(),
            R.drawable.ic_account
        )
        toolbar.inflateMenu(R.menu.top_bar_menu)
        toolbar.setOnMenuItemClickListener {
            when (it.itemId) {
                R.id.notification -> {
                    firebaseAnalytics.sendLogButtonClicked("Main: Notification")
                    findNavController().navigate(R.id.action_mainFragment_to_notificationFragment)
                }

                R.id.cart -> {
                    firebaseAnalytics.sendLogButtonClicked("Main: Cart")
                    findNavController().navigate(R.id.action_mainFragment_to_cartFragment2)
                }

                R.id.menu -> {
                    firebaseAnalytics.sendLogButtonClicked("Main: Menu")
                    binding.root.showSnackbar("Option menu")
                }

                else -> return@setOnMenuItemClickListener false
            }
            return@setOnMenuItemClickListener true
        }
    }

    private fun setNavBar() {
        val navHostFragment =
            childFragmentManager.findFragmentById(R.id.fragmentMain) as NavHostFragment
        val navController = navHostFragment.findNavController()

        with(binding) {
            bottomNavigation?.setupWithNavController(navController)
            navRail?.setupWithNavController(navController)
            navView?.setupWithNavController(navController)

            val arg = MainFragmentArgs.fromBundle(requireArguments()).flag
            Log.d("MainActivity", "$arg")
            if (arg == FLAG_TRANSACTION) {
                binding.apply {
                    bottomNavigation?.selectedItemId = R.id.transactionFragment
                    navRail?.selectedItemId = R.id.transactionFragment
                    navView?.id = R.id.transactionFragment
                }
            }

            val container: ViewGroup = containerMain
            container.addView(object : View(requireActivity()) {
                override fun onConfigurationChanged(newConfig: Configuration?) {
                    super.onConfigurationChanged(newConfig)
                    WindowMetricsCalculator.getOrCreate()
                        .computeCurrentWindowMetrics(requireActivity())
                }
            })
            WindowMetricsCalculator.getOrCreate()
                .computeCurrentWindowMetrics(requireActivity())

            if (mainViewModel.navigateToMainFragment) {
                bottomNavigation?.selectedItemId = R.id.transactionFragment
                navRail?.selectedItemId = R.id.transactionFragment
                navView?.menu?.performIdentifierAction(R.id.transactionFragment, 0)
                mainViewModel.navigateToMainFragment = false
            }
        }
    }

    override fun onDestroyView() {
        super.onDestroyView()
        this.arguments?.clear()
    }

    companion object {
        const val MOVE_TRANSACTION_BUNDLE_KEY = "move_transaction_bundle_key"
        const val FLAG_TRANSACTION = "transaction"
    }
}
