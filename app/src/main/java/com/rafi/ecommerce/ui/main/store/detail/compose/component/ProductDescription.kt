package com.rafi.ecommerce.ui.main.store.detail.compose.component

import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.Spacer
import androidx.compose.foundation.layout.height
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.ui.Modifier
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.text.PlatformTextStyle
import androidx.compose.ui.text.TextStyle
import androidx.compose.ui.text.font.FontWeight
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import com.rafi.core.data.model.ProductDetailsModel
import com.rafi.ecommerce.R
import com.rafi.ecommerce.ui.theme.poppinsFamily

@Composable
fun ProductDescription(product: ProductDetailsModel, modifier: Modifier = Modifier) {
    Column(modifier) {
        Text(
            text = stringResource(id = R.string.product_detail_title),
            fontSize = 16.sp,
            style = TextStyle(
                fontFamily = poppinsFamily,
                fontWeight = FontWeight.Medium,
                platformStyle = PlatformTextStyle(
                    includeFontPadding = false,
                ),
            ),
        )
        Spacer(modifier = Modifier.height(8.dp))
        Text(
            text = product.description,
            style = TextStyle(
                fontFamily = poppinsFamily,
                fontWeight = FontWeight.Normal,
                platformStyle = PlatformTextStyle(
                    includeFontPadding = false,
                ),
            ),
        )
    }
}

@Preview(showBackground = true)
@Composable
fun ProductDescriptionPreview() {
    val product = ProductDetailsModel(
        productId = "your_product_id",
        productName = "ASUS ROG Strix G17 G713RM-R736H6G-O - Eclipse Gray",
        productPrice = 24499000,
        image = listOf(
            "https://example.com/image1.jpg",
            "https://example.com/image2.jpg",
            "https://example.com/image3.jpg"
        ),
        brand = "Asus",
        description = "Your product description goes here. This is a placeholder for the product description. You can replace it with the actual product description.",
        store = "AsusStore",
        sale = 12,
        stock = 2,
        totalRating = 7,
        totalReview = 5,
        totalSatisfaction = 100,
        productRating = 5.0F,
        productVariant = listOf(
            ProductDetailsModel.ProductVariant(
                variantName = "Variant 1",
                variantPrice = 0
            ),
            ProductDetailsModel.ProductVariant(
                variantName = "Variant 2",
                variantPrice = 10000
            )
        )
    )

    ProductDescription(product = product)
}
