package com.rafi.ecommerce.ui.profile

import android.app.AlertDialog
import android.net.Uri
import android.os.Bundle
import android.view.View
import androidx.activity.result.PickVisualMediaRequest
import androidx.activity.result.contract.ActivityResultContracts
import androidx.core.view.isInvisible
import androidx.core.widget.doOnTextChanged
import androidx.fragment.app.viewModels
import androidx.lifecycle.Lifecycle
import androidx.lifecycle.lifecycleScope
import androidx.lifecycle.repeatOnLifecycle
import androidx.navigation.fragment.findNavController
import com.bumptech.glide.Glide
import com.rafi.core.util.DataMapper.toMultipartBody
import com.rafi.core.util.Resources
import com.rafi.ecommerce.R
import com.rafi.ecommerce.databinding.FragmentProfileBinding
import com.rafi.ecommerce.util.BaseFragment
import com.rafi.ecommerce.util.Helper
import com.rafi.ecommerce.util.MediaUtils
import com.rafi.ecommerce.util.ThrowableExt.getErrorMessage
import com.rafi.ecommerce.util.showSnackbar
import dagger.hilt.android.AndroidEntryPoint
import kotlinx.coroutines.launch
import okhttp3.RequestBody.Companion.toRequestBody

@AndroidEntryPoint
class ProfileFragment :
    BaseFragment<FragmentProfileBinding>(FragmentProfileBinding::inflate) {

    private val profileViewModel: ProfileViewModel by viewModels()

    private lateinit var uri: Uri

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        initAppBar()
        observeViewModel()
        initAction()
        validatedData()
        spannableText()
    }

    private fun initAppBar() {
        val toolbar = binding.appBarLayout.toolbar
        toolbar.title = resources.getString(R.string.profile)
        toolbar.isTitleCentered = true
    }

    private fun observeViewModel() {
        viewLifecycleOwner.lifecycleScope.launch {
            repeatOnLifecycle(Lifecycle.State.STARTED) {
                profileViewModel.state.collect { value ->
                    if (value != null) {
                        handleProfileViewState(value)
                    }
                }
            }
        }
    }

    private fun handleProfileViewState(value: Resources<Boolean>) {
        when (value) {
            is Resources.Loading -> showLoading(true)
            is Resources.Success -> {
                showLoading(false)
                if (value.data) {
                    navigateToHome()
                }
            }

            is Resources.Error -> {
                showLoading(false)
                val errorMessage = value.message.getErrorMessage()
                binding.root.showSnackbar(errorMessage)
            }
        }
    }

    private fun navigateToHome() {
        findNavController().navigate(R.id.action_profileFragment_to_mainFragment)
    }

    private fun postProfile() {
        val imageFile = profileViewModel.currentImageUri?.let { uri ->
            MediaUtils.uriToFile(uri, requireActivity())
        }
        val name = binding.nameInputText.text.toString()

        val nameRequestBody = name.toRequestBody()
        val imageMultipartBody = imageFile?.toMultipartBody("userImage")
        profileViewModel.uploadProfile(nameRequestBody, imageMultipartBody)
    }

    private fun initAction() {
        binding.apply {
            ivBlank.setOnClickListener {
                val options = arrayOf(getString(R.string.camera), getString(R.string.gallery))
                val builder = AlertDialog.Builder(context)
                builder.setTitle(getString(R.string.choose_img))
                    .setItems(options) { _, which ->
                        when (which) {
                            0 -> {
                                startCamera()
                            }

                            1 -> {
                                startGallery()
                            }
                        }
                    }
                val dialog = builder.create()
                dialog.show()
            }

            btnDone.setOnClickListener {
                postProfile()
            }
        }
    }

    private fun validatedData() {
        binding.apply {
            btnDone.isEnabled = false

            nameContainer.editText?.doOnTextChanged { text, _, _, _ ->
                if (text.toString() == "") {
                    btnDone.isEnabled = false
                    nameContainer.error = getString(R.string.please_input_your_name)
                } else {
                    nameContainer.error = null
                    btnDone.isEnabled = true
                }
            }
        }
    }

    private fun startCamera() {
        uri = MediaUtils.buildNewUri(requireActivity())
        launcherIntentCamera.launch(uri)
    }

    private val launcherIntentCamera = registerForActivityResult(
        ActivityResultContracts.TakePicture()
    ) { isSuccess ->
        if (isSuccess) {
            profileViewModel.currentImageUri = uri
            showImage()
        }
    }

    private fun startGallery() {
        launcherGallery.launch(
            PickVisualMediaRequest(
                ActivityResultContracts.PickVisualMedia.ImageOnly
            )
        )
    }

    private val launcherGallery = registerForActivityResult(
        ActivityResultContracts.PickVisualMedia()
    ) { uri ->
        if (uri != null) {
            profileViewModel.currentImageUri = uri
            showImage()
        }
    }

    private fun showImage() {
        Glide.with(this)
            .load(profileViewModel.currentImageUri)
            .circleCrop()
            .into(binding.ivBlank)
    }

    private fun showLoading(isLoading: Boolean) {
        binding.apply {
            profileProgressBar.isInvisible = !isLoading
            btnDone.isInvisible = isLoading
        }
    }

    private fun spannableText() {
        binding.tvTerms.text = Helper.getTncPrivacyPolicySpanText(requireActivity())
    }
}
