package com.rafi.ecommerce.ui.main.wishlist

import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.rafi.core.data.model.WishListProductsModel
import com.rafi.core.data.repository.ProductRepository
import com.rafi.core.util.DataMapper.toCart
import com.rafi.core.util.Resources
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.StateFlow
import kotlinx.coroutines.flow.asStateFlow
import kotlinx.coroutines.launch
import kotlinx.coroutines.runBlocking
import javax.inject.Inject

@HiltViewModel
class WishlistViewModel @Inject constructor(
    private val productRepository: ProductRepository
) : ViewModel() {

    private val _state = MutableStateFlow<Resources<Boolean>?>(null)
    val state: StateFlow<Resources<Boolean>?> = _state.asStateFlow()

    var recyclerViewType = WishlistAdapter.LINEAR_TYPE

    var totalPrice = 0

    fun wishListProduct(): Flow<List<WishListProductsModel>> =
        productRepository.getWishListProducts()

    fun deleteItemFromWishList(wishList: WishListProductsModel) {
        viewModelScope.launch {
            productRepository.deleteWishList(wishList)
        }
    }

    fun addToCart(wishList: WishListProductsModel): Boolean {
        val cart = wishList.toCart()
        val isReadyStock = runBlocking { productRepository.isReadyStock(cart) }
        return if (isReadyStock) {
            viewModelScope.launch {
                productRepository.insertCart(cart)
            }
            true
        } else {
            false
        }
    }
}
