package com.rafi.ecommerce.ui.main.transaction

import android.os.Bundle
import android.view.View
import androidx.core.view.isVisible
import androidx.fragment.app.viewModels
import androidx.lifecycle.Lifecycle
import androidx.lifecycle.lifecycleScope
import androidx.lifecycle.repeatOnLifecycle
import androidx.navigation.Navigation
import androidx.recyclerview.widget.LinearLayoutManager
import com.google.firebase.analytics.FirebaseAnalytics
import com.google.firebase.analytics.ktx.analytics
import com.google.firebase.ktx.Firebase
import com.rafi.core.util.DataMapper.toFulfillment
import com.rafi.core.util.Resources
import com.rafi.ecommerce.R
import com.rafi.ecommerce.databinding.FragmentTransactionBinding
import com.rafi.ecommerce.ui.main.fulfillment.rating.RatingFragment
import com.rafi.ecommerce.util.BaseFragment
import com.rafi.ecommerce.util.Helper.sendLogButtonClicked
import com.rafi.ecommerce.util.ThrowableExt.getErrorSubtitle
import com.rafi.ecommerce.util.ThrowableExt.getErrorTitle
import dagger.hilt.android.AndroidEntryPoint
import kotlinx.coroutines.launch
import okhttp3.ResponseBody.Companion.toResponseBody
import retrofit2.HttpException
import retrofit2.Response
import java.net.HttpURLConnection

@AndroidEntryPoint
class TransactionFragment :
    BaseFragment<FragmentTransactionBinding>(FragmentTransactionBinding::inflate) {

    private val viewModel: TransactionViewModel by viewModels()

    private val firebaseAnalytics: FirebaseAnalytics by lazy {
        Firebase.analytics
    }

    private val transactionAdapter: TransactionAdapter by lazy {
        TransactionAdapter { data ->
            firebaseAnalytics.sendLogButtonClicked("Transaction: Review")
            val bundle = Bundle().apply {
                putParcelable(RatingFragment.DATA_BUNDLE_KEY, data.toFulfillment())
                putInt(RatingFragment.RATING_BUNDLE_KEY, data.rating)
                putString(RatingFragment.REVIEW_BUNDLE_KEY, data.review)
            }
            Navigation.findNavController(requireActivity(), R.id.navHostFragment).navigate(
                R.id.action_mainFragment_to_ratingFragment,
                bundle
            )
        }
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        setupRecyclerView()
        observeTransaction()
    }

    private fun observeTransaction() {
        viewLifecycleOwner.lifecycleScope.launch {
            repeatOnLifecycle(Lifecycle.State.STARTED) {
                viewModel.state.collect { result ->
                    if (result != null) {
                        showLoading(result is Resources.Loading)
                        when (result) {
                            is Resources.Loading -> {}
                            is Resources.Success -> {
                                if (result.data.isEmpty()) {
                                    showErrorView(
                                        HttpException(
                                            Response.error<String>(
                                                HttpURLConnection.HTTP_NOT_FOUND,
                                                "".toResponseBody()
                                            )
                                        )
                                    )
                                } else {
                                    val sortedData = result.data.sortedByDescending { it.time }
                                    transactionAdapter.submitList(sortedData)
                                }
                            }
                            is Resources.Error -> showErrorView(result.message)
                        }
                    }
                }
            }
        }
    }

    private fun showErrorView(error: Throwable) {
        val title = error.getErrorTitle(requireActivity())
        val subtitle = error.getErrorSubtitle(requireActivity())
        val action = getString(R.string.refresh_btn)

        binding.rvTransaction.isVisible = false
        with(binding.includeErrorMessage) {
            tvErrorTitle.text = title
            tvErrorDesc.text = subtitle
            btnError.text = action

            btnError.setOnClickListener { viewModel.getTransactionList() }
            containerEmptyData.isVisible = true
        }
    }

    private fun showLoading(isLoading: Boolean) {
        binding.pbTransaction.isVisible = isLoading
        binding.rvTransaction.isVisible = !isLoading
        binding.includeErrorMessage.containerEmptyData.isVisible = false
    }

    private fun setupRecyclerView() {
        binding.rvTransaction.apply {
            adapter = transactionAdapter
            layoutManager = LinearLayoutManager(requireActivity())
        }
    }
}
