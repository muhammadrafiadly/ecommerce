package com.rafi.ecommerce.ui.main.notification

import android.os.Bundle
import android.view.View
import androidx.core.view.isVisible
import androidx.fragment.app.viewModels
import androidx.lifecycle.Lifecycle
import androidx.lifecycle.lifecycleScope
import androidx.lifecycle.repeatOnLifecycle
import androidx.navigation.fragment.findNavController
import androidx.recyclerview.widget.LinearLayoutManager
import com.rafi.ecommerce.R
import com.rafi.ecommerce.databinding.FragmentNotificationBinding
import com.rafi.ecommerce.util.BaseFragment
import dagger.hilt.android.AndroidEntryPoint
import kotlinx.coroutines.launch

@AndroidEntryPoint
class NotificationFragment :
    BaseFragment<FragmentNotificationBinding>(FragmentNotificationBinding::inflate) {

    private val notificationViewModel: NotificationViewModel by viewModels()

    private val notificationAdapter: NotificationAdapter by lazy {
        NotificationAdapter { data ->
            notificationViewModel.readNotification(data)
        }
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        initAppBar()
        initObserver()
        initRecyclerView()
    }

    private fun initAppBar() {
        val toolbar = binding.appBarLayout.toolbar
        toolbar.title = getString(R.string.notification)
        toolbar.setNavigationIcon(R.drawable.ic_arrow_back)
        toolbar.setNavigationOnClickListener {
            findNavController().navigateUp()
        }
    }

    private fun initObserver() {
        viewLifecycleOwner.lifecycleScope.launch {
            repeatOnLifecycle(Lifecycle.State.STARTED) {
                notificationViewModel.getNotificationData().collect {
                    initMessageDataEmpty(it.isEmpty())
                    notificationAdapter.submitList(it)
                }
            }
        }
    }

    private fun initRecyclerView() {
        binding.rvNotification.apply {
            adapter = notificationAdapter
            layoutManager = LinearLayoutManager(requireActivity())
            itemAnimator = null
        }
    }

    private fun initMessageDataEmpty(isEmpty: Boolean) {
        binding.rvNotification.isVisible = !isEmpty
        with(binding.includeEmptyData) {
            btnError.isVisible = false
            tvErrorTitle.text = getString(R.string.empty_title)
            tvErrorDesc.text = getString(R.string.empty_data_desc)
            containerEmptyData.isVisible = isEmpty
        }
    }
}
