package com.rafi.ecommerce.ui.main.transaction

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.core.view.isVisible
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.ListAdapter
import androidx.recyclerview.widget.RecyclerView
import coil.load
import com.rafi.core.data.model.TransactionModel
import com.rafi.ecommerce.R
import com.rafi.ecommerce.databinding.ItemTransactionListBinding
import com.rafi.ecommerce.util.toCurrencyFormat

class TransactionAdapter(
    private val itemCallback: (TransactionModel) -> Unit
) : ListAdapter<TransactionModel, TransactionAdapter.TransactionViewHolder>(TransactionComparator) {
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): TransactionViewHolder {
        val binding = ItemTransactionListBinding.inflate(
            LayoutInflater.from(parent.context),
            parent,
            false
        )
        return TransactionViewHolder(binding, itemCallback)
    }

    override fun onBindViewHolder(holder: TransactionViewHolder, position: Int) {
        holder.bind(getItem(position))
    }

    class TransactionViewHolder(
        private val binding: ItemTransactionListBinding,
        private val itemCallback: (TransactionModel) -> Unit
    ) : RecyclerView.ViewHolder(binding.root) {
        fun bind(data: TransactionModel) {
            with(binding) {
                ivProductImage.load(data.image) {
                    crossfade(true)
                }
                tvProductName.text = data.name
                tvTransactionDate.text = data.date
                tvTotalItem.text =
                    itemView.context.getString(R.string.total_item, data.items.size)
                tvTotalShopping.text = data.total.toCurrencyFormat()
                if (data.rating == 0 || data.review.isEmpty()) {
                    btnReviewDone.setOnClickListener { itemCallback(data) }
                } else {
                    btnReviewDone.isVisible = false
                }
            }
        }
    }

    object TransactionComparator : DiffUtil.ItemCallback<TransactionModel>() {
        override fun areItemsTheSame(
            oldItem: TransactionModel,
            newItem: TransactionModel
        ): Boolean =
            oldItem.invoiceId == newItem.invoiceId

        override fun areContentsTheSame(
            oldItem: TransactionModel,
            newItem: TransactionModel
        ): Boolean =
            oldItem == newItem
    }
}
