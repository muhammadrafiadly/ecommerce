package com.rafi.ecommerce.ui.main.fulfillment.payment

import android.os.Bundle
import android.util.Log
import android.view.View
import androidx.fragment.app.setFragmentResult
import androidx.navigation.fragment.findNavController
import androidx.recyclerview.widget.LinearLayoutManager
import com.google.android.material.divider.MaterialDividerItemDecoration
import com.google.firebase.analytics.FirebaseAnalytics
import com.google.firebase.analytics.ktx.analytics
import com.google.firebase.analytics.logEvent
import com.google.firebase.ktx.Firebase
import com.google.firebase.remoteconfig.ConfigUpdate
import com.google.firebase.remoteconfig.ConfigUpdateListener
import com.google.firebase.remoteconfig.FirebaseRemoteConfig
import com.google.firebase.remoteconfig.FirebaseRemoteConfigException
import com.google.gson.Gson
import com.rafi.core.data.source.remote.response.PaymentResponse
import com.rafi.core.util.DataMapper.toPayment
import com.rafi.ecommerce.R
import com.rafi.ecommerce.databinding.FragmentPaymentBinding
import com.rafi.ecommerce.util.BaseFragment
import dagger.hilt.android.AndroidEntryPoint
import javax.inject.Inject

@AndroidEntryPoint
class PaymentFragment :
    BaseFragment<FragmentPaymentBinding>(FragmentPaymentBinding::inflate) {

    @Inject
    lateinit var remoteConfig: FirebaseRemoteConfig

    private val paymentAdapter: PaymentAdapter by lazy {
        PaymentAdapter { paymentItem ->
            firebaseAnalytics.logEvent(FirebaseAnalytics.Event.ADD_PAYMENT_INFO) {
                param(FirebaseAnalytics.Param.PAYMENT_TYPE, paymentItem.label)
            }
            val bundle = Bundle().apply {
                putParcelable(BUNDLE_PAYMENT_KEY, paymentItem)
            }
            setFragmentResult(REQUEST_KEY, bundle)
            findNavController().navigateUp()
        }
    }

    private val firebaseAnalytics: FirebaseAnalytics by lazy {
        Firebase.analytics
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        initAppBar()
        initRecyclerView()
        initPayment()
    }

    private fun initPayment() {
        val paymentMethod = remoteConfig.getString("payment")
        showPayment(paymentMethod)

        remoteConfig.addOnConfigUpdateListener(object : ConfigUpdateListener {
            override fun onUpdate(configUpdate: ConfigUpdate) {
                if (configUpdate.updatedKeys.contains("payment")) {
                    remoteConfig.activate().addOnCompleteListener {
                        val paymentMethod = remoteConfig.getString("payment")
                        showPayment(paymentMethod)
                    }
                }
            }

            override fun onError(error: FirebaseRemoteConfigException) {
                // TODO
            }
        })
    }

    private fun showPayment(payment: String) {
        val paymentList = Gson().fromJson(payment, PaymentResponse::class.java)
        Log.d("LOG", "$paymentList")
        val checkPayment = paymentList.data?.flatMap {
            it.toPayment()
        }
        paymentAdapter.submitList(checkPayment)
    }

    private fun initAppBar() {
        val toolbar = binding.appBarLayout.toolbar
        toolbar.title = getString(R.string.choose_payment)
        toolbar.setNavigationIcon(R.drawable.ic_arrow_back)
        toolbar.setNavigationOnClickListener {
            findNavController().navigateUp()
        }
    }

    private fun initRecyclerView() {
        val divider = MaterialDividerItemDecoration(
            requireActivity(),
            LinearLayoutManager.VERTICAL
        )
        divider.isLastItemDecorated = false
        divider.dividerThickness = 4

        binding.rvPaymentType.apply {
            adapter = paymentAdapter
            layoutManager = LinearLayoutManager(requireActivity())
            addItemDecoration(divider)
        }
    }

    companion object {
        const val REQUEST_KEY = "payment_request_key"
        const val BUNDLE_PAYMENT_KEY = "payment_bundle_key"
    }
}
