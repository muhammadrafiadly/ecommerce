package com.rafi.ecommerce.ui.main.fulfillment.checkout

import android.annotation.SuppressLint
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.core.content.ContextCompat
import androidx.recyclerview.widget.ListAdapter
import androidx.recyclerview.widget.RecyclerView
import coil.load
import com.rafi.core.data.model.ProductCartModel
import com.rafi.ecommerce.R
import com.rafi.ecommerce.databinding.ItemProductCheckoutBinding
import com.rafi.ecommerce.ui.main.cart.CartAdapter
import com.rafi.ecommerce.util.toCurrencyFormat

class CheckoutAdapter(
    private val callback: CartAdapter.CartCallback
) : ListAdapter<ProductCartModel, CheckoutAdapter.CheckoutViewHolder>(CartAdapter.CartComparator) {

    class CheckoutViewHolder(
        private val binding: ItemProductCheckoutBinding,
        private val callback: CartAdapter.CartCallback
    ) : RecyclerView.ViewHolder(binding.root) {
        @SuppressLint("StringFormatMatches")
        fun bind(data: ProductCartModel) {
            with(binding) {
                ivCartImage.load(data.image) {
                    crossfade(true)
                    placeholder(R.drawable.product_image_placeholder)
                    error(R.drawable.product_image_placeholder)
                }
                tvCartName.text = data.productName
                tvCartVariant.text = data.variantName
                tvCartStock.text = if (data.stock <= 10) {
                    tvCartStock.setTextColor(ContextCompat.getColor(itemView.context, R.color.red))
                    itemView.context.getString(R.string.stock, data.stock)
                } else {
                    itemView.context.getString(R.string.stock, data.stock)
                }
                tvCartPrice.text = data.productPrice.plus(data.variantPrice).toCurrencyFormat()
                tvCartQuantity.text = data.quantity.toString()

                btnMinusCart.setOnClickListener {
                    callback.decreaseQuantityCallback(data)
                }
                btnPlusCart.setOnClickListener {
                    callback.increaseQuantityCallback(data)
                }
            }
        }
    }

    override fun onCreateViewHolder(
        parent: ViewGroup,
        viewType: Int
    ): CheckoutViewHolder {
        val binding = ItemProductCheckoutBinding
            .inflate(LayoutInflater.from(parent.context), parent, false)
        return CheckoutViewHolder(binding, callback)
    }

    override fun onBindViewHolder(holder: CheckoutViewHolder, position: Int) {
        holder.bind(getItem(position))
    }
}
