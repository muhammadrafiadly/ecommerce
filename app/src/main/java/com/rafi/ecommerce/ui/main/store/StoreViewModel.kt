package com.rafi.ecommerce.ui.main.store

import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import androidx.paging.PagingData
import androidx.paging.cachedIn
import com.rafi.core.data.model.ProductModel
import com.rafi.core.data.model.ProductQueryModel
import com.rafi.core.data.repository.ProductRepository
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.flatMapLatest
import kotlinx.coroutines.flow.update
import kotlinx.coroutines.launch
import javax.inject.Inject

@HiltViewModel
class StoreViewModel @Inject constructor(
    private val productRepository: ProductRepository,
) : ViewModel() {

    var viewType = ProductAdapter.LINEAR_TYPE

    var sortFilterProduct: Int? = null
    var brandFilterProduct: Int? = null

    var productsQuery: MutableStateFlow<ProductQueryModel> =
        MutableStateFlow(ProductQueryModel())

    val productsData: Flow<PagingData<ProductModel>> = productsQuery.flatMapLatest { query ->
        productRepository.getProducts(query)
    }.cachedIn(viewModelScope)

    fun getProductFilter(productQueryModel: ProductQueryModel) {
        viewModelScope.launch {
            productsQuery.update {
                productQueryModel.copy(search = it.search)
            }
        }
    }

    fun getProductSearch(query: String?) {
        viewModelScope.launch {
            productsQuery.update {
                it.copy(search = query)
            }
        }
    }

    fun resetProduct() {
        viewModelScope.launch {
            productsQuery.value = ProductQueryModel()
            sortFilterProduct = null
            brandFilterProduct = null
        }
    }
}
