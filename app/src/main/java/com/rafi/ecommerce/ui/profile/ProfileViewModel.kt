package com.rafi.ecommerce.ui.profile

import android.net.Uri
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.rafi.core.data.repository.UserRepository
import com.rafi.core.util.Resources
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.StateFlow
import kotlinx.coroutines.flow.asStateFlow
import kotlinx.coroutines.launch
import okhttp3.MultipartBody
import okhttp3.RequestBody
import javax.inject.Inject

@HiltViewModel
class ProfileViewModel @Inject constructor(
    private val userRepository: UserRepository
) : ViewModel() {

    private val _state = MutableStateFlow<Resources<Boolean>?>(null)
    val state: StateFlow<Resources<Boolean>?> = _state.asStateFlow()

    var currentImageUri: Uri? = null

    fun uploadProfile(userName: RequestBody, userImage: MultipartBody.Part?) {
        viewModelScope.launch {
            userRepository.uploadProfile(userName, userImage).collect { value ->
                _state.value = value
            }
        }
    }
}
