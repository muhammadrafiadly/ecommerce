package com.rafi.ecommerce.ui.main.wishlist

import android.view.LayoutInflater
import android.view.ViewGroup
import android.view.animation.AnimationUtils
import android.widget.Button
import android.widget.ImageView
import android.widget.TextView
import androidx.core.view.isVisible
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.ListAdapter
import androidx.recyclerview.widget.RecyclerView
import androidx.viewbinding.ViewBinding
import coil.load
import com.rafi.core.data.model.WishListProductsModel
import com.rafi.ecommerce.R
import com.rafi.ecommerce.databinding.ItemWishlistGridBinding
import com.rafi.ecommerce.databinding.ItemWishlistLinearBinding
import com.rafi.ecommerce.ui.main.store.ProductAdapter
import com.rafi.ecommerce.util.toCurrencyFormat

class WishlistAdapter(
    private val callback: WishlistCallback
) : ListAdapter<
    WishListProductsModel,
    WishlistAdapter.WishlistViewHolder<ViewBinding>
    >(WishlistComparator) {

    var viewType = LINEAR_TYPE

    override fun getItemViewType(position: Int): Int {
        return viewType
    }

    override fun onCreateViewHolder(
        parent: ViewGroup,
        viewType: Int
    ): WishlistViewHolder<ViewBinding> {
        if (viewType == LINEAR_TYPE) {
            val binding =
                ItemWishlistLinearBinding.inflate(
                    LayoutInflater.from(parent.context),
                    parent,
                    false
                )
            return WishlistViewHolder(binding, viewType, callback)
        } else {
            val binding =
                ItemWishlistGridBinding.inflate(
                    LayoutInflater.from(parent.context),
                    parent,
                    false
                )
            return WishlistViewHolder(binding, viewType, callback)
        }
    }

    override fun onBindViewHolder(holder: WishlistViewHolder<ViewBinding>, position: Int) {
        holder.bind(getItem(position))
    }

    class WishlistViewHolder<T : ViewBinding>(
        private val binding: T,
        private val viewType: Int,
        private val callback: WishlistCallback
    ) :
        RecyclerView.ViewHolder(binding.root) {
        fun bind(data: WishListProductsModel) {
            if (viewType == LINEAR_TYPE) {
                with(binding as ItemWishlistLinearBinding) {
                    bindView(
                        WishlistItemViewData(
                            data,
                            ivProduct,
                            tvProductName,
                            tvProductPrice,
                            tvStore,
                            tvRatingSales,
                            btnDeleteWishlist,
                            btnAddCart
                        )
                    )
                }
            } else {
                with(binding as ItemWishlistGridBinding) {
                    bindView(
                        WishlistItemViewData(
                            data,
                            ivProduct,
                            tvProductName,
                            tvProductPrice,
                            tvStore,
                            tvRatingSales,
                            btnDeleteWishlist,
                            btnAddCart
                        )
                    )
                }
            }
            itemView.setOnClickListener {
                callback.itemClickCallback(data.productId)
            }
        }

        private fun bindView(viewData: WishlistItemViewData) {
            with(viewData) {
                productImage.load(data.image) {
                    crossfade(true)
                    placeholder(R.drawable.product_image_placeholder)
                    error(R.drawable.product_image_placeholder)
                }
                productName.text = data.productName
                productPrice.text = data.productPrice.plus(data.variantPrice).toCurrencyFormat()
                productStore.text = data.store
                productRatingAndSales.text = itemView.resources.getString(
                    R.string.product_rating_and_sold,
                    data.productRating,
                    data.sale
                )
                buttonDelete.isVisible = true
                buttonDelete.setOnClickListener { callback.deleteCallback(data) }
                buttonAddCart.isVisible = true
                buttonAddCart.setOnClickListener { callback.addToCartCallback(data) }
            }
        }
    }

    data class WishlistItemViewData(
        val data: WishListProductsModel,
        val productImage: ImageView,
        val productName: TextView,
        val productPrice: TextView,
        val productStore: TextView,
        val productRatingAndSales: TextView,
        val buttonDelete: Button,
        val buttonAddCart: Button
    )

    interface WishlistCallback {
        fun itemClickCallback(productId: String)
        fun addToCartCallback(wishlist: WishListProductsModel)
        fun deleteCallback(wishlist: WishListProductsModel)
    }

    object WishlistComparator : DiffUtil.ItemCallback<WishListProductsModel>() {
        override fun areItemsTheSame(
            oldItem: WishListProductsModel,
            newItem: WishListProductsModel
        ): Boolean =
            oldItem.productId == newItem.productId

        override fun areContentsTheSame(
            oldItem: WishListProductsModel,
            newItem: WishListProductsModel
        ): Boolean =
            oldItem == newItem
    }

    companion object {
        const val LINEAR_TYPE = 1
        const val GRID_TYPE = 2
    }
}
