package com.rafi.ecommerce.ui.main.store.search

import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.inputmethod.EditorInfo
import androidx.core.os.bundleOf
import androidx.core.view.isVisible
import androidx.core.widget.doOnTextChanged
import androidx.fragment.app.DialogFragment
import androidx.fragment.app.setFragmentResult
import androidx.fragment.app.viewModels
import androidx.lifecycle.Lifecycle
import androidx.lifecycle.lifecycleScope
import androidx.lifecycle.repeatOnLifecycle
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.google.firebase.analytics.FirebaseAnalytics
import com.google.firebase.analytics.ktx.analytics
import com.google.firebase.analytics.logEvent
import com.google.firebase.ktx.Firebase
import com.rafi.core.util.Resources
import com.rafi.ecommerce.R
import com.rafi.ecommerce.databinding.FragmentSearchBinding
import dagger.hilt.android.AndroidEntryPoint
import kotlinx.coroutines.launch

@AndroidEntryPoint
class SearchFragment : DialogFragment() {

    private val searchViewModel: SearchViewModel by viewModels()
    private var _binding: FragmentSearchBinding? = null
    private val binding get() = _binding!!
    private lateinit var query: String
    private lateinit var firebaseAnalytics: FirebaseAnalytics

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        arguments?.let {
            query = it.getString(ARG_QUERY) ?: ""
        }

        firebaseAnalytics = Firebase.analytics
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        _binding = FragmentSearchBinding.inflate(inflater, container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        initEditText()
        initRecyclerView()
        observeSearch()
    }

    private fun initEditText() {
        with(binding.etSearch) {
            this.setText(query)
            this.requestFocus()
            this.isFocusableInTouchMode = true

            this.doOnTextChanged { text, _, _, _ ->
                showLoading(true)
                searchViewModel.getSearchData(text.toString())
            }

            this.setOnEditorActionListener { _, actionId, _ ->
                if (actionId == EditorInfo.IME_ACTION_SEARCH) {
                    sendSearchResult()
                }
                true
            }
        }
    }

    private val searchAdapter: SearchAdapter by lazy {
        SearchAdapter { keyword ->
            Log.d("Search Keyword", ": $$keyword")
            sendSearchResult(keyword)
        }
    }

    private fun sendSearchResult(data: String = binding.etSearch.text.toString()) {
        firebaseAnalytics.logEvent(FirebaseAnalytics.Event.VIEW_SEARCH_RESULTS) {
            param(FirebaseAnalytics.Param.SEARCH_TERM, data)
        }

        val bundle = bundleOf(
            BUNDLE_QUERY_KEY to data.ifEmpty { null }
        )
        setFragmentResult(FRAGMENT_REQUEST_KEY, bundle)
        dismiss()
    }

    override fun getTheme(): Int {
        return R.style.SearchDialogTheme
    }

    private fun initRecyclerView() {
        binding.apply {
            rvSearch.adapter = searchAdapter
            rvSearch.layoutManager = LinearLayoutManager(requireActivity())
        }
        searchAdapter.registerAdapterDataObserver(object : RecyclerView
            .AdapterDataObserver() {
            override fun onItemRangeInserted(positionStart: Int, itemCount: Int) {
                super.onItemRangeInserted(positionStart, itemCount)
                binding.rvSearch.scrollToPosition(0)
            }
        })
    }

    private fun observeSearch() {
        viewLifecycleOwner.lifecycleScope.launch {
            repeatOnLifecycle(Lifecycle.State.STARTED) {
                searchViewModel.searchData.collect { result ->
                    showLoading(result is Resources.Loading)
                    when (result) {
                        is Resources.Loading -> {}
                        is Resources.Success -> searchAdapter.submitList(result.data)
                        is Resources.Error -> {}
                    }
                }
            }
        }
    }

    private fun showLoading(isLoading: Boolean) {
        binding.pbSearch.isVisible = isLoading
    }

    companion object {
        private const val ARG_QUERY = "arg_query"
        const val TAG = "SearchProductFragment"
        const val FRAGMENT_REQUEST_KEY = "search_fragment_request_key"
        const val BUNDLE_QUERY_KEY = "query"

        @JvmStatic
        fun newInstance(query: String?) =
            SearchFragment().apply {
                arguments = Bundle().apply {
                    putString(ARG_QUERY, query)
                }
            }
    }
}
