package com.rafi.ecommerce.ui.main.home

import android.os.Bundle
import android.view.View
import androidx.fragment.app.viewModels
import androidx.lifecycle.lifecycleScope
import com.google.firebase.analytics.FirebaseAnalytics
import com.google.firebase.analytics.ktx.analytics
import com.google.firebase.ktx.Firebase
import com.rafi.ecommerce.databinding.FragmentHomeBinding
import com.rafi.ecommerce.util.BaseFragment
import com.rafi.ecommerce.util.Helper
import com.rafi.ecommerce.util.Helper.sendLogButtonClicked
import dagger.hilt.android.AndroidEntryPoint
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.flow.first
import kotlinx.coroutines.launch
import kotlinx.coroutines.runBlocking

@AndroidEntryPoint
class HomeFragment : BaseFragment<FragmentHomeBinding>(FragmentHomeBinding::inflate) {

    private val homeViewModel: HomeViewModel by viewModels()

    private val firebaseAnalytics: FirebaseAnalytics by lazy {
        Firebase.analytics
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        initSwitch()
        initAction()
    }

    private fun initAction() {
        binding.apply {
            btnLogout.setOnClickListener {
                homeViewModel.postLogout()
                clearDataRoom()
            }
            switchTheme.setOnCheckedChangeListener { _, isDarkTheme ->
                firebaseAnalytics.sendLogButtonClicked("Home: Theme")
                homeViewModel.setAppTheme(isDarkTheme)
                Helper.setAppTheme(requireActivity(), isDarkTheme)
            }
            switchLocalization.setOnCheckedChangeListener { _, isChecked ->
                firebaseAnalytics.sendLogButtonClicked("Home: Language")
                homeViewModel.setAppLanguage(isChecked)
                val appLanguage = if (isChecked) "id" else "en"
                Helper.setAppLanguage(requireActivity(), appLanguage)
                requireActivity().recreate()
            }
        }
    }

    private fun initSwitch() {
        binding.apply {
            switchTheme.isChecked = runBlocking {
                homeViewModel.getAppTheme().first()
            }
            switchLocalization.isChecked = runBlocking {
                homeViewModel.getAppLanguage().first() == "id"
            }
        }
    }

    private fun clearDataRoom() {
        viewLifecycleOwner.lifecycleScope.launch(Dispatchers.IO) {
            homeViewModel.clearDb()
        }
    }
}
