package com.rafi.ecommerce.ui.prelogin.onboarding

import android.Manifest
import android.content.pm.PackageManager
import android.os.Build
import android.os.Bundle
import android.view.View
import androidx.activity.result.contract.ActivityResultContracts
import androidx.core.content.ContextCompat
import androidx.core.view.isVisible
import androidx.fragment.app.viewModels
import androidx.navigation.fragment.findNavController
import androidx.viewpager2.widget.ViewPager2
import com.google.android.material.tabs.TabLayoutMediator
import com.rafi.ecommerce.R
import com.rafi.ecommerce.databinding.FragmentOnboardingBinding
import com.rafi.ecommerce.util.BaseFragment
import dagger.hilt.android.AndroidEntryPoint

@AndroidEntryPoint
class OnboardingFragment :
    BaseFragment<FragmentOnboardingBinding>(FragmentOnboardingBinding::inflate) {

    private lateinit var viewPager: ViewPager2
    private val onboardingViewModel: OnboardingViewModel by viewModels()
    private val adapter = OnboardingAdapter()

    private val images = listOf(
        R.drawable.swipe_1_4x,
        R.drawable.swipe_2_4x,
        R.drawable.swipe_3_4x
    )

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        initAction()
        initViewPager()
        checkNotificationPermission()
    }

    private fun checkNotificationPermission() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.TIRAMISU) {
            val notificationPermission = Manifest.permission.POST_NOTIFICATIONS
            if (!isPermissionsGranted(notificationPermission)) {
                requestPermissionLauncher.launch(notificationPermission)
            }
        }
    }

    private fun isPermissionsGranted(permission: String) =
        ContextCompat.checkSelfPermission(
            requireActivity(),
            permission
        ) == PackageManager.PERMISSION_GRANTED

    private val requestPermissionLauncher =
        registerForActivityResult(
            ActivityResultContracts.RequestPermission()
        ) { }

    private fun initAction() {
        binding.apply {
            btnJoin.setOnClickListener {
                onboardingViewModel.setOnboard(false)
                findNavController().navigate(R.id.action_onboardingFragment_to_registerFragment)
            }

            btnSkip.setOnClickListener {
                onboardingViewModel.setOnboard(false)
                findNavController().navigate(R.id.action_onboardingFragment_to_loginFragment)
            }

            btnNext.setOnClickListener {
                viewPager.setCurrentItem(nextItem(1), true)
            }
        }
    }

    private fun initViewPager() {
        adapter.submitList(images)

        viewPager = binding.viewPager
        viewPager.adapter = adapter

        val tabLayout = binding.indicator
        TabLayoutMediator(tabLayout, viewPager) { _, _ ->
        }.attach()

        viewPager.registerOnPageChangeCallback(object : ViewPager2.OnPageChangeCallback() {
            override fun onPageSelected(position: Int) {
                super.onPageSelected(position)
                val hide = position == (images.size - 1)
                nextButton(!hide)
            }
        })
    }

    private fun nextButton(value: Boolean) {
        binding.btnNext.isVisible = value
    }

    private fun nextItem(value: Int): Int {
        return viewPager.currentItem + value
    }
}
