package com.rafi.ecommerce.ui.main.fulfillment.checkout

import android.os.Bundle
import android.util.Log
import android.view.View
import androidx.core.view.isInvisible
import androidx.fragment.app.setFragmentResultListener
import androidx.fragment.app.viewModels
import androidx.lifecycle.Lifecycle
import androidx.lifecycle.lifecycleScope
import androidx.lifecycle.repeatOnLifecycle
import androidx.navigation.fragment.findNavController
import androidx.recyclerview.widget.LinearLayoutManager
import coil.load
import com.google.firebase.analytics.FirebaseAnalytics
import com.google.firebase.analytics.ktx.analytics
import com.google.firebase.analytics.logEvent
import com.google.firebase.ktx.Firebase
import com.rafi.core.data.model.FulfillmentModel
import com.rafi.core.data.model.ProductCartModel
import com.rafi.core.util.Resources
import com.rafi.ecommerce.R
import com.rafi.ecommerce.databinding.FragmentCheckoutBinding
import com.rafi.ecommerce.ui.main.cart.CartAdapter
import com.rafi.ecommerce.ui.main.fulfillment.payment.PaymentFragment
import com.rafi.ecommerce.ui.main.fulfillment.rating.RatingFragment
import com.rafi.ecommerce.util.BaseFragment
import com.rafi.ecommerce.util.GeneralExtension.parcelable
import com.rafi.ecommerce.util.ThrowableExt.getErrorMessage
import com.rafi.ecommerce.util.showSnackbar
import com.rafi.ecommerce.util.toCurrencyFormat
import dagger.hilt.android.AndroidEntryPoint
import kotlinx.coroutines.flow.first
import kotlinx.coroutines.launch
import kotlinx.coroutines.runBlocking

@AndroidEntryPoint
class CheckoutFragment :
    BaseFragment<FragmentCheckoutBinding>(FragmentCheckoutBinding::inflate) {

    private val checkoutViewModel: CheckoutViewModel by viewModels()

    private val checkoutAdapter: CheckoutAdapter by lazy {
        CheckoutAdapter(object : CartAdapter.CartCallback {
            override fun increaseQuantityCallback(cart: ProductCartModel) {
                checkoutViewModel.updateProductQuantity(cart, true)
            }

            override fun decreaseQuantityCallback(cart: ProductCartModel) {
                checkoutViewModel.updateProductQuantity(cart, false)
            }

            override fun itemClickCallback(productId: String) {
                TODO("Not yet implemented")
            }

            override fun deleteItemCallback(cart: ProductCartModel) {
                TODO("Not yet implemented")
            }

            override fun checkItemCallback(cart: ProductCartModel, isCheck: Boolean) {
                TODO("Not yet implemented")
            }
        })
    }

    private val firebaseAnalytics: FirebaseAnalytics by lazy {
        Firebase.analytics
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        initAppBar()
        initAdapter()
        observeItem()
        observeCheckout()
        initAction()
    }

    private fun logBeginCheckoutEvent(listProduct: List<ProductCartModel>) {
        firebaseAnalytics.logEvent(FirebaseAnalytics.Event.BEGIN_CHECKOUT) {
            Log.d("Log Checkout", "Called")
            val bundle = ArrayList<Bundle>()
            listProduct.map { product ->
                val itemBundle = Bundle().apply {
                    putString(FirebaseAnalytics.Param.ITEM_ID, product.productId)
                    putString(FirebaseAnalytics.Param.ITEM_NAME, product.productName)
                    putString(FirebaseAnalytics.Param.ITEM_BRAND, product.brand)
                    putString(FirebaseAnalytics.Param.ITEM_VARIANT, product.variantName)
                }
                bundle.add(itemBundle)
            }
            param(FirebaseAnalytics.Param.ITEMS, bundle.toTypedArray())
            param(FirebaseAnalytics.Param.CURRENCY, "IDR")
            param(FirebaseAnalytics.Param.VALUE, checkoutViewModel.totalPrice.toLong())
        }
    }

    private fun logPurchaseEvent(data: FulfillmentModel) {
        firebaseAnalytics.logEvent(FirebaseAnalytics.Event.PURCHASE) {
            Log.d("Log Purchase", "Called")
            val list = runBlocking { checkoutViewModel.listData.first() }
            val bundle = ArrayList<Bundle>()
            list.map { product ->
                val itemBundle = Bundle().apply {
                    putString(FirebaseAnalytics.Param.ITEM_ID, product.productId)
                    putString(FirebaseAnalytics.Param.ITEM_NAME, product.productName)
                    putString(FirebaseAnalytics.Param.ITEM_BRAND, product.brand)
                    putString(FirebaseAnalytics.Param.ITEM_VARIANT, product.variantName)
                }
                bundle.add(itemBundle)
            }
            param(FirebaseAnalytics.Param.ITEMS, bundle.toTypedArray())
            param(FirebaseAnalytics.Param.CURRENCY, "IDR")
            param(FirebaseAnalytics.Param.VALUE, checkoutViewModel.totalPrice.toLong())
            param(FirebaseAnalytics.Param.START_DATE, data.date)
            param(FirebaseAnalytics.Param.TRANSACTION_ID, data.invoiceId)
        }
    }

    private fun initAppBar() {
        val toolbar = binding.appBarLayout.toolbar
        toolbar.title = getString(R.string.checkout)
        toolbar.setNavigationIcon(R.drawable.ic_arrow_back)
        toolbar.setNavigationOnClickListener {
            findNavController().navigateUp()
        }
    }

    private fun initAdapter() {
        binding.rvCheckoutItem.apply {
            adapter = checkoutAdapter
            layoutManager = object : LinearLayoutManager(requireActivity()) {
                override fun canScrollVertically(): Boolean {
                    return false
                }
            }
            itemAnimator = null
        }
    }

    private fun observeItem() {
        viewLifecycleOwner.lifecycleScope.launch {
            repeatOnLifecycle(Lifecycle.State.STARTED) {
                checkoutViewModel.listData.collect { data ->
                    checkoutAdapter.submitList(data)
                    setPrice(data)
                    setPayment()
                    logBeginCheckoutEvent(data)
                }
            }
        }

        setFragmentResultListener(PaymentFragment.REQUEST_KEY) { _, bundle ->
            checkoutViewModel.paymentItem =
                bundle.parcelable(PaymentFragment.BUNDLE_PAYMENT_KEY)
            setPayment()
        }
    }

    private fun observeCheckout() {
        viewLifecycleOwner.lifecycleScope.launch {
            repeatOnLifecycle(Lifecycle.State.STARTED) {
                checkoutViewModel.state.collect { result ->
                    if (result != null) {
                        showLoading(result is Resources.Loading)
                        when (result) {
                            is Resources.Loading -> {}
                            is Resources.Success -> {
                                logPurchaseEvent(result.data)
                                initRating(result.data)
                            }

                            is Resources.Error -> {
                                val message = result.message.getErrorMessage()
                                binding.root.showSnackbar(message)
                            }
                        }
                    }
                }
            }
        }
    }

    private fun initAction() {
        binding.apply {
            cvChoosePayment.setOnClickListener {
                findNavController().navigate(R.id.action_checkoutFragment_to_paymentFragment)
            }
            btnBuyFromCart.setOnClickListener {
                checkoutViewModel.postPayment()
                checkoutViewModel.deleteItemFromCart()
            }
        }
    }

    private fun initRating(data: FulfillmentModel) {
        val bundle = Bundle().apply {
            putParcelable(RatingFragment.DATA_BUNDLE_KEY, data)
            putString("flag", FLAG_CHECKOUT)
        }
        findNavController().navigate(R.id.action_checkoutFragment_to_ratingFragment, bundle)
    }

    private fun setPrice(data: List<ProductCartModel>) {
        checkoutViewModel.totalPrice = data.sumOf {
            (it.productPrice + it.variantPrice) * it.quantity!!
        }
        binding.tvCheckoutPrice.text = checkoutViewModel.totalPrice.toCurrencyFormat()
    }

    private fun setPayment() {
        if (checkoutViewModel.paymentItem != null) {
            binding.apply {
                ivPaymentCard.load(checkoutViewModel.paymentItem!!.image) {
                    crossfade(true)
                    placeholder(R.drawable.product_image_placeholder)
                    error(R.drawable.product_image_placeholder)
                }
                tvCheckoutPayment.text = checkoutViewModel.paymentItem!!.label
                binding.btnBuyFromCart.isEnabled = true
            }
        }
    }

    private fun showLoading(isLoading: Boolean) {
        with(binding) {
            pbCheckout.isInvisible = !isLoading
            btnBuyFromCart.isInvisible = isLoading
        }
    }

    companion object {
        const val ARG_DATA = "arg_data"
        const val FLAG_CHECKOUT = "checkout"
    }
}
