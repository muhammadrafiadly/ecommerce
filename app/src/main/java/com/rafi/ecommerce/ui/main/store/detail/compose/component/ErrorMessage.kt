package com.rafi.ecommerce.ui.main.store.detail.compose.component

import androidx.compose.foundation.Image
import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.Spacer
import androidx.compose.foundation.layout.height
import androidx.compose.foundation.layout.size
import androidx.compose.material3.Button
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.platform.LocalContext
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.text.PlatformTextStyle
import androidx.compose.ui.text.TextStyle
import androidx.compose.ui.text.font.FontWeight
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import com.rafi.ecommerce.R
import com.rafi.ecommerce.ui.theme.ComposeAppTheme
import com.rafi.ecommerce.ui.theme.poppinsFamily
import com.rafi.ecommerce.util.ThrowableExt.getErrorSubtitle
import com.rafi.ecommerce.util.ThrowableExt.getErrorTitle

@Composable
fun ErrorMessage(
    modifier: Modifier = Modifier,
    error: Throwable,
    onErrorAction: () -> Unit
) {
    val title = error.getErrorTitle(LocalContext.current)
    val subtitle = error.getErrorSubtitle(LocalContext.current)
    val action = stringResource(id = R.string.refresh_btn)
    Column(
        modifier,
        verticalArrangement = Arrangement.Center,
        horizontalAlignment = Alignment.CenterHorizontally
    ) {
        Image(
            painter = painterResource(id = R.drawable.error_image),
            contentDescription = null,
            modifier = Modifier.size(128.dp)
        )
        Spacer(modifier = Modifier.height(8.dp))
        Text(
            text = title,
            fontSize = 32.sp,
            style = TextStyle(
                fontFamily = poppinsFamily,
                fontWeight = FontWeight.Medium,
                platformStyle = PlatformTextStyle(
                    includeFontPadding = false,
                ),
            ),
        )
        Spacer(modifier = Modifier.height(4.dp))
        Text(
            text = subtitle,
            fontSize = 16.sp,
            style = TextStyle(
                fontFamily = poppinsFamily,
                fontWeight = FontWeight.Normal,
                platformStyle = PlatformTextStyle(
                    includeFontPadding = false,
                ),
            ),
        )
        Spacer(modifier = Modifier.height(8.dp))
        Button(onClick = { onErrorAction() }) {
            Text(
                text = action,
                fontSize = 14.sp,
                style = TextStyle(
                    fontFamily = poppinsFamily,
                    fontWeight = FontWeight.Medium,
                    platformStyle = PlatformTextStyle(
                        includeFontPadding = false,
                    ),
                ),
            )
        }
    }
}

@Preview(showBackground = true)
@Composable
fun ErrorMessagePreview() {
    val sampleError = Throwable("Oops, something went wrong")
    ComposeAppTheme {
        com.rafi.ecommerce.ui.main.fulfillment.review.compose.components.ErrorMessage(
            error = sampleError,
            onErrorAction = {}
        )
    }
}
