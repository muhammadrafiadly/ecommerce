package com.rafi.ecommerce.ui.main.store

import android.view.LayoutInflater
import android.view.ViewGroup
import android.view.animation.AnimationUtils
import androidx.paging.PagingDataAdapter
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.RecyclerView
import androidx.viewbinding.ViewBinding
import coil.load
import com.rafi.core.data.model.ProductModel
import com.rafi.ecommerce.R
import com.rafi.ecommerce.databinding.ItemProductGridBinding
import com.rafi.ecommerce.databinding.ItemProductLinearBinding
import com.rafi.ecommerce.util.toCurrencyFormat

class ProductAdapter(private val itemClickCallback: (ProductModel) -> Unit) :
    PagingDataAdapter<ProductModel, RecyclerView.ViewHolder>(ProductComparator) {

    companion object {
        const val LINEAR_TYPE = 1
        const val GRID_TYPE = 2
    }

    var viewType = LINEAR_TYPE

    override fun getItemViewType(position: Int): Int {
        return viewType
    }

    object ProductComparator : DiffUtil.ItemCallback<ProductModel>() {
        override fun areItemsTheSame(oldItem: ProductModel, newItem: ProductModel): Boolean {
            return oldItem.productId == newItem.productId
        }

        override fun areContentsTheSame(oldItem: ProductModel, newItem: ProductModel): Boolean {
            return oldItem == newItem
        }
    }

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        getItem(position)?.let {
            (holder as ProductViewHolder<*>).bind(it)
        }
        if (viewType == LINEAR_TYPE) {
            holder.itemView.startAnimation(
                AnimationUtils.loadAnimation(holder.itemView.context, R.anim.slide_in)
            )
        } else {
            holder.itemView.startAnimation(
                AnimationUtils.loadAnimation(holder.itemView.context, R.anim.zoom_in)
            )
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        return if (viewType == LINEAR_TYPE) {
            val binding = ItemProductLinearBinding.inflate(
                LayoutInflater.from(parent.context),
                parent,
                false
            )
            ProductViewHolder(binding, viewType, itemClickCallback)
        } else {
            val binding = ItemProductGridBinding.inflate(
                LayoutInflater.from(parent.context),
                parent,
                false
            )
            ProductViewHolder(binding, viewType, itemClickCallback)
        }
    }

    class ProductViewHolder<T : ViewBinding>(
        private val binding: T,
        private val viewType: Int,
        private val itemClickCallback: (ProductModel) -> Unit
    ) :
        RecyclerView.ViewHolder(binding.root) {
        fun bind(product: ProductModel) {
            if (viewType == LINEAR_TYPE) {
                with(binding as ItemProductLinearBinding) {
                    ivProductImage.load(product.image) {
                        crossfade(true)
                        placeholder(R.drawable.image_blank_product)
                        error(R.drawable.image_blank_product)
                    }
                    tvProductName.text = product.productName
                    tvProductPrice.text = product.productPrice.toCurrencyFormat()
                    tvProductStore.text = product.store
                    tvProductRatingAndSales.text = itemView.resources.getString(
                        R.string.product_rating_and_sold,
                        product.productRating.toFloat(),
                        product.sale
                    )
                }
            } else {
                with(binding as ItemProductGridBinding) {
                    ivProduct.load(product.image) {
                        crossfade(true)
                        placeholder(R.drawable.image_blank_product)
                        error(R.drawable.image_blank_product)
                    }
                    tvProductName.text = product.productName
                    tvProductPrice.text = product.productPrice.toCurrencyFormat()
                    tvStore.text = product.store
                    tvRatingSales.text = itemView.resources.getString(
                        R.string.product_rating_and_sold,
                        product.productRating.toFloat(),
                        product.sale
                    )
                }
            }
            itemView.setOnClickListener { itemClickCallback(product) }
        }
    }
}
