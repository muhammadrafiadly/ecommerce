package com.rafi.ecommerce.ui.main.fulfillment.review

import androidx.lifecycle.SavedStateHandle
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.rafi.core.data.model.ProductReviewsModel
import com.rafi.core.data.repository.ProductRepository
import com.rafi.core.util.Resources
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.StateFlow
import kotlinx.coroutines.launch
import javax.inject.Inject

@HiltViewModel
class ReviewViewModel @Inject constructor(
    private val productRepository: ProductRepository,
    savedStateHandle: SavedStateHandle
) : ViewModel() {

    var productId: String = savedStateHandle[ReviewFragment.BUNDLE_PRODUCT_ID_KEY] ?: ""

    private val _reviewProductState = MutableStateFlow<Resources<List<ProductReviewsModel>>?>(null)
    val reviewProductState: StateFlow<Resources<List<ProductReviewsModel>>?> = _reviewProductState

    init {
        getListReview()
    }

    fun getListReview() {
        viewModelScope.launch {
            productRepository.reviewProduct(productId).collect { resultState ->
                _reviewProductState.value = resultState
            }
        }
    }
}
