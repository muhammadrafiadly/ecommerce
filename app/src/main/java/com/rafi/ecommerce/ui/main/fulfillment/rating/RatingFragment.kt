package com.rafi.ecommerce.ui.main.fulfillment.rating

import android.os.Bundle
import android.util.Log
import android.view.View
import androidx.activity.OnBackPressedCallback
import androidx.core.view.isInvisible
import androidx.fragment.app.viewModels
import androidx.lifecycle.Lifecycle
import androidx.lifecycle.lifecycleScope
import androidx.lifecycle.repeatOnLifecycle
import androidx.navigation.fragment.findNavController
import com.google.firebase.analytics.FirebaseAnalytics
import com.google.firebase.analytics.ktx.analytics
import com.google.firebase.ktx.Firebase
import com.rafi.core.util.Resources
import com.rafi.ecommerce.R
import com.rafi.ecommerce.databinding.FragmentRatingBinding
import com.rafi.ecommerce.util.BaseFragment
import com.rafi.ecommerce.util.Helper.sendLogButtonClicked
import com.rafi.ecommerce.util.ThrowableExt.getErrorMessage
import com.rafi.ecommerce.util.showSnackbar
import com.rafi.ecommerce.util.toCurrencyFormat
import dagger.hilt.android.AndroidEntryPoint
import kotlinx.coroutines.launch

@AndroidEntryPoint
class RatingFragment : BaseFragment<FragmentRatingBinding>(FragmentRatingBinding::inflate) {

    private val viewModel: RatingViewModel by viewModels()

    private val firebaseAnalytics: FirebaseAnalytics by lazy {
        Firebase.analytics
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        Log.d("RatingFragment", "${viewModel.flag}")
        initBackButton()
        initAppBar()
        setupView()
        initButton()
        observeRating()
    }

    private fun initAppBar() {
        val toolbar = binding.appBarLayout.toolbar
        toolbar.title = getString(R.string.status)
        toolbar.isTitleCentered = true
    }

    private fun initButton() {
        binding.btnRatingDone.setOnClickListener {
            firebaseAnalytics.sendLogButtonClicked("Rating: Finish")
            val rating: Int? = with(binding.rbTransactionRating.rating.toInt()) {
                if (this == 0) {
                    null
                } else {
                    this
                }
            }
            val review: String? = with(binding.editTextReview.text.toString()) {
                this.ifEmpty { null }
            }
            val invoiceId = viewModel.transactionDetail!!.invoiceId
            viewModel.postRating(invoiceId, rating, review)
        }
    }

    private fun observeRating() {
        viewLifecycleOwner.lifecycleScope.launch {
            repeatOnLifecycle(Lifecycle.State.STARTED) {
                viewModel.state.collect { result ->
                    if (result != null) {
                        showLoading(result is Resources.Loading)
                        when (result) {
                            is Resources.Loading -> {}
                            is Resources.Success -> {
                                val isRatingChanged =
                                    binding.rbTransactionRating.rating !=
                                        viewModel.rating?.toFloat()
                                val isReviewChanged =
                                    binding.editTextReview.text.toString() != viewModel.review

                                if (!isRatingChanged && !isReviewChanged) {
                                    findNavController().navigateUp()
                                } else if (result.data) {
                                    initMoveToMainFragment()
                                }
                            }

                            is Resources.Error -> {
                                val message = result.message.getErrorMessage()
                                binding.root.showSnackbar(message)
                            }
                        }
                    }
                }
            }
        }
    }

    private fun initBackButton() {
        requireActivity().onBackPressedDispatcher.addCallback(
            viewLifecycleOwner,
            object : OnBackPressedCallback(true) {
                override fun handleOnBackPressed() {
                    if (viewModel.flag == FLAG_CHECKOUT) {
                        initMoveToMainFragment()
                    } else {
                        findNavController().navigateUp()
                    }
                }
            }
        )
    }

    private fun setupView() {
        val data = viewModel.transactionDetail
        with(binding) {
            tvTransactionId.text = data?.invoiceId
            tvTransactionStatus.text =
                if (data?.status == true) {
                    getString(R.string.succeed)
                } else {
                    getString(R.string.failed)
                }
            tvTransactionDate.text = data?.date
            tvTransactionTime.text = data?.time
            tvPaymentMethod.text = data?.payment
            tvTotalPayment.text = data?.total?.toCurrencyFormat()
            rbTransactionRating.rating = viewModel.rating?.toFloat() ?: 0F
            editTextReview.setText(viewModel.review)
        }
    }

    private fun initMoveToMainFragment() {
        if (viewModel.flag == "checkout") {
            val arg = RatingFragmentDirections.actionRatingFragmentToMainFragment()
            arg.flag = FLAG_HOME
            Log.d("RatingFragment", "toHome ${arg.flag}")
            findNavController().navigate(
                arg
            )
        } else {
            val arg = RatingFragmentDirections.actionRatingFragmentToMainFragment()
            Log.d("RatingFragment", "toTransaction ${arg.flag}")
            findNavController().navigate(
                R.id.action_ratingFragment_to_mainFragment
            )
        }
    }

    private fun showLoading(isLoading: Boolean) {
        binding.btnRatingDone.isInvisible = isLoading
        binding.pbRating.isInvisible = !isLoading
    }

    companion object {
        const val RATING_BUNDLE_KEY = "rating_bundle_key"
        const val REVIEW_BUNDLE_KEY = "review_bundle_key"
        const val DATA_BUNDLE_KEY = "data_bundle_key"
        const val FLAG_HOME = "home"
        const val FLAG_CHECKOUT = "checkout"
    }
}
