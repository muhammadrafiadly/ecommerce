package com.rafi.ecommerce.util

import android.content.Context
import com.google.gson.Gson
import com.rafi.core.data.source.remote.response.ErrorResponse
import com.rafi.ecommerce.R
import retrofit2.HttpException
import java.io.IOException

object ThrowableExt {
    fun Throwable.getErrorMessage(): String {
        var message = this.message
        if (this is HttpException) {
            val errorResponse =
                Gson().fromJson(
                    this.response()?.errorBody()?.string(),
                    ErrorResponse::class.java
                ) ?: ErrorResponse()
            errorResponse.message?.let { message = it }
        }
        return message.toString()
    }

    fun Throwable.getErrorTitle(context: Context): String {
        return when (this) {
            is HttpException -> if (response()?.code() == 404) {
                context.getString(R.string.empty_title)
            } else {
                code().toString()
            }

            is IOException -> context.getString(R.string.connection_title)
            else -> context.getString(R.string.error_title)
        }
    }

    fun Throwable.getErrorSubtitle(context: Context): String {
        return when (this) {
            is HttpException -> if (response()?.code() == 404) {
                context.getString(R.string.empty_data_desc)
            } else {
                response()?.message().toString()
            }

            is IOException -> context.getString(R.string.io_exception_desc)
            else -> message.toString()
        }
    }

    fun Throwable.getErrorAction(context: Context): String {
        return when (this) {
            is HttpException -> if (response()?.code() == 404) {
                context.getString(R.string.reset)
            } else {
                context.getString(R.string.refresh_btn)
            }

            else -> context.getString(R.string.refresh_btn)
        }
    }
}
