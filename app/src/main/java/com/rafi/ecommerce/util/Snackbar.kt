package com.rafi.ecommerce.util

import android.view.View
import android.widget.TextView
import androidx.core.content.res.ResourcesCompat
import com.google.android.material.snackbar.Snackbar
import com.rafi.ecommerce.R

fun View.showSnackbar(message: String) {
    val snackbar = Snackbar.make(this, message, Snackbar.LENGTH_SHORT)
    val textSnackbar =
        snackbar.view.findViewById<TextView>(com.google.android.material.R.id.snackbar_text)
    textSnackbar.setTypeface(
        ResourcesCompat.getFont(
            this.context,
            R.font.poppins_regular_400
        )
    )
    snackbar.show()
}
