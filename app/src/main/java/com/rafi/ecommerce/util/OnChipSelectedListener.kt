package com.rafi.ecommerce.util

interface OnChipSelectedListener {
    fun onChipSelected(chipText: String)
}
