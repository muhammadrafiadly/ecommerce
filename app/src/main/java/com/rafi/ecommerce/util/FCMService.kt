package com.rafi.ecommerce.util

import android.app.NotificationChannel
import android.app.NotificationManager
import android.app.PendingIntent
import android.content.Context
import android.graphics.Bitmap
import android.graphics.BitmapFactory
import android.media.RingtoneManager
import android.os.Build
import android.util.Log
import androidx.core.app.NotificationCompat
import androidx.navigation.NavDeepLinkBuilder
import com.google.firebase.messaging.FirebaseMessagingService
import com.google.firebase.messaging.RemoteMessage
import com.rafi.core.data.model.NotificationModel
import com.rafi.core.data.source.local.room.dao.NotificationDao
import com.rafi.core.util.DataMapper.toNotificationEntity
import com.rafi.ecommerce.MainActivity
import com.rafi.ecommerce.R
import dagger.hilt.android.AndroidEntryPoint
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import java.net.URL
import javax.inject.Inject

@AndroidEntryPoint
class FCMService : FirebaseMessagingService() {

    @Inject
    lateinit var notificationDao: NotificationDao

    private var notificationId = 0

    override fun onNewToken(token: String) {
        super.onNewToken(token)
    }

    override fun onMessageReceived(message: RemoteMessage) {
        super.onMessageReceived(message)
        val data = message.data.toNotification()
        Log.d("FCM", "${message.data}")
        sendNotification(data)
        saveNotification(data)
    }

    private fun saveNotification(data: NotificationModel) {
        CoroutineScope(Dispatchers.IO).launch {
            notificationDao.insert(data.toNotificationEntity())
        }
    }

    private fun sendNotification(data: NotificationModel) {
        val defaultSoundUri = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION)
        val notificationBuilder = NotificationCompat.Builder(this, CHANNEL_ID)
            .setSmallIcon(R.drawable.ic_notification)
            .setLargeIcon(createImageBitmap(data.image))
            .setContentTitle(data.title)
            .setContentText(data.body)
            .setAutoCancel(true)
            .setSound(defaultSoundUri)
            .setContentIntent(createContentIntent())

        val notificationManager =
            getSystemService(Context.NOTIFICATION_SERVICE) as NotificationManager

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            val channel = NotificationChannel(
                CHANNEL_ID,
                CHANNEL_NAME,
                NotificationManager.IMPORTANCE_HIGH,
            )
            notificationManager.createNotificationChannel(channel)
        }

        notificationManager.notify(++notificationId, notificationBuilder.build())
    }

    private fun createContentIntent(): PendingIntent {
        return NavDeepLinkBuilder(this)
            .setComponentName(MainActivity::class.java)
            .setGraph(R.navigation.app_nav)
            .setDestination(R.id.notificationFragment)
            .createPendingIntent()
    }

    private fun createImageBitmap(image: String): Bitmap {
        val url = URL(image)
        return BitmapFactory.decodeStream(url.openConnection().getInputStream())
    }

    companion object {
        private const val CHANNEL_ID = "notification_1"
        private const val CHANNEL_NAME = "notification_update_app"
    }
}

private fun Map<String, String>.toNotification(): NotificationModel =
    NotificationModel(
        image = this["image"] ?: "",
        title = this["title"] ?: "",
        body = this["body"] ?: "",
        type = this["type"] ?: "",
        date = this["date"] ?: "",
        time = this["time"] ?: "",
    )
