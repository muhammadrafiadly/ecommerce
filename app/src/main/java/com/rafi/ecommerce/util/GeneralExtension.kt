package com.rafi.ecommerce.util

import android.os.Build
import android.os.Bundle
import android.os.Parcelable
import androidx.fragment.app.Fragment
import com.rafi.ecommerce.ui.prelogin.login.LoginFragment
import com.rafi.ecommerce.ui.prelogin.onboarding.OnboardingFragment
import com.rafi.ecommerce.ui.prelogin.register.RegisterFragment

object GeneralExtension {

    inline fun <reified T : Parcelable> Bundle.parcelableArrayList(key: String): ArrayList<T>? =
        when {
            Build.VERSION.SDK_INT >= 33 -> getParcelableArrayList(key, T::class.java)
            else ->
                @Suppress("DEPRECATION")
                getParcelableArrayList(key)
        }

    inline fun <reified T : Parcelable> Bundle.parcelable(key: String): T? = when {
        Build.VERSION.SDK_INT >= 33 -> getParcelable(key, T::class.java)
        else ->
            @Suppress("DEPRECATION")
            getParcelable(key)
                as? T
    }

    fun Fragment.isPreLoginFragment(): Boolean {
        return this is OnboardingFragment || this is LoginFragment || this is RegisterFragment
    }
}
