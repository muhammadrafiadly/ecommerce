package com.rafi.ecommerce.util

import android.app.UiModeManager
import android.content.Context
import android.os.Build
import android.text.Spannable
import android.text.SpannableStringBuilder
import android.text.style.ForegroundColorSpan
import android.util.TypedValue
import androidx.appcompat.app.AppCompatDelegate
import androidx.core.os.bundleOf
import com.google.firebase.analytics.FirebaseAnalytics
import com.rafi.ecommerce.R
import java.util.Locale

object Helper {
    fun getTncPrivacyPolicySpanText(context: Context): SpannableStringBuilder {
        val color = getColorTheme(context, com.google.android.material.R.attr.colorPrimary)

        val listSpanData = listOf(R.string.tnc, R.string.privacy_policy)
        val tncPrivacyPolicyText = context.getString(R.string.terms_condition)
        val spannable = SpannableStringBuilder(tncPrivacyPolicyText)

        listSpanData.forEach { textId ->
            val text = context.getString(textId)
            val startIndex = tncPrivacyPolicyText.indexOf(text)
            val endIndex = startIndex + text.length

            spannable.setSpan(
                ForegroundColorSpan(color),
                startIndex,
                endIndex,
                Spannable.SPAN_EXCLUSIVE_INCLUSIVE
            )
        }
        return spannable
    }

    fun getColorTheme(context: Context, colorName: Int): Int {
        val typedValue = TypedValue()
        val theme = context.theme
        theme.resolveAttribute(colorName, typedValue, true)
        return typedValue.data
    }

    fun FirebaseAnalytics.sendLogButtonClicked(buttonName: String) {
        this.logEvent("button_click", bundleOf("button_name" to buttonName))
    }

    fun setAppTheme(context: Context, isDarkTheme: Boolean) {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.S) {
            val uiMode = context.getSystemService(Context.UI_MODE_SERVICE) as UiModeManager
            uiMode.setApplicationNightMode(
                if (isDarkTheme) {
                    UiModeManager.MODE_NIGHT_YES
                } else {
                    UiModeManager.MODE_NIGHT_NO
                }
            )
        } else {
            AppCompatDelegate.setDefaultNightMode(
                if (isDarkTheme) {
                    AppCompatDelegate.MODE_NIGHT_YES
                } else {
                    AppCompatDelegate.MODE_NIGHT_NO
                }
            )
        }
    }

    fun setAppLanguage(context: Context, appLanguage: String) {
        val locale = Locale(appLanguage)
        Locale.setDefault(locale)

        val config = context.resources.configuration
        config.setLocale(locale)

        context.createConfigurationContext(config)
        context.resources.updateConfiguration(
            config,
            context.resources.displayMetrics
        )
    }
}
