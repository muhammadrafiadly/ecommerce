package com.rafi.core.data.source.local.room

import android.content.Context
import androidx.room.Room
import androidx.test.core.app.ApplicationProvider
import com.rafi.core.data.source.local.room.dao.NotificationDao
import com.rafi.core.data.source.local.room.entity.dummyNotification
import kotlinx.coroutines.flow.first
import kotlinx.coroutines.test.runTest
import org.junit.After
import org.junit.Assert.assertEquals
import org.junit.Before
import org.junit.Test
import org.junit.runner.RunWith
import org.robolectric.RobolectricTestRunner

@RunWith(RobolectricTestRunner::class)
class NotificationDaoTest {

    private lateinit var appRoomDatabase: AppRoomDatabase
    private lateinit var notificationDao: NotificationDao

    @Before
    fun setUp() {
        val context = ApplicationProvider.getApplicationContext<Context>()
        appRoomDatabase = Room.inMemoryDatabaseBuilder(
            context, AppRoomDatabase::class.java
        ).allowMainThreadQueries().build()
        notificationDao = appRoomDatabase.notificationDao()
    }

    @After
    fun closeDb() {
        appRoomDatabase.close()
    }

    @Test
    fun `test insert and getNotification`() = runTest {
        // Given
        dummyNotification.map {
            notificationDao.insert(it)
        }

        // When
        val actualData = notificationDao.getNotification().first()
        val expectedData = dummyNotification.reversed()

        // Then
        assertEquals(expectedData, actualData)
    }

    @Test
    fun `test insert and getNotificationSize`() = runTest {
        // Given
        dummyNotification.map {
            notificationDao.insert(it)
        }

        // When
        val actualData = notificationDao.getNotificationSize().first()
        val expectedData = dummyNotification.size

        // Then
        assertEquals(expectedData, actualData)
    }

    @Test
    fun `test insert and update`() = runTest {
        // Given
        dummyNotification.map {
            notificationDao.insert(it)
        }

        // When
        val dummyNotificationData = dummyNotification[0]
        notificationDao.update(dummyNotificationData.copy(isRead = true))
        val actualData = notificationDao.getNotification().first()
        val expectedData = dummyNotification.map {
            if (it.id == dummyNotificationData.id) {
                it.copy(isRead = true)
            } else {
                it
            }
        }.reversed()

        // Then
        assertEquals(expectedData, actualData)
    }
}
