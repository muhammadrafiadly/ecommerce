package com.rafi.core.data.repository

import app.cash.turbine.test
import com.nhaarman.mockitokotlin2.verify
import com.nhaarman.mockitokotlin2.whenever
import com.rafi.core.data.firebase.AppFirebaseMessaging
import com.rafi.core.data.model.LoginBody
import com.rafi.core.data.model.RegisterBody
import com.rafi.core.data.model.dummyPreferences
import com.rafi.core.data.source.local.preferences.dataStore.SessionPreferences
import com.rafi.core.data.source.remote.response.dummyLoginResponse
import com.rafi.core.data.source.remote.response.dummyProfileResponse
import com.rafi.core.data.source.remote.response.dummyRegisterResponse
import com.rafi.core.data.source.remote.retrofit.ApiService
import com.rafi.core.util.DataMapper.toUser
import com.rafi.core.util.Resources
import junit.framework.TestCase.assertEquals
import junit.framework.TestCase.assertFalse
import junit.framework.TestCase.assertTrue
import kotlinx.coroutines.flow.first
import kotlinx.coroutines.flow.flowOf
import kotlinx.coroutines.test.runTest
import okhttp3.MediaType.Companion.toMediaType
import okhttp3.RequestBody.Companion.toRequestBody
import org.junit.Before
import org.junit.Test
import org.junit.runner.RunWith
import org.junit.runners.JUnit4
import org.mockito.Mockito.mock

@RunWith(JUnit4::class)
class UserRepositoryTest {

    private lateinit var userRepository: UserRepository
    private lateinit var sessionPreferences: SessionPreferences
    private lateinit var apiService: ApiService
    private lateinit var firebaseMessaging: AppFirebaseMessaging

    @Before
    fun setUp() {
        sessionPreferences = mock()
        apiService = mock()
        firebaseMessaging = mock()
        userRepository = UserRepositoryImpl(
            sessionPreferences,
            apiService,
            firebaseMessaging
        )
    }

    @Test
    fun `test setOnboarding`() = runTest {
        userRepository.setOnboarding(false)
        verify(sessionPreferences).setIsOnboard(false)
    }

    @Test
    fun `test getOnboarding`() = runTest {
        whenever(sessionPreferences.getIsOnboard())
            .thenReturn(flowOf(false))
        val actualData = userRepository.getOnboarding().first()
        assertFalse(actualData)
    }

    @Test
    fun `test register when success`() = runTest {
        whenever(firebaseMessaging.getToken()).thenReturn("token")
        whenever(
            apiService.postRegister(
                RegisterBody(
                    "email",
                    "password",
                    "token"
                )
            )
        ).thenReturn(dummyRegisterResponse)

        userRepository.register("email", "password")
            .test {
                assertEquals(Resources.Loading, awaitItem())
                assertEquals(Resources.Success(true), awaitItem())
                awaitComplete()
            }

        verify(sessionPreferences).setSession(dummyRegisterResponse.data!!.toUser())
        verify(sessionPreferences).setUserAuth(true)
        verify(firebaseMessaging).subscribeToTopic("promo")
    }

    @Test
    fun `test register when failed`() = runTest {
        whenever(firebaseMessaging.getToken()).thenReturn("token")
        whenever(apiService.postRegister(RegisterBody("email", "password", "token")))
            .thenReturn(dummyRegisterResponse.copy(data = null))

        userRepository.register("email", "password").test {
            assertEquals(Resources.Loading, awaitItem())
            assertTrue(awaitItem() is Resources.Error)
            awaitComplete()
        }
    }

    @Test
    fun `test login when success`() = runTest {
        whenever(firebaseMessaging.getToken()).thenReturn("token")
        whenever(apiService.postLogin(LoginBody("email", "password", "token")))
            .thenReturn(dummyLoginResponse)

        userRepository.login("email", "password").test {
            assertEquals(Resources.Loading, awaitItem())
            assertEquals(Resources.Success(true), awaitItem())
            awaitComplete()
        }

        verify(sessionPreferences).setSession(dummyLoginResponse.data!!.toUser())
        verify(sessionPreferences).setUserAuth(true)
        verify(firebaseMessaging).subscribeToTopic("promo")
    }

    @Test
    fun `test login when failed`() = runTest {
        whenever(firebaseMessaging.getToken()).thenReturn("token")
        whenever(
            apiService.postLogin(
                LoginBody(
                    "email",
                    "password",
                    "token"
                )
            )
        ).thenReturn(dummyLoginResponse.copy(data = null))

        userRepository.login("email", "password").test {
            assertEquals(Resources.Loading, awaitItem())
            assertTrue(awaitItem() is Resources.Error)
            awaitComplete()
        }
    }

    @Test
    fun `test logout`() = runTest {
        userRepository.logout()
        verify(firebaseMessaging).unsubscribeFromTopic("promo")
    }

    @Test
    fun `test getSession`() = runTest {
        whenever(sessionPreferences.getSession())
            .thenReturn(flowOf(dummyPreferences))

        val actualData = userRepository.getSession().first()

        assertEquals(dummyPreferences, actualData)
    }

    @Test
    fun `test userAuth`() = runTest {
        whenever(sessionPreferences.getUserAuth())
            .thenReturn(flowOf(true))

        val actualData = userRepository.userAuth().first()

        assertEquals(true, actualData)
    }

    @Test
    fun `test uploadProfile when success`() = runTest {
        val userNameRequestBody = "name".toRequestBody("text/plain".toMediaType())
        whenever(
            apiService.postProfile(userNameRequestBody, null)
        ).thenReturn(dummyProfileResponse)

        userRepository.uploadProfile(userNameRequestBody, null).test {
            assertEquals(Resources.Loading, awaitItem())
            assertEquals(Resources.Success(true), awaitItem())
            awaitComplete()
        }

        verify(sessionPreferences).setSession(dummyProfileResponse.data!!.toUser())
    }

    @Test
    fun `test uploadProfile when failed`() = runTest {
        val userNameRequestBody = "name".toRequestBody("text/plain".toMediaType())
        whenever(
            apiService.postProfile(userNameRequestBody, null)
        ).thenReturn(dummyProfileResponse.copy(data = null))

        userRepository.uploadProfile(userNameRequestBody, null).test {
            assertEquals(Resources.Loading, awaitItem())
            assertTrue(awaitItem() is Resources.Error)
            awaitComplete()
        }
    }

    @Test
    fun `test setAppTheme`() = runTest {
        userRepository.setAppTheme(true)
        verify(sessionPreferences).setAppTheme(true)
    }

    @Test
    fun `test getAppTheme`() = runTest {
        whenever(sessionPreferences.getAppTheme()).thenReturn(flowOf(true))
        val actualData = userRepository.getAppTheme().first()

        assertEquals(true, actualData)
    }

    @Test
    fun `test setAppLanguage`() = runTest {
        userRepository.setAppLanguage("id")
        verify(sessionPreferences).setAppLanguage("id")
    }

    @Test
    fun `test getAppLanguage`() = runTest {
        whenever(sessionPreferences.getAppLanguage()).thenReturn(flowOf("id"))
        val actualData = userRepository.getAppLanguage().first()

        assertEquals("id", actualData)
    }
}
