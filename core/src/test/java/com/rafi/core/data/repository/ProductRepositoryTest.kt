package com.rafi.core.data.repository

import app.cash.turbine.test
import com.nhaarman.mockitokotlin2.verify
import com.nhaarman.mockitokotlin2.whenever
import com.rafi.core.data.source.local.room.dao.ProductCartDao
import com.rafi.core.data.source.local.room.dao.WishListProductsDao
import com.rafi.core.data.source.local.room.entity.dummyCartList
import com.rafi.core.data.source.local.room.entity.dummyWishList
import com.rafi.core.data.source.remote.response.dummyProductsDetailResponse
import com.rafi.core.data.source.remote.response.dummyProductsReviewResponse
import com.rafi.core.data.source.remote.response.dummySearchResponse
import com.rafi.core.data.source.remote.retrofit.ApiService
import com.rafi.core.util.DataMapper.toCart
import com.rafi.core.util.DataMapper.toCartEntity
import com.rafi.core.util.DataMapper.toProductDetails
import com.rafi.core.util.DataMapper.toProductReviews
import com.rafi.core.util.DataMapper.toWishlist
import com.rafi.core.util.Resources
import junit.framework.TestCase.assertEquals
import junit.framework.TestCase.assertTrue
import kotlinx.coroutines.flow.first
import kotlinx.coroutines.flow.flowOf
import kotlinx.coroutines.test.runTest
import org.junit.Assert
import org.junit.Before
import org.junit.Test
import org.junit.runner.RunWith
import org.junit.runners.JUnit4
import org.mockito.Mockito.mock

@RunWith(JUnit4::class)
class ProductRepositoryTest {

    private lateinit var productRepository: ProductRepository
    private lateinit var apiService: ApiService
    private lateinit var wishListProductsDao: WishListProductsDao
    private lateinit var productCartDao: ProductCartDao

    @Before
    fun setUp() {
        apiService = mock()
        wishListProductsDao = mock()
        productCartDao = mock()
        productRepository = ProductRepositoryImpl(
            apiService, wishListProductsDao, productCartDao
        )
    }

    @Test
    fun `test searchProducts with query`() = runTest {
        whenever(apiService.postSearch("query")).thenReturn(dummySearchResponse)
        productRepository.searchProducts("query")
            .test {
                assertEquals(Resources.Loading, awaitItem())
                assertEquals(Resources.Success(dummySearchResponse.data), awaitItem())
                awaitComplete()
            }
    }

    @Test
    fun `test searchProducts without query`() = runTest {
        productRepository.searchProducts("").test {
            assertEquals(Resources.Loading, awaitItem())
            assertEquals(Resources.Success<List<String>>(emptyList()), awaitItem())
            awaitComplete()
        }
    }

    @Test
    fun `test searchProducts when error`() = runTest {
        whenever(apiService.postSearch("query"))
            .then {
                throw Exception("Error")
            }
        productRepository.searchProducts("query")
            .test {
                assertEquals(Resources.Loading, awaitItem())
                assertTrue(awaitItem() is Resources.Error)
                awaitComplete()
            }
    }

    @Test
    fun `test productDetails when success`() = runTest {
        whenever(apiService.getProductDetails("id")).thenReturn(dummyProductsDetailResponse)
        productRepository.productDetails("id").test {
            assertEquals(Resources.Loading, awaitItem())
            assertEquals(
                Resources.Success(dummyProductsDetailResponse.data!!.toProductDetails()),
                awaitItem()
            )
            awaitComplete()
        }
    }

    @Test
    fun `test productDetails when error`() = runTest {
        whenever(apiService.getProductDetails("id"))
            .thenReturn(dummyProductsDetailResponse.copy(data = null))
        productRepository.productDetails("id").test {
            assertEquals(Resources.Loading, awaitItem())
            Assert.assertTrue(awaitItem() is Resources.Error)
            awaitComplete()
        }
    }

    @Test
    fun `test reviewProduct when success`() = runTest {
        whenever(apiService.getProductReviews("id")).thenReturn(dummyProductsReviewResponse)
        productRepository.reviewProduct("id").test {
            assertEquals(Resources.Loading, awaitItem())
            assertEquals(
                Resources.Success(
                    dummyProductsReviewResponse.data!!
                        .map { it.toProductReviews() }
                ),
                awaitItem()
            )
            awaitComplete()
        }
    }

    @Test
    fun `test reviewProduct when error`() = runTest {
        whenever(apiService.getProductReviews("id"))
            .thenReturn(dummyProductsReviewResponse.copy(data = null))
        productRepository.reviewProduct("id").test {
            assertEquals(Resources.Loading, awaitItem())
            Assert.assertTrue(awaitItem() is Resources.Error)
            awaitComplete()
        }
    }

    @Test
    fun `test getWishListProducts`() = runTest {
        whenever(wishListProductsDao.getProduct()).thenReturn(flowOf(dummyWishList))
        val actualData = productRepository.getWishListProducts().first()
        val expectedData = dummyWishList.map { it.toWishlist() }

        assertEquals(expectedData, actualData)
    }

    @Test
    fun `test getWishListProductSize`() = runTest {
        whenever(wishListProductsDao.getDataSize()).thenReturn(flowOf(dummyWishList.size))
        val actualData = productRepository.getWishListProductsSize().first()
        val expectedData = dummyWishList.size

        assertEquals(expectedData, actualData)
    }

    @Test
    fun `test checkWishListProducts`() = runTest {
        whenever(wishListProductsDao.checkExistsProduct("id")).thenReturn(true)
        val actualData = productRepository.checkWishListProducts("id")

        Assert.assertTrue(actualData)
    }

    @Test
    fun `test insertWishList`() = runTest {
        productRepository.insertWishList(dummyWishList[0].toWishlist())
        verify(wishListProductsDao).insert(dummyWishList[0])
    }

    @Test
    fun `test deleteWishList`() = runTest {
        productRepository.deleteWishList(dummyWishList[0].toWishlist())
        verify(wishListProductsDao).delete(dummyWishList[0])
    }

    @Test
    fun `test getProductCart`() = runTest {
        whenever(productCartDao.getProduct()).thenReturn(flowOf(dummyCartList))
        val actualData = productRepository.getProductCart().first()
        val expectedData = dummyCartList.map { it.toCart() }

        assertEquals(expectedData, actualData)
    }

    @Test
    fun `test getProductCartSize`() = runTest {
        whenever(productCartDao.getDataSize()).thenReturn(flowOf(dummyCartList.size))
        val actualData = productRepository.getProductCartSize().first()
        val expectedData = dummyCartList.size

        assertEquals(expectedData, actualData)
    }

    @Test
    fun `test isReadyStock when quantity is null and not exist`() = runTest {
        val cart = dummyCartList[0].toCart().copy(quantity = null, stock = 5)
        whenever(productCartDao.checkExistProduct(cart.productId)).thenReturn(false)

        val actualData = productRepository.isReadyStock(cart)

        Assert.assertTrue(actualData)
    }

    @Test
    fun `test isReadyStock when quantity is null and exist`() = runTest {
        val cart = dummyCartList[0].toCart().copy(quantity = null, stock = 5)
        whenever(productCartDao.checkExistProduct(cart.productId)).thenReturn(true)
        whenever(productCartDao.getDetailData(cart.productId)).thenReturn(flowOf(cart.toCartEntity()))

        val actualData = productRepository.isReadyStock(cart)

        Assert.assertTrue(actualData)
    }

    @Test
    fun `test isReadyStock when quantity null`() = runTest {
        val cart = dummyCartList[0].toCart().copy(quantity = 5, stock = 5)

        val actualData = productRepository.isReadyStock(cart)
        Assert.assertFalse(actualData)
    }

    @Test
    fun `test insertCart`() = runTest {
        val cart = dummyCartList[0].toCart()
        whenever(productCartDao.checkExistProduct(cart.productId)).thenReturn(false)
        productRepository.insertCart(cart)

        verify(productCartDao).insert(cart.toCartEntity())
    }

    @Test
    fun `test updateCartQuantity when quantity is null`() = runTest {
        val cart = dummyCartList[0].toCart().copy(quantity = null)
        val cartEntity = cart.toCartEntity()
        whenever(productCartDao.getDetailData(cart.productId)).thenReturn(flowOf(cart.toCartEntity()))
        productRepository.updateCartQuantity(cart, true)

        verify(productCartDao).update(cartEntity.copy(quantity = cartEntity.quantity + 1))
    }

    @Test
    fun `test updateCartQuantity when quantity is not null`() = runTest {
        val cart = dummyCartList[0].toCart().copy(quantity = 2)
        val cartEntity = cart.toCartEntity()
        productRepository.updateCartQuantity(cart, true)

        verify(productCartDao).update(cartEntity.copy(quantity = cartEntity.quantity + 1))
    }

    @Test
    fun `test updateCartChecked`() = runTest {
        val cart = dummyCartList.filter {
            (it.productId.toInt() % 2) == 0
        }.map { it.toCart() }
        productRepository.updateCartChecked(true, *cart.toTypedArray())

        val cartCheckedEntity = cart.map { it.toCartEntity().copy(isChecked = true) }
        verify(productCartDao).update(*cartCheckedEntity.toTypedArray())
    }

    @Test
    fun `test deleteCart`() = runTest {
        val cart = dummyCartList.filter {
            (it.productId.toInt() % 2) == 0
        }.map { it.toCart() }
        productRepository.deleteCart(*cart.toTypedArray())

        val cartCheckedEntity = cart.map { it.toCartEntity() }
        verify(productCartDao).delete(*cartCheckedEntity.toTypedArray())
    }
}
