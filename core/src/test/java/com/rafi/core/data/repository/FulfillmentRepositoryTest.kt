package com.rafi.core.data.repository

import app.cash.turbine.test
import com.nhaarman.mockitokotlin2.whenever
import com.rafi.core.data.model.FulfillmentRequest
import com.rafi.core.data.model.RatingRequest
import com.rafi.core.data.source.local.room.entity.dummyCartList
import com.rafi.core.data.source.remote.response.dummyFulfillmentResponse
import com.rafi.core.data.source.remote.response.dummyRatingResponse
import com.rafi.core.data.source.remote.response.dummyTransactionResponse
import com.rafi.core.data.source.remote.retrofit.ApiService
import com.rafi.core.util.DataMapper.toCart
import com.rafi.core.util.DataMapper.toFulfillment
import com.rafi.core.util.DataMapper.toFulfillmentRequestItem
import com.rafi.core.util.DataMapper.toTransaction
import com.rafi.core.util.Resources
import junit.framework.TestCase.assertEquals
import kotlinx.coroutines.test.runTest
import org.junit.Assert
import org.junit.Before
import org.junit.Test
import org.junit.runner.RunWith
import org.junit.runners.JUnit4
import org.mockito.Mockito.mock

@RunWith(JUnit4::class)
class FulfillmentRepositoryTest {

    private lateinit var fulfillmentRepository: FulfillmentRepository
    private lateinit var apiService: ApiService

    @Before
    fun setUp() {
        apiService = mock()
        fulfillmentRepository = FulfillmentRepositoryImpl(
            apiService
        )
    }

    @Test
    fun `test fulfillment when success`() = runTest {
        val payment = "Bank BCA"
        val cartList = dummyCartList.map { it.toCart() }
        val fulfillmentItem = cartList.map { it.toFulfillmentRequestItem() }

        whenever(apiService.postFulfillment(FulfillmentRequest(payment, fulfillmentItem)))
            .thenReturn(dummyFulfillmentResponse)

        fulfillmentRepository.fulfillment(payment, cartList).test {
            assertEquals(Resources.Loading, awaitItem())
            assertEquals(
                Resources.Success(dummyFulfillmentResponse.data!!.toFulfillment()),
                awaitItem()
            )
            awaitComplete()
        }
    }

    @Test
    fun `test fulfillment when failed`() = runTest {
        val payment = "Bank BCA"
        val cartList = dummyCartList.map { it.toCart() }
        val fulfillmentItem = cartList.map { it.toFulfillmentRequestItem() }

        whenever(apiService.postFulfillment(FulfillmentRequest(payment, fulfillmentItem)))
            .thenReturn(dummyFulfillmentResponse.copy(data = null))

        fulfillmentRepository.fulfillment(payment, cartList).test {
            assertEquals(Resources.Loading, awaitItem())
            Assert.assertEquals(
                "No data",
                (awaitItem() as Resources.Error).message.message
            )
            awaitComplete()
        }
    }

    @Test
    fun `test rating when success`() = runTest {
        val invoiceId = "invoiceId"
        val rating = 3
        val review = "review"

        whenever(apiService.postRating(RatingRequest(invoiceId, rating, review)))
            .thenReturn(dummyRatingResponse)

        fulfillmentRepository.rating(invoiceId, rating, review).test {
            assertEquals(Resources.Loading, awaitItem())
            assertEquals(Resources.Success(true), awaitItem())
            awaitComplete()
        }
    }

    @Test
    fun `test rating when failed`() = runTest {
        val invoiceId = "invoiceId"
        val rating = 3
        val review = "review"

        whenever(apiService.postRating(RatingRequest(invoiceId, rating, review)))
            .thenReturn(dummyRatingResponse.copy(code = 404))

        fulfillmentRepository.rating(invoiceId, rating, review).test {
            assertEquals(Resources.Loading, awaitItem())
            Assert.assertEquals(
                "Failed",
                (awaitItem() as Resources.Error).message.message
            )
            awaitComplete()
        }
    }

    @Test
    fun `test getTransaction when success`() = runTest {
        whenever(apiService.getTransaction())
            .thenReturn(dummyTransactionResponse)

        fulfillmentRepository.getTransaction().test {
            assertEquals(Resources.Loading, awaitItem())
            assertEquals(
                Resources.Success(dummyTransactionResponse.data!!.map { it.toTransaction() }),
                awaitItem()
            )
            awaitComplete()
        }
    }

    @Test
    fun `test getTransaction when failed`() = runTest {
        whenever(apiService.getTransaction())
            .thenReturn(dummyTransactionResponse.copy(data = null))

        fulfillmentRepository.getTransaction().test {
            assertEquals(Resources.Loading, awaitItem())
            Assert.assertEquals(
                "Data is Empty",
                (awaitItem() as Resources.Error).message.message
            )
            awaitComplete()
        }
    }
}
