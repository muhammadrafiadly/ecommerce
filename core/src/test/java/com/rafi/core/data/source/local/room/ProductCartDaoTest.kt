package com.rafi.core.data.source.local.room

import android.content.Context
import androidx.room.Room
import androidx.test.core.app.ApplicationProvider
import com.rafi.core.data.source.local.room.dao.ProductCartDao
import com.rafi.core.data.source.local.room.entity.dummyCartList
import kotlinx.coroutines.flow.first
import kotlinx.coroutines.test.runTest
import org.junit.After
import org.junit.Assert.assertEquals
import org.junit.Assert.assertTrue
import org.junit.Before
import org.junit.Test
import org.junit.runner.RunWith
import org.robolectric.RobolectricTestRunner

@RunWith(RobolectricTestRunner::class)
class ProductCartDaoTest {

    private lateinit var appRoomDatabase: AppRoomDatabase
    private lateinit var productCartDao: ProductCartDao

    @Before
    fun setUp() {
        val context = ApplicationProvider.getApplicationContext<Context>()
        appRoomDatabase = Room.inMemoryDatabaseBuilder(
            context, AppRoomDatabase::class.java
        ).allowMainThreadQueries().build()
        productCartDao = appRoomDatabase.productCartDao()
    }

    @After
    fun closeDb() {
        appRoomDatabase.close()
    }

    @Test
    fun `test insert and getProduct`() = runTest {
        // Given
        dummyCartList.map {
            productCartDao.insert(it)
        }

        // When
        val actualData = productCartDao.getProduct().first()

        // Then
        val expectedData = dummyCartList
        assertEquals(expectedData, actualData)
    }

    @Test
    fun `test insert and getDetailData`() = runTest {
        // Given
        dummyCartList.map {
            productCartDao.insert(it)
        }

        // When
        val actualData = productCartDao.getDetailData(
            dummyCartList[0].productId
        ).first()
        val expectedData = dummyCartList[0]

        // Then
        assertEquals(expectedData, actualData)
    }

    @Test
    fun `test insert and getDataSize`() = runTest {
        // Given
        dummyCartList.map {
            productCartDao.insert(it)
        }

        // When
        val actualData = productCartDao.getDataSize().first()
        val expectedData = dummyCartList.size

        // Then
        assertEquals(expectedData, actualData)
    }

    @Test
    fun `test insert and checkExistProduct`() = runTest {
        // Given
        dummyCartList.map {
            productCartDao.insert(it)
        }

        // When
        val data = dummyCartList[0].productId

        // Then
        assertTrue(productCartDao.checkExistProduct(data))
    }

    @Test
    fun `test insert and update`() = runTest {
        // Given
        dummyCartList.map {
            productCartDao.insert(it)
        }

        // When
        val dummyCart = dummyCartList[0]
        productCartDao.update(dummyCart.copy(quantity = 10))

        // Then
        val actualData = productCartDao.getDetailData(dummyCart.productId).first()
        val expectedData = dummyCart.copy(quantity = 10)
        assertEquals(expectedData, actualData)
    }

    @Test
    fun `test insert and delete`() = runTest {
        // Given
        dummyCartList.map {
            productCartDao.insert(it)
        }

        // When
        productCartDao.delete(dummyCartList[0])

        // Then
        val actualData = productCartDao.getProduct().first()
        val expectedData = dummyCartList
        expectedData.remove(dummyCartList[0])
        assertEquals(expectedData, actualData)
        assertEquals(expectedData.size, actualData.size)
    }
}
