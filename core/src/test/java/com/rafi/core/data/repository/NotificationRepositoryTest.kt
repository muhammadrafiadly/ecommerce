package com.rafi.core.data.repository

import com.nhaarman.mockitokotlin2.verify
import com.nhaarman.mockitokotlin2.whenever
import com.rafi.core.data.source.local.room.dao.NotificationDao
import com.rafi.core.data.source.local.room.entity.dummyNotification
import com.rafi.core.util.DataMapper.toNotification
import kotlinx.coroutines.flow.first
import kotlinx.coroutines.flow.flowOf
import kotlinx.coroutines.test.runTest
import org.junit.Assert.assertEquals
import org.junit.Before
import org.junit.Test
import org.junit.runner.RunWith
import org.junit.runners.JUnit4
import org.mockito.Mockito.mock

@RunWith(JUnit4::class)
class NotificationRepositoryTest {

    private lateinit var notificationRepository: NotificationRepository
    private lateinit var notificationDao: NotificationDao

    @Before
    fun setUp() {
        notificationDao = mock()
        notificationRepository = NotificationRepositoryImpl(
            notificationDao
        )
    }

    @Test
    fun `test getNotification`() = runTest {
        // Given
        whenever(notificationDao.getNotification())
            .thenReturn(flowOf(dummyNotification))

        // When
        val actualData = notificationRepository.getNotification().first()

        // Then
        val expectedData = dummyNotification.map {
            it.toNotification()
        }
        assertEquals(expectedData, actualData)
    }

    @Test
    fun `test getNotificationDataSize`() = runTest {
        // Given
        whenever(notificationDao.getNotificationSize())
            .thenReturn(flowOf(dummyNotification.size))

        // When
        val actualData = notificationRepository.getNotificationDataSize().first()

        // Then
        val expectedData = dummyNotification.size
        assertEquals(expectedData, actualData)
    }

    @Test
    fun `test insertNotification`() = runTest {
        notificationRepository.insertNotification(
            dummyNotification[0].toNotification()
        )
        verify(notificationDao).insert(dummyNotification[0])
    }

    @Test
    fun `test updateNotification`() = runTest {
        notificationRepository.updateNotification(dummyNotification[0].toNotification())
        verify(notificationDao).update(dummyNotification[0])
    }
}
