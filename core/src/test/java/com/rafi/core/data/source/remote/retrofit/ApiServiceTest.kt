package com.rafi.core.data.source.remote.retrofit

import com.rafi.core.data.model.FulfillmentRequest
import com.rafi.core.data.model.LoginBody
import com.rafi.core.data.model.RatingRequest
import com.rafi.core.data.model.RefreshBody
import com.rafi.core.data.model.RegisterBody
import com.rafi.core.data.source.remote.response.dummyFulfillmentResponse
import com.rafi.core.data.source.remote.response.dummyLoginResponse
import com.rafi.core.data.source.remote.response.dummyProductResponse
import com.rafi.core.data.source.remote.response.dummyProductsDetailResponse
import com.rafi.core.data.source.remote.response.dummyProductsReviewResponse
import com.rafi.core.data.source.remote.response.dummyProfileResponse
import com.rafi.core.data.source.remote.response.dummyRatingResponse
import com.rafi.core.data.source.remote.response.dummyRefreshResponse
import com.rafi.core.data.source.remote.response.dummyRegisterResponse
import com.rafi.core.data.source.remote.response.dummySearchResponse
import com.rafi.core.data.source.remote.response.dummyTransactionResponse
import com.rafi.core.util.Helper
import kotlinx.coroutines.test.runTest
import okhttp3.MediaType.Companion.toMediaType
import okhttp3.OkHttpClient
import okhttp3.RequestBody.Companion.toRequestBody
import okhttp3.mockwebserver.MockResponse
import okhttp3.mockwebserver.MockWebServer
import org.junit.After
import org.junit.Assert.assertEquals
import org.junit.Before
import org.junit.Test
import org.junit.runner.RunWith
import org.junit.runners.JUnit4
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import java.net.HttpURLConnection

@RunWith(JUnit4::class)
class ApiServiceTest {

    private lateinit var server: MockWebServer
    private lateinit var apiService: ApiService

    @Before
    fun setup() {
        server = MockWebServer()
        server.start()

        apiService = Retrofit.Builder()
            .baseUrl(server.url("/"))
            .addConverterFactory(GsonConverterFactory.create())
            .client(OkHttpClient.Builder().build())
            .build()
            .create(ApiService::class.java)
    }

    @After
    fun teardown() {
        server.shutdown()
    }

    @Test
    fun register() = runTest {
        val response = MockResponse()
            .setResponseCode(HttpURLConnection.HTTP_OK)
            .setBody(Helper.readTestResourceFile("authentication/register.json"))
        server.enqueue(response)

        val actualData = apiService.postRegister(RegisterBody("", "", ""))
        val expectedData = dummyRegisterResponse

        assertEquals(expectedData, actualData)
    }

    @Test
    fun login() = runTest {
        val response = MockResponse()
            .setResponseCode(HttpURLConnection.HTTP_OK)
            .setBody(Helper.readTestResourceFile("authentication/login.json"))
        server.enqueue(response)

        val actualData = apiService.postLogin(LoginBody("", "", ""))
        val expectedData = dummyLoginResponse

        assertEquals(expectedData, actualData)
    }

    @Test
    fun refresh() = runTest {
        val response = MockResponse()
            .setResponseCode(HttpURLConnection.HTTP_OK)
            .setBody(Helper.readTestResourceFile("authentication/refresh.json"))
        server.enqueue(response)

        val actualData = apiService.postRefresh(RefreshBody(""))
        val expectedData = dummyRefreshResponse

        assertEquals(expectedData, actualData)
    }

    @Test
    fun profile() = runTest {
        val response = MockResponse()
            .setResponseCode(HttpURLConnection.HTTP_OK)
            .setBody(Helper.readTestResourceFile("main/profile.json"))
        server.enqueue(response)

        val actualData = apiService.postProfile("".toRequestBody("text/plain".toMediaType()), null)
        val expectedData = dummyProfileResponse

        assertEquals(expectedData, actualData)
    }

    @Test
    fun products() = runTest {
        val response = MockResponse()
            .setResponseCode(HttpURLConnection.HTTP_OK)
            .setBody(Helper.readTestResourceFile("main/products.json"))
        server.enqueue(response)

        val actualData = apiService.postProducts(mapOf("" to ""))
        val expectedData = dummyProductResponse

        assertEquals(expectedData, actualData)
    }

    @Test
    fun search() = runTest {
        val response = MockResponse()
            .setResponseCode(HttpURLConnection.HTTP_OK)
            .setBody(Helper.readTestResourceFile("main/search.json"))
        server.enqueue(response)

        val actualData = apiService.postSearch("")
        val expectedData = dummySearchResponse

        assertEquals(expectedData, actualData)
    }

    @Test
    fun detailProducts() = runTest {
        val response = MockResponse()
            .setResponseCode(HttpURLConnection.HTTP_OK)
            .setBody(Helper.readTestResourceFile("fulfillment/products_detail.json"))
        server.enqueue(response)

        val actualData = apiService.getProductDetails("")
        val expectedData = dummyProductsDetailResponse

        assertEquals(expectedData, actualData)
    }

    @Test
    fun reviewProducts() = runTest {
        val response = MockResponse()
            .setResponseCode(HttpURLConnection.HTTP_OK)
            .setBody(Helper.readTestResourceFile("fulfillment/products_review.json"))
        server.enqueue(response)

        val actualData = apiService.getProductReviews("")
        val expectedData = dummyProductsReviewResponse

        assertEquals(expectedData, actualData)
    }

    @Test
    fun fulfillment() = runTest {
        val response = MockResponse()
            .setResponseCode(HttpURLConnection.HTTP_OK)
            .setBody(Helper.readTestResourceFile("fulfillment/fulfillment.json"))
        server.enqueue(response)

        val actualData = apiService.postFulfillment(
            FulfillmentRequest(
                "",
                arrayListOf(FulfillmentRequest.Item("", "", 0))
            )
        )
        val expectedData = dummyFulfillmentResponse

        assertEquals(expectedData, actualData)
    }

    @Test
    fun rating() = runTest {
        val response = MockResponse()
            .setResponseCode(HttpURLConnection.HTTP_OK)
            .setBody(Helper.readTestResourceFile("fulfillment/rating.json"))
        server.enqueue(response)

        val actualData = apiService.postRating(RatingRequest("", null, null))
        val expectedData = dummyRatingResponse

        assertEquals(expectedData, actualData)
    }

    @Test
    fun transaction() = runTest {
        val response = MockResponse()
            .setResponseCode(HttpURLConnection.HTTP_OK)
            .setBody(Helper.readTestResourceFile("main/transaction.json"))
        server.enqueue(response)

        val actualData = apiService.getTransaction()
        val expectedData = dummyTransactionResponse

        assertEquals(expectedData, actualData)
    }
}
