package com.rafi.core.data.source.local.preferences

import android.content.Context
import androidx.datastore.preferences.core.PreferenceDataStoreFactory
import androidx.datastore.preferences.preferencesDataStoreFile
import androidx.test.core.app.ApplicationProvider
import com.rafi.core.data.model.SessionModel
import com.rafi.core.data.model.dummyPreferences
import com.rafi.core.data.source.local.preferences.dataStore.SessionPreferences
import junit.framework.TestCase.assertEquals
import junit.framework.TestCase.assertFalse
import junit.framework.TestCase.assertTrue
import kotlinx.coroutines.flow.first
import kotlinx.coroutines.test.runTest
import org.junit.Before
import org.junit.Test
import org.junit.runner.RunWith
import org.robolectric.RobolectricTestRunner

@RunWith(RobolectricTestRunner::class)
class SessionPreferencesTest {

    private lateinit var preferences: SessionPreferences

    @Before
    fun setUp() {
        val context = ApplicationProvider.getApplicationContext<Context>()
        preferences = SessionPreferences(
            PreferenceDataStoreFactory.create(produceFile = {
                context.preferencesDataStoreFile(
                    "test_session_preferences"
                )
            })
        )
    }

    @Test
    fun `test setIsOnboard and getIsOnboard`() = runTest {
        preferences.setIsOnboard(false)
        val actualData = preferences.getIsOnboard().first()

        assertFalse(actualData)
    }

    @Test
    fun `test setSession and getSession`() = runTest {
        preferences.setSession(dummyPreferences)

        val actualData = preferences.getSession().first()

        assertEquals(dummyPreferences, actualData)
    }

    @Test
    fun `test setUserAuth and getUserAuth`() = runTest {
        preferences.setUserAuth(true)

        val actualData = preferences.getUserAuth().first()

        assertTrue(actualData)
    }

    @Test
    fun `test setAppTheme and getAppTheme`() = runTest {
        preferences.setAppTheme(true)

        val actualData = preferences.getAppTheme().first()
        assertTrue(actualData)
    }

    @Test
    fun `test setAppLanguage and getAppLanguage`() = runTest {
        preferences.setAppLanguage("id")

        val actualData = preferences.getAppLanguage().first()
        val expectedData = "id"

        assertEquals(expectedData, actualData)
    }

    @Test
    fun `test logout`() = runTest {
        preferences.setSession(dummyPreferences)
        preferences.logout()

        val actualData = preferences.getSession().first()

        assertEquals(SessionModel(), actualData)
    }
}
