package com.rafi.core.data.source.local.room

import android.content.Context
import androidx.room.Room
import androidx.test.core.app.ApplicationProvider
import com.rafi.core.data.source.local.room.dao.WishListProductsDao
import com.rafi.core.data.source.local.room.entity.dummyWishList
import kotlinx.coroutines.flow.first
import kotlinx.coroutines.test.runTest
import org.junit.After
import org.junit.Assert.assertEquals
import org.junit.Assert.assertTrue
import org.junit.Before
import org.junit.Test
import org.junit.runner.RunWith
import org.robolectric.RobolectricTestRunner

@RunWith(RobolectricTestRunner::class)
class WishListProductsDaoTest {

    private lateinit var appRoomDatabase: AppRoomDatabase
    private lateinit var wishListProductsDao: WishListProductsDao

    @Before
    fun setUp() {
        val context = ApplicationProvider.getApplicationContext<Context>()
        appRoomDatabase = Room.inMemoryDatabaseBuilder(
            context, AppRoomDatabase::class.java
        ).allowMainThreadQueries().build()
        wishListProductsDao = appRoomDatabase.wishListProductsDao()
    }

    @After
    fun closeDb() {
        appRoomDatabase.close()
    }

    @Test
    fun `test insert and getProduct`() = runTest {
        // Given
        dummyWishList.map {
            wishListProductsDao.insert(it)
        }

        // When
        val actualData = wishListProductsDao.getProduct().first()
        val expectedData = dummyWishList

        // Then
        assertEquals(expectedData, actualData)
    }

    @Test
    fun `test insert and getDataSize`() = runTest {
        // Given
        dummyWishList.map {
            wishListProductsDao.insert(it)
        }

        // When
        val actualData = wishListProductsDao.getDataSize().first()
        val expectedData = dummyWishList.size

        // Then
        assertEquals(expectedData, actualData)
    }

    @Test
    fun `test insert and checkExistProduct`() = runTest {
        // Given
        dummyWishList.map {
            wishListProductsDao.insert(it)
        }

        // When
        val data = dummyWishList[0].productId

        // Then
        assertTrue(wishListProductsDao.checkExistsProduct(data))
    }

    @Test
    fun `test insert and delete`() = runTest {
        // Given
        dummyWishList.map {
            wishListProductsDao.insert(it)
        }

        // When
        wishListProductsDao.delete(dummyWishList[0])
        val actualData = wishListProductsDao.getProduct().first()
        val expectedData = dummyWishList
        expectedData.remove(dummyWishList[0])

        // Then
        assertEquals(expectedData, actualData)
        assertEquals(expectedData.size, actualData.size)
    }
}
