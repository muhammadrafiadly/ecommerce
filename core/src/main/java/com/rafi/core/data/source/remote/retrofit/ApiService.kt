package com.rafi.core.data.source.remote.retrofit

import com.rafi.core.data.model.FulfillmentRequest
import com.rafi.core.data.model.LoginBody
import com.rafi.core.data.model.RatingRequest
import com.rafi.core.data.model.RefreshBody
import com.rafi.core.data.model.RegisterBody
import com.rafi.core.data.source.remote.response.AuthenticateResponse
import com.rafi.core.data.source.remote.response.FulfillmentResponse
import com.rafi.core.data.source.remote.response.PaymentResponse
import com.rafi.core.data.source.remote.response.ProductDetailsResponse
import com.rafi.core.data.source.remote.response.ProductResponse
import com.rafi.core.data.source.remote.response.RatingResponse
import com.rafi.core.data.source.remote.response.ReviewResponse
import com.rafi.core.data.source.remote.response.SearchResponse
import com.rafi.core.data.source.remote.response.TransactionResponse
import okhttp3.MultipartBody
import okhttp3.RequestBody
import retrofit2.http.Body
import retrofit2.http.GET
import retrofit2.http.Multipart
import retrofit2.http.POST
import retrofit2.http.Part
import retrofit2.http.Path
import retrofit2.http.Query
import retrofit2.http.QueryMap

interface ApiService {

    @POST("register")
    suspend fun postRegister(
        @Body registerBody: RegisterBody
    ): AuthenticateResponse

    @POST("login")
    suspend fun postLogin(
        @Body loginBody: LoginBody
    ): AuthenticateResponse

    @POST("refresh")
    suspend fun postRefresh(
        @Body refreshBody: RefreshBody
    ): AuthenticateResponse

    @Multipart
    @POST("profile")
    suspend fun postProfile(
        @Part("userName") userName: RequestBody,
        @Part userImage: MultipartBody.Part?
    ): AuthenticateResponse

    @POST("products")
    suspend fun postProducts(
        @QueryMap query: Map<String, String>
    ): ProductResponse

    @POST("search")
    suspend fun postSearch(
        @Query("query") query: String
    ): SearchResponse

    @GET("products/{id}")
    suspend fun getProductDetails(
        @Path("id") id: String
    ): ProductDetailsResponse

    @GET("review/{id}")
    suspend fun getProductReviews(
        @Path("id") id: String
    ): ReviewResponse

    @GET("payment")
    suspend fun getPayment(): PaymentResponse

    @POST("fulfillment")
    suspend fun postFulfillment(
        @Body fulfillmentRequest: FulfillmentRequest
    ): FulfillmentResponse

    @POST("rating")
    suspend fun postRating(
        @Body ratingRequest: RatingRequest
    ): RatingResponse

    @GET("transaction")
    suspend fun getTransaction(): TransactionResponse
}
