package com.rafi.core.data.repository

import androidx.paging.PagingData
import com.rafi.core.data.model.ProductCartModel
import com.rafi.core.data.model.ProductDetailsModel
import com.rafi.core.data.model.ProductModel
import com.rafi.core.data.model.ProductQueryModel
import com.rafi.core.data.model.ProductReviewsModel
import com.rafi.core.data.model.WishListProductsModel
import com.rafi.core.util.Resources
import kotlinx.coroutines.flow.Flow

interface ProductRepository {
    fun getProducts(query: ProductQueryModel): Flow<PagingData<ProductModel>>
    fun searchProducts(query: String): Flow<Resources<List<String>>>
    fun productDetails(id: String): Flow<Resources<ProductDetailsModel>>
    fun reviewProduct(id: String): Flow<Resources<List<ProductReviewsModel>>>

    fun getWishListProducts(): Flow<List<WishListProductsModel>>
    fun getWishListProductsSize(): Flow<Int>
    suspend fun checkWishListProducts(wishListId: String): Boolean
    suspend fun insertWishList(wishListId: WishListProductsModel)
    suspend fun deleteWishList(wishListId: WishListProductsModel)

    fun getProductCart(): Flow<List<ProductCartModel>>
    fun getProductCartSize(): Flow<Int>
    suspend fun isReadyStock(cartId: ProductCartModel): Boolean
    suspend fun insertCart(cartId: ProductCartModel)
    suspend fun updateCartQuantity(cartId: ProductCartModel, isInsert: Boolean)
    suspend fun updateCartChecked(isChecked: Boolean, vararg cartId: ProductCartModel)
    suspend fun deleteCart(vararg cartId: ProductCartModel)
}
