package com.rafi.core.data.repository

import com.rafi.core.data.model.FulfillmentModel
import com.rafi.core.data.model.FulfillmentRequest
import com.rafi.core.data.model.ProductCartModel
import com.rafi.core.data.model.RatingRequest
import com.rafi.core.data.model.TransactionModel
import com.rafi.core.data.source.remote.retrofit.ApiService
import com.rafi.core.util.DataMapper.toFulfillment
import com.rafi.core.util.DataMapper.toFulfillmentRequestItem
import com.rafi.core.util.DataMapper.toTransaction
import com.rafi.core.util.Resources
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.flow
import javax.inject.Inject

class FulfillmentRepositoryImpl @Inject constructor(
    private val apiService: ApiService
) : FulfillmentRepository {

    override fun fulfillment(
        payment: String,
        listItem: List<ProductCartModel>
    ): Flow<Resources<FulfillmentModel>> = flow {
        emit(Resources.Loading)
        try {
            val listItemRequest = listItem.map { it.toFulfillmentRequestItem() }
            val request = FulfillmentRequest(payment, listItemRequest)

            val response = apiService.postFulfillment(request)

            val data = response.data?.toFulfillment()
            if (data != null) {
                emit(Resources.Success(data))
            } else {
                throw Exception("No data")
            }
        } catch (e: Exception) {
            emit(Resources.Error(e))
        }
    }

    override fun rating(
        invoiceId: String,
        rating: Int?,
        review: String?
    ): Flow<Resources<Boolean>> {
        return flow {
            emit(Resources.Loading)
            try {
                val response = apiService.postRating(
                    RatingRequest(invoiceId, rating, review)
                )
                if (response.code == 200) {
                    emit(Resources.Success(true))
                } else {
                    throw Exception("Failed")
                }
            } catch (e: Exception) {
                emit(Resources.Error(e))
            }
        }
    }

    override fun getTransaction(): Flow<Resources<List<TransactionModel>>> {
        return flow {
            emit(Resources.Loading)
            try {
                val response = apiService.getTransaction()
                val data = response.data?.map { it.toTransaction() }
                if (data != null) {
                    emit(Resources.Success(data))
                } else {
                    throw Exception("Data is Empty")
                }
            } catch (e: Exception) {
                emit(Resources.Error(e))
            }
        }
    }
}
