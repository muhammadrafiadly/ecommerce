package com.rafi.core.data.model

data class LoginBody(
    val email: String,
    val password: String,
    val firebaseToken: String
)
