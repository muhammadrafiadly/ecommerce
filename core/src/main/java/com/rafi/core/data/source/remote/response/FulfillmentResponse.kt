package com.rafi.core.data.source.remote.response

import com.google.gson.annotations.SerializedName

data class FulfillmentResponse(
    @SerializedName("code")
    val code: Int? = null,
    @SerializedName("message")
    val message: String? = null,
    @SerializedName("data")
    val `data`: FulfillmentResponseData? = null
)

data class FulfillmentResponseData(
    @SerializedName("invoiceId")
    val invoiceId: String? = null,
    @SerializedName("status")
    val status: Boolean? = null,
    @SerializedName("date")
    val date: String? = null,
    @SerializedName("time")
    val time: String? = null,
    @SerializedName("payment")
    val payment: String? = null,
    @SerializedName("total")
    val total: Int? = null
)

val dummyFulfillmentResponse = FulfillmentResponse(
    code = 200,
    message = "OK",
    data = FulfillmentResponseData(
        invoiceId = "ba47402c-d263-49d3-a1f8-759ae59fa4a1",
        status = true,
        date = "09 Jun 2023",
        time = "08:53",
        payment = "Bank BCA",
        total = 48998000
    )
)
