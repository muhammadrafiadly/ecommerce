package com.rafi.core.data.model

data class FulfillmentRequest(
    val payment: String,
    val items: List<Item>
) {
    data class Item(
        val productId: String,
        val variantName: String,
        var quantity: Int
    )
}
