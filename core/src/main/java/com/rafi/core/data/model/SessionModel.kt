package com.rafi.core.data.model

data class SessionModel(
    val userName: String? = null,
    val userImage: String? = null,
    val accessToken: String? = null,
    val refreshToken: String? = null,
    val expiresAt: Int? = null
)

val dummyPreferences = SessionModel(
    "test",
    "test.png",
    "test",
    "test",
    0
)
