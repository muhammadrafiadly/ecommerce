package com.rafi.core.data.source.local.room.entity

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey

@Entity(tableName = "notification")
data class NotificationEntity(
    @PrimaryKey(autoGenerate = true)
    @ColumnInfo(name = "id")
    val id: Int? = null,
    @ColumnInfo(name = "title")
    val title: String,
    @ColumnInfo(name = "body")
    val body: String,
    @ColumnInfo(name = "image")
    val image: String,
    @ColumnInfo(name = "type")
    val type: String,
    @ColumnInfo(name = "date")
    val date: String,
    @ColumnInfo(name = "time")
    val time: String,
    @ColumnInfo(name = "is_read")
    val isRead: Boolean
)

val dummyNotification = ArrayList<NotificationEntity>().apply {
    repeat(6) {
        add(
            NotificationEntity(
                id = it,
                image = "image.png",
                title = "Title",
                body = "Lorem ipsum",
                date = "12 Oct 2023",
                isRead = false,
                time = "09:32",
                type = "Info"
            )
        )
    }
}
