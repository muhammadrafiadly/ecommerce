package com.rafi.core.data.source.local.room.dao

import androidx.room.Dao
import androidx.room.Insert
import androidx.room.Query
import androidx.room.Update
import com.rafi.core.data.source.local.room.entity.NotificationEntity
import kotlinx.coroutines.flow.Flow

@Dao
interface NotificationDao {
    @Query("SELECT * FROM notification ORDER BY id DESC")
    fun getNotification(): Flow<List<NotificationEntity>>

    @Query("SELECT COUNT(id) FROM notification WHERE is_read = 0")
    fun getNotificationSize(): Flow<Int>

    @Insert
    suspend fun insert(notification: NotificationEntity)

    @Update
    suspend fun update(notification: NotificationEntity)
}
