package com.rafi.core.data.model

data class RegisterBody(
    val email: String,
    val password: String,
    val firebaseToken: String
)
