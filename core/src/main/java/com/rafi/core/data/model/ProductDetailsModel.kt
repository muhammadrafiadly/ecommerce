package com.rafi.core.data.model

data class ProductDetailsModel(
    val image: List<String>,
    val productId: String,
    val description: String,
    val totalRating: Int,
    val store: String,
    val productName: String,
    val totalSatisfaction: Int,
    val sale: Int,
    val productVariant: List<ProductVariant>,
    val stock: Int,
    val productRating: Float,
    val brand: String,
    val productPrice: Int,
    val totalReview: Int
) {
    data class ProductVariant(
        val variantPrice: Int,
        val variantName: String
    )
}

val dummyProductDetailsModel = ProductDetailsModel(
    image = listOf(
        "https://example.com/image1.jpg",
        "https://example.com/image2.jpg",
        "https://example.com/image3.jpg"
    ),
    productId = "123456",
    description = "Lorem ipsum dolor sit amet, consectetur adipiscing elit.",
    totalRating = 5,
    store = "SampleStore",
    productName = "Sample Product",
    totalSatisfaction = 90,
    sale = 50,
    productVariant = listOf(
        ProductDetailsModel.ProductVariant(variantPrice = 100, variantName = "Variant 1"),
        ProductDetailsModel.ProductVariant(variantPrice = 150, variantName = "Variant 2")
    ),
    stock = 100,
    productRating = 4.5f,
    brand = "SampleBrand",
    productPrice = 500,
    totalReview = 30
)
