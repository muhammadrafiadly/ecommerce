package com.rafi.core.data.source.remote.response

import com.google.gson.annotations.SerializedName

data class ProductDetailsResponse(
    @SerializedName("code")
    val code: Int? = null,
    @SerializedName("message")
    val message: String? = null,
    @SerializedName("data")
    val `data`: ProductDetailsResponseData? = null
)

data class ProductDetailsResponseData(
    @SerializedName("productId")
    val productId: String? = null,
    @SerializedName("productName")
    val productName: String? = null,
    @SerializedName("productPrice")
    val productPrice: Int? = null,
    @SerializedName("image")
    val image: List<String>? = null,
    @SerializedName("brand")
    val brand: String? = null,
    @SerializedName("description")
    val description: String? = null,
    @SerializedName("store")
    val store: String? = null,
    @SerializedName("sale")
    val sale: Int? = null,
    @SerializedName("stock")
    val stock: Int? = null,
    @SerializedName("totalRating")
    val totalRating: Int? = null,
    @SerializedName("totalReview")
    val totalReview: Int? = null,
    @SerializedName("totalSatisfaction")
    val totalSatisfaction: Int? = null,
    @SerializedName("productRating")
    val productRating: Float? = null,
    @SerializedName("productVariant")
    val productVariant: List<ProductVariantItem>? = null
)

data class ProductVariantItem(
    @SerializedName("variantName")
    val variantName: String? = null,
    @SerializedName("variantPrice")
    val variantPrice: Int? = null
)

val dummyProductsDetailResponse = ProductDetailsResponse(
    code = 200,
    message = "OK",
    data = ProductDetailsResponseData(
        productId = "17b4714d-527a-4be2-84e2-e4c37c2b3292",
        productName = "ASUS ROG Strix G17 G713RM-R736H6G-O - Eclipse Gray",
        productPrice = 24499000,
        image = arrayListOf(
            "https://images.tokopedia.net/img/cache/900/VqbcmM/2022/4/6/0a49c399-cf6b-47f5-91c9-8cbd0b86462d.jpg",
            "https://images.tokopedia.net/img/cache/900/VqbcmM/2022/3/25/0cc3d06c-b09d-4294-8c3f-1c37e60631a6.jpg",
            "https://images.tokopedia.net/img/cache/900/VqbcmM/2022/3/25/33a06657-9f88-4108-8676-7adafaa94921.jpg"
        ),
        brand = "Asus",
        description = "ASUS ROG Strix G17 G713RM-R736H6G-O - Eclipse Gray [AMD Ryzen™ 7 6800H / NVIDIA® GeForce RTX™ 3060 / 8G*2 / 512GB / 17.3inch / WIN11 / OHS]\n\nCPU : AMD Ryzen™ 7 6800H Mobile Processor (8-core/16-thread, 20MB cache, up to 4.7 GHz max boost)\nGPU : NVIDIA® GeForce RTX™ 3060 Laptop GPU\nGraphics Memory : 6GB GDDR6\nDiscrete/Optimus : MUX Switch + Optimus\nTGP ROG Boost : 1752MHz* at 140W (1702MHz Boost Clock+50MHz OC, 115W+25W Dynamic Boost)\nPanel : 17.3-inch FHD (1920 x 1080) 16:9 360Hz IPS-level 300nits sRGB % 100.00%",
        store = "AsusStore",
        sale = 12,
        stock = 2,
        totalRating = 7,
        totalReview = 5,
        totalSatisfaction = 100,
        productRating = 5.0F,
        productVariant = arrayListOf(
            ProductVariantItem(
                variantName = "RAM 16GB",
                variantPrice = 0
            ),
            ProductVariantItem(
                variantName = "RAM 32GB",
                variantPrice = 1000000
            ),
        )
    )
)
