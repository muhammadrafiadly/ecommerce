package com.rafi.core.data.model

data class ProductQueryModel(
    val search: String? = null,
    val brand: String? = null,
    val lowest: String? = null,
    val highest: String? = null,
    val sort: String? = null,
    val limit: Int? = null,
    val page: Int? = null
)

val dummyProductQueryModel = ProductQueryModel(
    search = "MacBook Pro M1",
    brand = "Apple",
    lowest = "15000000",
    highest = "25000000",
    sort = "Highest Price",
    limit = 3,
    page = 3
)
