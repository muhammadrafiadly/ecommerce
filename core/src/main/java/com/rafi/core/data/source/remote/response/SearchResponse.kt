package com.rafi.core.data.source.remote.response

import com.google.gson.annotations.SerializedName

data class SearchResponse(
    @field:SerializedName("code")
    val code: Int? = null,
    @field:SerializedName("message")
    val message: String? = null,
    @field:SerializedName("data")
    val data: List<String>? = null
)

val dummySearchResponse = SearchResponse(
    code = 200,
    message = "OK",
    data = arrayListOf(
        "MacBook Air M1",
        "MacBook Air M2",
        "MacBook Air M3",
        "MacBook Pro M1",
        "MacBook Pro M2",
        "MacBook Pro M3"
    )
)
