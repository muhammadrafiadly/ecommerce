package com.rafi.core.data.firebase

import android.util.Log
import com.google.firebase.remoteconfig.ConfigUpdate
import com.google.firebase.remoteconfig.ConfigUpdateListener
import com.google.firebase.remoteconfig.FirebaseRemoteConfig
import com.google.firebase.remoteconfig.FirebaseRemoteConfigException
import com.google.gson.Gson
import com.google.gson.reflect.TypeToken
import com.rafi.core.data.model.PaymentModel
import com.rafi.core.util.Resources
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.flow
import kotlinx.coroutines.tasks.await
import javax.inject.Inject

class AppFirebaseRemoteConfig @Inject constructor(
    private val remoteConfig: FirebaseRemoteConfig
) {
    fun fetchPaymentMethod(): Flow<Resources<List<PaymentModel>>> = flow {
        emit(Resources.Loading)
        try {
            remoteConfig.fetchAndActivate().await()

            val dataJson = remoteConfig.getString(REMOTE_DATA_KEY)
            Log.d("PaymentFragment", "Fetched data from remote config: $dataJson")

            val data = extractJsonToObject(dataJson)
            Log.d("PaymentFragment", "Extracted data: $data")

            emit(Resources.Success(data))
        } catch (e: Exception) {
            Log.e("PaymentFragment", "Error fetching payment method: $e")
            emit(Resources.Error(e))
        }
    }

    fun onConfigUpdate(callback: (Resources<List<PaymentModel>>) -> Unit) {
        remoteConfig.addOnConfigUpdateListener(object : ConfigUpdateListener {
            override fun onUpdate(configUpdate: ConfigUpdate) {
                if (configUpdate.updatedKeys.contains("data")) {
                    remoteConfig.activate().addOnCompleteListener {
                        val dataJson = remoteConfig.getString(REMOTE_DATA_KEY)
                        Log.d("PaymentFragment", "Updated data from remote config: $dataJson")

                        val data = extractJsonToObject(dataJson)
                        Log.d("PaymentFragment", "Extracted updated data: $data")

                        callback(Resources.Success(data))
                    }
                }
            }

            override fun onError(error: FirebaseRemoteConfigException) {
                callback(Resources.Error(error))
            }
        })
    }

    private fun extractJsonToObject(dataJson: String): ArrayList<PaymentModel> {
        return Gson().fromJson(
            dataJson,
            object : TypeToken<ArrayList<PaymentModel>>() {}.type
        ) ?: ArrayList()
    }

    companion object {
        private const val REMOTE_DATA_KEY = "data"
    }
}
