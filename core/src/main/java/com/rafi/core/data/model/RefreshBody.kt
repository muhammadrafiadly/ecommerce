package com.rafi.core.data.model

data class RefreshBody(
    val token: String
)
