package com.rafi.core.data.repository

import com.rafi.core.data.model.SessionModel
import com.rafi.core.util.Resources
import kotlinx.coroutines.flow.Flow
import okhttp3.MultipartBody
import okhttp3.RequestBody

interface UserRepository {

    suspend fun setOnboarding(isOnboarding: Boolean)
    fun getOnboarding(): Flow<Boolean>

    fun register(email: String, password: String): Flow<Resources<Boolean>>
    fun login(email: String, password: String): Flow<Resources<Boolean>>
    suspend fun logout()

    fun getSession(): Flow<SessionModel>
    fun userAuth(): Flow<Boolean>

    fun uploadProfile(
        userName: RequestBody,
        userImage: MultipartBody.Part?
    ): Flow<Resources<Boolean>>

    suspend fun setAppTheme(darkTheme: Boolean)
    fun getAppTheme(): Flow<Boolean>

    suspend fun setAppLanguage(appLanguage: String?)
    fun getAppLanguage(): Flow<String>
}
