package com.rafi.core.data.model

data class RatingRequest(
    val invoiceId: String,
    val rating: Int?,
    val review: String?
)
