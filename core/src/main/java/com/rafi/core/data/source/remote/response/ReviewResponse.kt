package com.rafi.core.data.source.remote.response

import com.google.gson.annotations.SerializedName

data class ReviewResponse(
    @SerializedName("code")
    val code: Int? = null,
    @SerializedName("message")
    val message: String? = null,
    @SerializedName("data")
    val `data`: List<ReviewResponseData>? = null
)

data class ReviewResponseData(
    @SerializedName("userName")
    val userName: String? = null,
    @SerializedName("userImage")
    val userImage: String? = null,
    @SerializedName("userRating")
    val userRating: Int? = null,
    @SerializedName("userReview")
    val userReview: String? = null
)

val dummyProductsReviewResponse = ReviewResponse(
    code = 200,
    message = "OK",
    data = arrayListOf(
        ReviewResponseData(
            userName = "John",
            userImage = "https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcTQM4VpzpVw8mR2j9_gDajEthwY3KCOWJ1tOhcv47-H9o1a-s9GRPxdb_6G9YZdGfv0HIg&usqp=CAU",
            userRating = 4,
            userReview = "Lorem Ipsum is simply dummy text of the printing and typesetting industry."
        ),
        ReviewResponseData(
            userName = "Doe",
            userImage = "https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcTR3Z6PN8QNVhH0e7rEINu_XJS0qHIFpDT3nwF5WSkcYmr3znhY7LOTkc8puJ68Bts-TMc&usqp=CAU",
            userRating = 5,
            userReview = "Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book."
        )
    )
)
