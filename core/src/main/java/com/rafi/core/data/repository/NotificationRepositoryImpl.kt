package com.rafi.core.data.repository

import com.rafi.core.data.model.NotificationModel
import com.rafi.core.data.source.local.room.dao.NotificationDao
import com.rafi.core.data.source.local.room.entity.NotificationEntity
import com.rafi.core.util.DataMapper.toNotification
import com.rafi.core.util.DataMapper.toNotificationEntity
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.map
import javax.inject.Inject

class NotificationRepositoryImpl @Inject constructor(
    private val notificationDao: NotificationDao
) : NotificationRepository {
    override fun getNotification(): Flow<List<NotificationModel>> {
        return notificationDao.getNotification().map { value:
        List<NotificationEntity> ->
            value.map {
                it.toNotification()
            }
        }
    }

    override fun getNotificationDataSize(): Flow<Int> {
        return notificationDao.getNotificationSize()
    }

    override suspend fun insertNotification(notification: NotificationModel) {
        notificationDao.insert(notification.toNotificationEntity())
    }

    override suspend fun updateNotification(notification: NotificationModel) {
        notificationDao.update(notification.toNotificationEntity())
    }
}
