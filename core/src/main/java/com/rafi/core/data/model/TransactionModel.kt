package com.rafi.core.data.model

data class TransactionModel(
    val invoiceId: String,
    val status: Boolean,
    val date: String,
    val time: String,
    val payment: String,
    val total: Int,
    val items: List<TransactionItem>,
    val rating: Int,
    val review: String,
    val image: String,
    val name: String
) {
    data class TransactionItem(
        val productId: String,
        val variantName: String,
        val quantity: Int
    )
}
