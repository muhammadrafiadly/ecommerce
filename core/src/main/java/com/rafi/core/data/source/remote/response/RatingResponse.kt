package com.rafi.core.data.source.remote.response

import com.google.gson.annotations.SerializedName

data class RatingResponse(
    @SerializedName("code")
    val code: Int? = null,
    @SerializedName("message")
    val message: String? = null
)

val dummyRatingResponse = RatingResponse(
    code = 200,
    message = "Fulfillment rating and review success"
)
