package com.rafi.core.data.model

data class ProductReviewsModel(
    val userImage: String,
    val userName: String,
    val userReview: String,
    val userRating: Int
)
