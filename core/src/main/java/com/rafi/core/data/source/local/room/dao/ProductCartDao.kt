package com.rafi.core.data.source.local.room.dao

import androidx.room.Dao
import androidx.room.Delete
import androidx.room.Insert
import androidx.room.Query
import androidx.room.Update
import com.rafi.core.data.source.local.room.entity.ProductCartEntity
import kotlinx.coroutines.flow.Flow

@Dao
interface ProductCartDao {
    @Query("SELECT * FROM cart")
    fun getProduct(): Flow<List<ProductCartEntity>>

    @Query("SELECT * FROM cart WHERE product_id = :cartId")
    fun getDetailData(cartId: String): Flow<ProductCartEntity>

    @Query("SELECT COUNT(product_id) FROM cart")
    fun getDataSize(): Flow<Int>

    @Query("SELECT EXISTS(SELECT * FROM cart WHERE product_id = :cartId)")
    suspend fun checkExistProduct(cartId: String): Boolean

    @Update
    suspend fun update(vararg cart: ProductCartEntity)

    @Insert
    suspend fun insert(cart: ProductCartEntity)

    @Delete
    suspend fun delete(vararg cart: ProductCartEntity)
}
