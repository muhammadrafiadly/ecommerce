package com.rafi.core.data.source.remote.response

import com.google.gson.annotations.SerializedName

data class PaymentResponse(
    @SerializedName("code")
    val code: Int? = null,
    @SerializedName("message")
    val message: String? = null,
    @SerializedName("data")
    val `data`: List<PaymentResponseData>? = null
)

data class PaymentResponseData(
    @SerializedName("title")
    val title: String? = null,
    @SerializedName("item")
    val item: List<PaymentResponseDataItem>? = null
)

data class PaymentResponseDataItem(
    @SerializedName("label")
    val label: String? = null,
    @SerializedName("image")
    val image: String? = null,
    @SerializedName("status")
    val status: Boolean? = null
)
