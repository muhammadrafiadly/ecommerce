package com.rafi.core.data.source.local.preferences.dataStore

import androidx.datastore.core.DataStore
import androidx.datastore.preferences.core.Preferences
import androidx.datastore.preferences.core.booleanPreferencesKey
import androidx.datastore.preferences.core.edit
import androidx.datastore.preferences.core.intPreferencesKey
import androidx.datastore.preferences.core.stringPreferencesKey
import com.rafi.core.data.model.SessionModel
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.map
import javax.inject.Inject

class SessionPreferences @Inject constructor(
    private val dataStore: DataStore<Preferences>
) {

    suspend fun setIsOnboard(isOnboarding: Boolean) {
        dataStore.edit { preferences ->
            preferences[ONBOARD_STATUS_KEY] = isOnboarding
        }
    }

    fun getIsOnboard(): Flow<Boolean> {
        return dataStore.data.map { preferences ->
            preferences[ONBOARD_STATUS_KEY] ?: true
        }
    }

    suspend fun setSession(sessionModel: SessionModel) {
        dataStore.edit { preferences ->
            sessionModel.userName?.let { preferences[USERNAME_KEY] = it }
            sessionModel.userImage?.let { preferences[IMAGE_KEY] = it }
            sessionModel.accessToken?.let { preferences[ACCESS_TOKEN_KEY] = it }
            sessionModel.refreshToken?.let { preferences[REFRESH_TOKEN_KEY] = it }
            sessionModel.expiresAt?.let { preferences[EXPIRES_KEY] = it }
        }
    }

    fun getSession(): Flow<SessionModel> {
        return dataStore.data.map { preferences ->
            SessionModel(
                preferences[USERNAME_KEY],
                preferences[IMAGE_KEY],
                preferences[ACCESS_TOKEN_KEY],
                preferences[REFRESH_TOKEN_KEY],
                preferences[EXPIRES_KEY]
            )
        }
    }

    suspend fun setUserAuth(isLogin: Boolean) {
        dataStore.edit { preferences ->
            preferences[AUTHORIZE_KEY] = isLogin
        }
    }

    fun getUserAuth(): Flow<Boolean> {
        return dataStore.data.map { preferences ->
            preferences[AUTHORIZE_KEY] ?: false
        }
    }

    suspend fun login() {
        dataStore.edit { preferences ->
            preferences[LOGIN_STATUS_KEY] ?: false
        }
    }

    suspend fun logout() {
        dataStore.edit { preferences ->
            preferences.remove(USERNAME_KEY)
            preferences.remove(IMAGE_KEY)
            preferences.remove(ACCESS_TOKEN_KEY)
            preferences.remove(REFRESH_TOKEN_KEY)
            preferences.remove(EXPIRES_KEY)
            preferences.remove(AUTHORIZE_KEY)
        }
    }

    suspend fun setAppTheme(darkTheme: Boolean) {
        dataStore.edit { preferences ->
            preferences[THEME_MODE_KEY] = darkTheme
        }
    }

    fun getAppTheme(): Flow<Boolean> =
        dataStore.data.map { preferences ->
            preferences[THEME_MODE_KEY] ?: false
        }

    suspend fun setAppLanguage(appLanguage: String?) {
        dataStore.edit { preferences ->
            preferences[LANGUAGE_KEY] = appLanguage ?: "en"
        }
    }

    fun getAppLanguage(): Flow<String> = dataStore.data.map { preferences ->
        preferences[LANGUAGE_KEY] ?: "en"
    }

    companion object {
        private val USERNAME_KEY = stringPreferencesKey("username_key")
        private val IMAGE_KEY = stringPreferencesKey("image_key")
        private val ACCESS_TOKEN_KEY = stringPreferencesKey("access_token_key")
        private val REFRESH_TOKEN_KEY = stringPreferencesKey("refresh_token_key")
        private val EXPIRES_KEY = intPreferencesKey("expires_key")
        private val AUTHORIZE_KEY = booleanPreferencesKey("authorize_key")
        private val LOGIN_STATUS_KEY = booleanPreferencesKey("login_status_key")
        private val ONBOARD_STATUS_KEY = booleanPreferencesKey("onboard_status_key")
        private val THEME_MODE_KEY = booleanPreferencesKey("theme_mode_key")
        private val LANGUAGE_KEY = stringPreferencesKey("language_key")
    }
}
