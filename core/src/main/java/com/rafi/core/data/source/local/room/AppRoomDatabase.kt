package com.rafi.core.data.source.local.room

import androidx.room.Database
import androidx.room.RoomDatabase
import com.rafi.core.data.source.local.room.dao.NotificationDao
import com.rafi.core.data.source.local.room.dao.ProductCartDao
import com.rafi.core.data.source.local.room.dao.WishListProductsDao
import com.rafi.core.data.source.local.room.entity.NotificationEntity
import com.rafi.core.data.source.local.room.entity.ProductCartEntity
import com.rafi.core.data.source.local.room.entity.WishListProductsEntity

@Database(
    entities = [
        WishListProductsEntity::class,
        ProductCartEntity::class,
        NotificationEntity::class
    ],
    version = 1,
    exportSchema = false
)
abstract class AppRoomDatabase : RoomDatabase() {
    abstract fun wishListProductsDao(): WishListProductsDao
    abstract fun productCartDao(): ProductCartDao
    abstract fun notificationDao(): NotificationDao
}
