package com.rafi.core.data.source.remote.retrofit

import android.content.Context
import com.chuckerteam.chucker.api.ChuckerCollector
import com.chuckerteam.chucker.api.ChuckerInterceptor
import com.chuckerteam.chucker.api.RetentionManager
import com.rafi.core.data.model.RefreshBody
import com.rafi.core.data.model.SessionModel
import com.rafi.core.data.source.local.preferences.dataStore.SessionPreferences
import com.rafi.core.util.Constant
import com.rafi.core.util.DataMapper.toBearerToken
import com.rafi.core.util.DataMapper.toUser
import com.rafi.core.util.HeaderInterceptor
import kotlinx.coroutines.flow.first
import kotlinx.coroutines.runBlocking
import okhttp3.Authenticator
import okhttp3.OkHttpClient
import okhttp3.Request
import okhttp3.Response
import okhttp3.Route
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import javax.inject.Inject

class ApiConfig @Inject constructor(
    private val context: Context,
    private val sessionPreferences: SessionPreferences,
) : Authenticator {

    private val apiService: ApiService by lazy {
        val okHttpClient = OkHttpClient.Builder()
            .apply {
                addInterceptor(
                    ChuckerInterceptor.Builder(context)
                        .collector(
                            ChuckerCollector(
                                context = context,
                                showNotification = true,
                                retentionPeriod = RetentionManager.Period.ONE_HOUR
                            )
                        )
                        .build()
                )
                addInterceptor(HeaderInterceptor(sessionPreferences))
            }
            .build()

        val retrofit = Retrofit.Builder().baseUrl(Constant.Remote.BASE_URL)
            .addConverterFactory(GsonConverterFactory.create()).client(okHttpClient).build()

        val apiService = retrofit.create(ApiService::class.java)

        apiService
    }

    override fun authenticate(route: Route?, response: Response): Request? {
        synchronized(this) {
            try {
                val newToken = getNewToken()

                return response.request.newBuilder()
                    .header("Authorization", newToken.toBearerToken())
                    .build()
            } catch (e: Exception) {
                runBlocking {
                    sessionPreferences.logout()
                }
            }

            return null
        }
    }

    private fun getNewToken(): String {
        val user = runBlocking {
            sessionPreferences.getSession().first()
        }

        val refreshToken = user.refreshToken ?: ""

        val newToken = runBlocking {
            val refreshResponse = apiService.postRefresh(RefreshBody(refreshToken))
            sessionPreferences.setSession(refreshResponse.data?.toUser() ?: SessionModel())

            return@runBlocking sessionPreferences.getSession().first<SessionModel>().accessToken
        }
        return newToken ?: ""
    }
}
