package com.rafi.core.data.repository

import com.rafi.core.data.model.FulfillmentModel
import com.rafi.core.data.model.ProductCartModel
import com.rafi.core.data.model.TransactionModel
import com.rafi.core.util.Resources
import kotlinx.coroutines.flow.Flow

interface FulfillmentRepository {
    fun fulfillment(
        payment: String,
        listItem: List<ProductCartModel>
    ): Flow<Resources<FulfillmentModel>>

    fun rating(
        invoiceId: String,
        rating: Int? = null,
        review: String? = null
    ): Flow<Resources<Boolean>>

    fun getTransaction(): Flow<Resources<List<TransactionModel>>>
}
