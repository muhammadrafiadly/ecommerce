package com.rafi.core.data.source.remote.response

import com.google.gson.annotations.SerializedName

data class ProductResponse(
    @field:SerializedName("code")
    val code: Int? = null,
    @field:SerializedName("message")
    val message: String? = null,
    @field:SerializedName("data")
    val `data`: ProductsResponseData? = null
)

data class ProductsResponseData(
    @field:SerializedName("itemsPerPage")
    val itemsPerPage: Int? = null,
    @field:SerializedName("currentItemCount")
    val currentItemCount: Int? = null,
    @field:SerializedName("pageIndex")
    val pageIndex: Int? = null,
    @field:SerializedName("totalPages")
    val totalPages: Int? = null,
    @field:SerializedName("items")
    val items: List<ProductsResponseItem>? = null
)

data class ProductsResponseItem(
    @field:SerializedName("productId")
    val productId: String? = null,
    @field:SerializedName("productName")
    val productName: String? = null,
    @field:SerializedName("productPrice")
    val productPrice: Int? = null,
    @field:SerializedName("image")
    val image: String? = null,
    @field:SerializedName("brand")
    val brand: String? = null,
    @field:SerializedName("store")
    val store: String? = null,
    @field:SerializedName("sale")
    val sale: Int? = null,
    @field:SerializedName("productRating")
    val productRating: Float? = null
)

val dummyProductResponse = ProductResponse(
    code = 200,
    message = "OK",
    data = (
        ProductsResponseData(
            itemsPerPage = 10,
            currentItemCount = 10,
            pageIndex = 1,
            totalPages = 3,
            items = arrayListOf(
                ProductsResponseItem(
                    productId = "601bb59a-4170-4b0a-bd96-f34538922c7c",
                    productName = "Lenovo Legion 3",
                    productPrice = 10000000,
                    image = "image1",
                    brand = "Lenovo",
                    store = "LenovoStore",
                    sale = 2,
                    productRating = 4f
                ),
                ProductsResponseItem(
                    productId = "3134a179-dff6-464f-b76e-d7507b06887b",
                    productName = "Lenovo Legion 5",
                    productPrice = 15000000,
                    image = "image1",
                    brand = "Lenovo",
                    store = "LenovoStore",
                    sale = 4,
                    productRating = 4f
                )
            )
        )
        )
)
