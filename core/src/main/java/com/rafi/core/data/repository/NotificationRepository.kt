package com.rafi.core.data.repository

import com.rafi.core.data.model.NotificationModel
import kotlinx.coroutines.flow.Flow

interface NotificationRepository {
    fun getNotification(): Flow<List<NotificationModel>>
    fun getNotificationDataSize(): Flow<Int>
    suspend fun insertNotification(notification: NotificationModel)
    suspend fun updateNotification(notification: NotificationModel)
}
