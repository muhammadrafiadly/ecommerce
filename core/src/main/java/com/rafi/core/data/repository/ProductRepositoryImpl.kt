package com.rafi.core.data.repository

import androidx.paging.Pager
import androidx.paging.PagingConfig
import androidx.paging.PagingData
import androidx.paging.map
import com.rafi.core.data.ProductPagingSource
import com.rafi.core.data.model.ProductCartModel
import com.rafi.core.data.model.ProductDetailsModel
import com.rafi.core.data.model.ProductModel
import com.rafi.core.data.model.ProductQueryModel
import com.rafi.core.data.model.ProductReviewsModel
import com.rafi.core.data.model.WishListProductsModel
import com.rafi.core.data.source.local.room.dao.ProductCartDao
import com.rafi.core.data.source.local.room.dao.WishListProductsDao
import com.rafi.core.data.source.local.room.entity.ProductCartEntity
import com.rafi.core.data.source.local.room.entity.WishListProductsEntity
import com.rafi.core.data.source.remote.retrofit.ApiService
import com.rafi.core.util.DataMapper.toCart
import com.rafi.core.util.DataMapper.toCartEntity
import com.rafi.core.util.DataMapper.toProduct
import com.rafi.core.util.DataMapper.toProductDetails
import com.rafi.core.util.DataMapper.toProductReviews
import com.rafi.core.util.DataMapper.toWishlist
import com.rafi.core.util.DataMapper.toWishlistEntity
import com.rafi.core.util.Resources
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.first
import kotlinx.coroutines.flow.flow
import kotlinx.coroutines.flow.map
import javax.inject.Inject

class ProductRepositoryImpl @Inject constructor(
    private val apiService: ApiService,
    private val wishListProductsDao: WishListProductsDao,
    private val productCartDao: ProductCartDao
) : ProductRepository {

    override fun getProducts(query: ProductQueryModel): Flow<PagingData<ProductModel>> {
        return Pager(
            config = PagingConfig(pageSize = 10, prefetchDistance = 1, initialLoadSize = 10),
            pagingSourceFactory = {
                ProductPagingSource(apiService, query)
            },
        ).flow.map {
            it.map { productResponseItem ->
                productResponseItem.toProduct()
            }
        }
    }

    override fun searchProducts(query: String): Flow<Resources<List<String>>> {
        return flow {
            emit(Resources.Loading)
            try {
                if (query.isNotEmpty()) {
                    val response = apiService.postSearch(query)
                    val listSearch = response.data ?: emptyList()
                    emit(Resources.Success(listSearch))
                } else {
                    emit(Resources.Success(emptyList()))
                }
            } catch (e: Exception) {
                emit(Resources.Error(e))
            }
        }
    }

    override fun productDetails(id: String): Flow<Resources<ProductDetailsModel>> {
        return flow {
            emit(Resources.Loading)
            try {
                val response = apiService.getProductDetails(id)
                val data = response.data?.toProductDetails()
                if (data != null) {
                    emit(Resources.Success(data))
                } else {
                    throw Exception("Data not found")
                }
            } catch (e: Exception) {
                emit(Resources.Error(e))
            }
        }
    }

    override fun reviewProduct(id: String): Flow<Resources<List<ProductReviewsModel>>> {
        return flow {
            emit(Resources.Loading)
            try {
                val response = apiService.getProductReviews(id)
                val data = response.data?.map { it.toProductReviews() }
                if (data != null) {
                    emit(Resources.Success(data))
                } else {
                    throw Exception("Data not found")
                }
            } catch (e: Exception) {
                emit(Resources.Error(e))
            }
        }
    }

    override fun getWishListProducts(): Flow<List<WishListProductsModel>> {
        return wishListProductsDao.getProduct().map { value: List<WishListProductsEntity> ->
            value.map { it.toWishlist() }
        }
    }

    override fun getWishListProductsSize(): Flow<Int> {
        return wishListProductsDao.getDataSize()
    }

    override suspend fun checkWishListProducts(wishListId: String): Boolean {
        return wishListProductsDao.checkExistsProduct(wishListId)
    }

    override suspend fun insertWishList(wishListId: WishListProductsModel) {
        wishListProductsDao.insert(wishListId.toWishlistEntity())
    }

    override suspend fun deleteWishList(wishListId: WishListProductsModel) {
        wishListProductsDao.delete(wishListId.toWishlistEntity())
    }

    override fun getProductCart(): Flow<List<ProductCartModel>> {
        return productCartDao.getProduct().map { value: List<ProductCartEntity> ->
            value.map { it.toCart() }
        }
    }

    override fun getProductCartSize(): Flow<Int> {
        return productCartDao.getDataSize()
    }

    override suspend fun isReadyStock(cartId: ProductCartModel): Boolean {
        val stock = cartId.stock
        val totalBuy = if (cartId.quantity == null) {
            val cartIsExisted = productCartDao.checkExistProduct(cartId.productId)
            if (cartIsExisted) {
                productCartDao.getDetailData(cartId.productId).first().quantity
            } else {
                0
            }
        } else {
            cartId.quantity
        }
        return totalBuy < stock
    }

    override suspend fun insertCart(cartId: ProductCartModel) {
        val cartIsExisted = productCartDao.checkExistProduct(cartId.productId)
        if (cartIsExisted) {
            updateCartQuantity(cartId, true)
        } else {
            productCartDao.insert(cartId.toCartEntity())
        }
    }

    override suspend fun updateCartQuantity(cartId: ProductCartModel, isInsert: Boolean) {
        val number = if (isInsert) 1 else -1

        val cartEntity = if (cartId.quantity == null) {
            productCartDao.getDetailData(cartId.productId).first()
        } else {
            cartId.toCartEntity()
        }

        productCartDao.update(cartEntity.copy(quantity = cartEntity.quantity + number))
    }

    override suspend fun updateCartChecked(isChecked: Boolean, vararg cartId: ProductCartModel) {
        val arrayCartEntity = cartId.map {
            it.toCartEntity().copy(isChecked = isChecked)
        }.toTypedArray()
        productCartDao.update(*arrayCartEntity)
    }

    override suspend fun deleteCart(vararg cartId: ProductCartModel) {
        val arrayCartEntity = cartId.map { it.toCartEntity() }.toTypedArray()
        productCartDao.delete(*arrayCartEntity)
    }
}
