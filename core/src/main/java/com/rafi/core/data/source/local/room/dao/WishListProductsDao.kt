package com.rafi.core.data.source.local.room.dao

import androidx.room.Dao
import androidx.room.Delete
import androidx.room.Insert
import androidx.room.Query
import com.rafi.core.data.source.local.room.entity.WishListProductsEntity
import kotlinx.coroutines.flow.Flow

@Dao
interface WishListProductsDao {
    @Query("SELECT * FROM wish_list")
    fun getProduct(): Flow<List<WishListProductsEntity>>

    @Query("SELECT COUNT(product_id) FROM wish_list")
    fun getDataSize(): Flow<Int>

    @Query("SELECT EXISTS(SELECT * FROM wish_list WHERE product_id = :wishListId)")
    suspend fun checkExistsProduct(wishListId: String): Boolean

    @Insert
    suspend fun insert(wishListId: WishListProductsEntity)

    @Delete
    suspend fun delete(wishListId: WishListProductsEntity)
}
