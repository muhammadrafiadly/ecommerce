package com.rafi.core.data.repository

import com.rafi.core.data.firebase.AppFirebaseMessaging
import com.rafi.core.data.model.LoginBody
import com.rafi.core.data.model.RegisterBody
import com.rafi.core.data.model.SessionModel
import com.rafi.core.data.source.local.preferences.dataStore.SessionPreferences
import com.rafi.core.data.source.remote.response.AuthenticateResponse
import com.rafi.core.data.source.remote.retrofit.ApiService
import com.rafi.core.util.DataMapper.toUser
import com.rafi.core.util.Resources
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.flow
import okhttp3.MultipartBody
import okhttp3.RequestBody
import javax.inject.Inject

class UserRepositoryImpl @Inject constructor(
    private val preferences: SessionPreferences,
    private val apiService: ApiService,
    private val firebaseMessaging: AppFirebaseMessaging
) : UserRepository {

    override suspend fun setOnboarding(isOnboarding: Boolean) {
        preferences.setIsOnboard(isOnboarding)
    }

    override fun getOnboarding(): Flow<Boolean> = preferences.getIsOnboard()

    override fun register(email: String, password: String): Flow<Resources<Boolean>> {
        return flow {
            emit(Resources.Loading)
            try {
                val token = firebaseMessaging.getToken()
                val request = RegisterBody(email, password, token)
                val response = apiService.postRegister(request)
                val user = response.data?.toUser()

                if (user != null) {
                    preferences.setSession(user)
                    preferences.setUserAuth(true)
                    firebaseMessaging.subscribeToTopic("promo")
                    emit(Resources.Success(true))
                } else {
                    throw Exception("Failed")
                }
            } catch (e: Exception) {
                emit(Resources.Error(e))
            }
        }
    }

    override fun login(email: String, password: String): Flow<Resources<Boolean>> {
        return flow {
            emit(Resources.Loading)
            try {
                val firebaseToken = firebaseMessaging.getToken()
                val request = LoginBody(email, password, firebaseToken)
                val response = apiService.postLogin(request)
                val user = response.data?.toUser()
                if (user != null) {
                    preferences.setSession(user)
                    preferences.setUserAuth(true)
                    firebaseMessaging.subscribeToTopic("promo")
                    emit(Resources.Success(true))
                } else {
                    throw Exception("Failed")
                }
            } catch (e: Exception) {
                emit(Resources.Error(e))
            }
        }
    }

    override suspend fun logout() {
        preferences.logout()
        firebaseMessaging.unsubscribeFromTopic("promo")
    }

    override fun getSession(): Flow<SessionModel> {
        return preferences.getSession()
    }

    override fun userAuth(): Flow<Boolean> {
        return preferences.getUserAuth()
    }

    override fun uploadProfile(
        userName: RequestBody,
        userImage: MultipartBody.Part?
    ): Flow<Resources<Boolean>> =
        flow {
            emit(Resources.Loading)
            try {
                val response: AuthenticateResponse = apiService.postProfile(userName, userImage)

                val user = response.data?.toUser()
                if (user != null) {
                    preferences.setSession(user)
                    emit(Resources.Success(true))
                } else {
                    throw Exception("Failed")
                }
            } catch (e: Exception) {
                emit(Resources.Error(e))
            }
        }

    override suspend fun setAppTheme(darkTheme: Boolean) {
        preferences.setAppTheme(darkTheme)
    }

    override fun getAppTheme(): Flow<Boolean> {
        return preferences.getAppTheme()
    }

    override suspend fun setAppLanguage(appLanguage: String?) {
        preferences.setAppLanguage(appLanguage)
    }

    override fun getAppLanguage(): Flow<String> = preferences.getAppLanguage()
}
