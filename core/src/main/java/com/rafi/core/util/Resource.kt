package com.rafi.core.util

sealed class Resources<out T> {
    data class Success<T>(val data: T) : Resources<T>()
    data class Error(val message: Throwable) : Resources<Nothing>()
    data object Loading : Resources<Nothing>()
}
