package com.rafi.core.util

import com.rafi.core.data.model.FulfillmentModel
import com.rafi.core.data.model.FulfillmentRequest
import com.rafi.core.data.model.NotificationModel
import com.rafi.core.data.model.PaymentModel
import com.rafi.core.data.model.ProductCartModel
import com.rafi.core.data.model.ProductDetailsModel
import com.rafi.core.data.model.ProductModel
import com.rafi.core.data.model.ProductQueryModel
import com.rafi.core.data.model.ProductReviewsModel
import com.rafi.core.data.model.SessionModel
import com.rafi.core.data.model.TransactionModel
import com.rafi.core.data.model.WishListProductsModel
import com.rafi.core.data.source.local.room.entity.NotificationEntity
import com.rafi.core.data.source.local.room.entity.ProductCartEntity
import com.rafi.core.data.source.local.room.entity.WishListProductsEntity
import com.rafi.core.data.source.remote.response.AuthDataResponse
import com.rafi.core.data.source.remote.response.FulfillmentResponseData
import com.rafi.core.data.source.remote.response.PaymentResponseData
import com.rafi.core.data.source.remote.response.PaymentResponseDataItem
import com.rafi.core.data.source.remote.response.ProductDetailsResponseData
import com.rafi.core.data.source.remote.response.ProductVariantItem
import com.rafi.core.data.source.remote.response.ProductsResponseItem
import com.rafi.core.data.source.remote.response.ReviewResponseData
import com.rafi.core.data.source.remote.response.TransactionResponseData
import com.rafi.core.data.source.remote.response.TransactionResponseDataItem
import okhttp3.MediaType.Companion.toMediaType
import okhttp3.MultipartBody
import okhttp3.RequestBody.Companion.asRequestBody
import java.io.File

object DataMapper {
    fun AuthDataResponse.toUser(): SessionModel =
        SessionModel(
            userName = userName,
            userImage = userImage,
            accessToken = accessToken,
            refreshToken = refreshToken,
            expiresAt = expiresAt
        )

    fun File.toMultipartBody(field: String): MultipartBody.Part {
        val requestImageFile = this.asRequestBody("image/*".toMediaType())
        return MultipartBody.Part.createFormData(
            field,
            this.name,
            requestImageFile
        )
    }

    fun String.toBearerToken(): String = "Bearer $this"

    fun ProductQueryModel.toQueryMap(): Map<String, String> {
        val map = HashMap<String, String>()
        search?.let { map.put("search", it) }
        brand?.let { map.put("brand", it) }
        lowest?.let { map.put("lowest", it) }
        highest?.let { map.put("highest", it) }
        sort?.let { map.put("sort", it) }
        limit?.let { map.put("limit", it.toString()) }
        page?.let { map.put("page", it.toString()) }
        return map
    }

    fun ProductsResponseItem.toProduct(): ProductModel =
        ProductModel(
            productId ?: "",
            productName ?: "",
            productPrice ?: 0,
            image ?: "",
            brand ?: "",
            store ?: "",
            sale ?: 0,
            productRating ?: 0F
        )

    fun ProductDetailsResponseData.toProductDetails(): ProductDetailsModel =
        ProductDetailsModel(
            image ?: emptyList(),
            productId ?: "",
            description ?: "",
            totalRating ?: 0,
            store ?: "",
            productName ?: "",
            totalSatisfaction ?: 0,
            sale ?: 0,
            productVariant?.map { it.toProductVariant() } ?: emptyList(),
            stock ?: 0,
            productRating ?: 0F,
            brand ?: "",
            productPrice ?: 0,
            totalReview ?: 0
        )

    private fun ProductVariantItem.toProductVariant(): ProductDetailsModel.ProductVariant =
        ProductDetailsModel.ProductVariant(
            variantPrice ?: 0,
            variantName ?: ""
        )

    fun ReviewResponseData.toProductReviews(): ProductReviewsModel =
        ProductReviewsModel(
            userImage ?: "",
            userName ?: "",
            userReview ?: "",
            userRating ?: 0
        )

    fun WishListProductsEntity.toWishlist(): WishListProductsModel =
        WishListProductsModel(
            productId,
            productName,
            productPrice,
            image,
            stock,
            store,
            sale,
            productRating,
            description,
            totalRating,
            totalSatisfaction,
            brand,
            totalReview,
            variantName,
            variantPrice
        )

    fun WishListProductsModel.toWishlistEntity(): WishListProductsEntity =
        WishListProductsEntity(
            productId,
            productName,
            productPrice,
            image,
            stock,
            store,
            sale,
            productRating,
            description,
            totalRating,
            totalSatisfaction,
            brand,
            totalReview,
            variantName,
            variantPrice
        )

    fun ProductCartModel.toCartEntity(): ProductCartEntity =
        ProductCartEntity(
            productId,
            productName,
            productPrice,
            image,
            stock,
            store,
            sale,
            productRating,
            description,
            totalRating,
            totalSatisfaction,
            brand,
            totalReview,
            variantName,
            variantPrice,
            quantity ?: 1,
            isChecked,
        )

    fun ProductCartEntity.toCart(): ProductCartModel =
        ProductCartModel(
            productId,
            productName,
            productPrice,
            image,
            stock,
            store,
            sale,
            productRating,
            description,
            totalRating,
            totalSatisfaction,
            brand,
            totalReview,
            variantName,
            variantPrice,
            quantity,
            isChecked,
        )

    fun ProductDetailsModel.toWishlist(variant: ProductDetailsModel.ProductVariant): WishListProductsModel =
        WishListProductsModel(
            productId,
            productName,
            productPrice,
            image[0],
            stock,
            store,
            sale,
            productRating,
            description,
            totalRating,
            totalSatisfaction,
            brand,
            totalReview,
            variant.variantName,
            variant.variantPrice
        )

    fun ProductDetailsModel.toCart(variant: ProductDetailsModel.ProductVariant): ProductCartModel =
        ProductCartModel(
            productId,
            productName,
            productPrice,
            image[0],
            stock,
            store,
            sale,
            productRating,
            description,
            totalRating,
            totalSatisfaction,
            brand,
            totalReview,
            variant.variantName,
            variant.variantPrice,
        )

    fun WishListProductsModel.toCart(): ProductCartModel =
        ProductCartModel(
            productId,
            productName,
            productPrice,
            image,
            stock,
            store,
            sale,
            productRating,
            description,
            totalRating,
            totalSatisfaction,
            brand,
            totalReview,
            variantName,
            variantPrice,
        )

    fun PaymentResponseData.toPaymentList(): MutableList<PaymentModel> =
        mutableListOf(
            PaymentModel(
                title ?: "",
                item?.map { it.toPaymentItem() } ?: ArrayList()
            )
        )

    fun PaymentResponseData.toPayment(): MutableList<PaymentModel> =
        mutableListOf(
            PaymentModel(
                title ?: "",
                item?.map { it.toPaymentItem() } ?: ArrayList()
            )
        )

    fun PaymentResponseDataItem.toPaymentItem(): PaymentModel.PaymentItem =
        PaymentModel.PaymentItem(
            image ?: "",
            label ?: "",
            status ?: false
        )

    fun FulfillmentResponseData.toFulfillment(): FulfillmentModel =
        FulfillmentModel(
            invoiceId ?: "",
            status ?: false,
            date ?: "",
            time ?: "",
            payment ?: "",
            total ?: 0
        )

    fun ProductCartModel.toFulfillmentRequestItem(): FulfillmentRequest.Item =
        FulfillmentRequest.Item(
            productId,
            variantName,
            quantity ?: 1
        )

    fun TransactionResponseData.toTransaction(): TransactionModel =
        TransactionModel(
            invoiceId ?: "",
            status ?: false,
            date ?: "",
            time ?: "",
            payment ?: "",
            total ?: 0,
            items?.map { it.toTransactionItem() } ?: ArrayList(),
            rating ?: 0,
            review ?: "",
            image ?: "",
            name ?: ""
        )

    fun TransactionResponseDataItem.toTransactionItem(): TransactionModel.TransactionItem =
        TransactionModel.TransactionItem(
            productId ?: "",
            variantName ?: "",
            quantity ?: 0
        )

    fun TransactionModel.toFulfillment(): FulfillmentModel =
        FulfillmentModel(
            invoiceId,
            status,
            date,
            time,
            payment,
            total
        )

    fun NotificationModel.toNotificationEntity(): NotificationEntity =
        NotificationEntity(
            id,
            title,
            body,
            image,
            type,
            date,
            time,
            isRead
        )

    fun NotificationEntity.toNotification(): NotificationModel =
        NotificationModel(
            id,
            title,
            body,
            image,
            type,
            date,
            time,
            isRead
        )
}
