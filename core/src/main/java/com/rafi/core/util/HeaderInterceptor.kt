package com.rafi.core.util

import com.rafi.core.data.source.local.preferences.dataStore.SessionPreferences
import com.rafi.core.util.DataMapper.toBearerToken
import kotlinx.coroutines.flow.first
import kotlinx.coroutines.runBlocking
import okhttp3.Interceptor
import okhttp3.Response
import javax.inject.Inject

class HeaderInterceptor @Inject constructor(private val sessionPreferences: SessionPreferences) :
    Interceptor {
    override fun intercept(chain: Interceptor.Chain): Response {
        val originalRequest = chain.request()
        val requestUrl = originalRequest.url.toString()
        val preLoginRequest = Constant.Remote.AUTH_ENDPOINT.contains(requestUrl)

        val newRequest = originalRequest.newBuilder().apply {
            if (preLoginRequest) {
                addHeader("API_KEY", Constant.Remote.API_KEY)
            } else {
                val user = runBlocking {
                    sessionPreferences.getSession().first()
                }
                val bearerToken: String = user.accessToken?.toBearerToken() ?: ""
                addHeader("Authorization", bearerToken)
            }
        }.build()
        return chain.proceed(newRequest)
    }
}
