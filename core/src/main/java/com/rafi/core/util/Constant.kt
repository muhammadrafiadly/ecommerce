package com.rafi.core.util

object Constant {

    object DataStorePreferences {
        const val DATASTORE_NAME = "app_preferences"
    }

    object Room {
        const val DATABASE_NAME = "app_room_database"
    }

    object Remote {
        const val BASE_URL = "http://172.17.20.96:5000/"

//        const val BASE_URL = "http://192.168.101.126:5000/"
        const val API_KEY = "6f8856ed-9189-488f-9011-0ff4b6c08edc"

        val AUTH_ENDPOINT = listOf(
            "${BASE_URL}login",
            "${BASE_URL}register",
            "${BASE_URL}refresh"
        )
    }

    object Filter {
        const val SORT = "sort"
        const val BRAND = "brand"
        const val LOWEST = "lowest"
        const val HIGHEST = "highest"

        const val SORT_KEY = "sort_key"
        const val SORT_ID_KEY = "sort_id_key"
        const val BRAND_KEY = "brand_key"
        const val BRAND_ID_KEY = "brand_id_key"
        const val LOWEST_KEY = "lowest_key"
        const val HIGHEST_KEY = "highest_key"
    }

    object Number {
        const val SNACK_BAR_DELAY = 200L
    }
}
