package com.rafi.core.di

import android.content.Context
import androidx.datastore.core.DataStore
import androidx.datastore.preferences.core.PreferenceDataStoreFactory
import androidx.datastore.preferences.core.Preferences
import androidx.datastore.preferences.preferencesDataStoreFile
import androidx.room.Room
import com.chuckerteam.chucker.api.ChuckerCollector
import com.chuckerteam.chucker.api.ChuckerInterceptor
import com.chuckerteam.chucker.api.RetentionManager
import com.google.firebase.messaging.FirebaseMessaging
import com.google.firebase.remoteconfig.FirebaseRemoteConfig
import com.rafi.core.data.firebase.AppFirebaseMessaging
import com.rafi.core.data.repository.FulfillmentRepository
import com.rafi.core.data.repository.FulfillmentRepositoryImpl
import com.rafi.core.data.repository.NotificationRepository
import com.rafi.core.data.repository.NotificationRepositoryImpl
import com.rafi.core.data.repository.ProductRepository
import com.rafi.core.data.repository.ProductRepositoryImpl
import com.rafi.core.data.repository.UserRepository
import com.rafi.core.data.repository.UserRepositoryImpl
import com.rafi.core.data.source.local.preferences.dataStore.SessionPreferences
import com.rafi.core.data.source.local.room.AppRoomDatabase
import com.rafi.core.data.source.local.room.dao.NotificationDao
import com.rafi.core.data.source.local.room.dao.ProductCartDao
import com.rafi.core.data.source.local.room.dao.WishListProductsDao
import com.rafi.core.data.source.remote.retrofit.ApiConfig
import com.rafi.core.data.source.remote.retrofit.ApiService
import com.rafi.core.util.Constant
import com.rafi.core.util.HeaderInterceptor
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.android.qualifiers.ApplicationContext
import dagger.hilt.components.SingletonComponent
import okhttp3.OkHttpClient
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import javax.inject.Singleton

@Module
@InstallIn(SingletonComponent::class)
object AppModule {

    @Singleton
    @Provides
    fun provideDataStore(@ApplicationContext context: Context): DataStore<Preferences> =
        PreferenceDataStoreFactory.create(produceFile = {
            context.preferencesDataStoreFile(
                Constant.DataStorePreferences.DATASTORE_NAME
            )
        })

    @Singleton
    @Provides
    fun providePreferences(dataStore: DataStore<Preferences>): SessionPreferences =
        SessionPreferences(dataStore)

    @Singleton
    @Provides
    fun provideChuckerInterceptor(@ApplicationContext context: Context): ChuckerInterceptor =
        ChuckerInterceptor.Builder(context)
            .collector(
                ChuckerCollector(
                    context = context,
                    showNotification = true,
                    retentionPeriod = RetentionManager.Period.ONE_HOUR
                )
            )
            .build()

    @Singleton
    @Provides
    fun provideHeaderInterceptor(sessionPreferences: SessionPreferences): HeaderInterceptor =
        HeaderInterceptor(sessionPreferences)

    @Singleton
    @Provides
    fun provideSupportAuthenticator(
        @ApplicationContext context: Context,
        sessionPreferences: SessionPreferences
    ): ApiConfig =
        ApiConfig(context, sessionPreferences)

    @Provides
    @Singleton
    fun provideOkHttpClient(
        chuckerInterceptor: ChuckerInterceptor,
        headerInterceptor: HeaderInterceptor,
        apiConfig: ApiConfig
    ): OkHttpClient = OkHttpClient.Builder()
        .apply {
            addInterceptor(chuckerInterceptor)
            addInterceptor(headerInterceptor)
            authenticator(apiConfig)
        }
        .build()

    @Provides
    @Singleton
    fun provideRetrofit(
        okHttpClient: OkHttpClient
    ): Retrofit = Retrofit.Builder().baseUrl(Constant.Remote.BASE_URL)
        .addConverterFactory(GsonConverterFactory.create()).client(okHttpClient).build()

    @Provides
    @Singleton
    fun provideApiService(retrofit: Retrofit): ApiService = retrofit.create(ApiService::class.java)

    @Provides
    @Singleton
    fun provideUserRepository(
        preferences: SessionPreferences,
        apiService: ApiService,
        firebaseMessaging: AppFirebaseMessaging
    ): UserRepository = UserRepositoryImpl(preferences, apiService, firebaseMessaging)

    @Provides
    @Singleton
    fun provideStoreRepository(
        apiService: ApiService,
        wishListProductsDao: WishListProductsDao,
        productCartDao: ProductCartDao
    ): ProductRepository = ProductRepositoryImpl(apiService, wishListProductsDao, productCartDao)

    @Provides
    @Singleton
    fun provideFulfillmentRepository(
        apiService: ApiService
    ): FulfillmentRepository = FulfillmentRepositoryImpl(apiService)

    @Provides
    @Singleton
    fun provideNotificationRepository(
        notificationDao: NotificationDao
    ): NotificationRepository = NotificationRepositoryImpl(notificationDao)

    @Provides
    @Singleton
    fun provideAppRoomDatabase(
        @ApplicationContext context: Context
    ): AppRoomDatabase = Room.databaseBuilder(
        context,
        AppRoomDatabase::class.java,
        Constant.Room.DATABASE_NAME
    )
        .allowMainThreadQueries().build()

    @Provides
    @Singleton
    fun provideWishListProductsDao(appRoomDatabase: AppRoomDatabase): WishListProductsDao =
        appRoomDatabase
            .wishListProductsDao()

    @Provides
    @Singleton
    fun provideProductCartDao(appRoomDatabase: AppRoomDatabase): ProductCartDao = appRoomDatabase
        .productCartDao()

    @Provides
    @Singleton
    fun provideNotificationDao(appRoomDatabase: AppRoomDatabase): NotificationDao =
        appRoomDatabase.notificationDao()

    @Provides
    @Singleton
    fun provideFirebaseRemoteConfig(): FirebaseRemoteConfig {
//        val configSettings = FirebaseRemoteConfigSettings.Builder()
//            .setMinimumFetchIntervalInSeconds(1)
//            .build()
//
//        remoteConfig.setConfigSettingsAsync(configSettings)
        return FirebaseRemoteConfig.getInstance()
    }

//    @Provides
//    @Singleton
//    fun provideFirebaseRemoteConfig(): FirebaseRemoteConfig =
//        Firebase.remoteConfig.apply {
//            val configSettings = remoteConfigSettings {
//                minimumFetchIntervalInSeconds = 1
//            }
//            setConfigSettingsAsync(configSettings)
//            setDefaultsAsync(R.xml.remote_config_defaults)
//        }

//    @Singleton
//    @Provides
//    fun provideAppFirebaseRemoteConfig(
//        firebaseRemoteConfig: FirebaseRemoteConfig
//    ): AppFirebaseRemoteConfig =
//        AppFirebaseRemoteConfig(firebaseRemoteConfig)

    @Singleton
    @Provides
    fun provideAppFirebaseMessaging(): AppFirebaseMessaging =
        AppFirebaseMessaging(FirebaseMessaging.getInstance())
}
